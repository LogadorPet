<?php
header ("Pragma: no-cache");					              	// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
header("Cache-Control: no-cache, must-revalidate");   // idem	http 1,1
?>

<!-- Programa Principal do Logador (index.php)
1. Mostra a tabela com os dados de todos os usu�rios (logados ou n�o)
2. Permite o link para submiss�o de pendentes
3. Permite o link para entrar na �rea do administrador
4. Permite links para Login/Logout e Esqueci Logado
-->

<?php

// inclui as fun��es necess�rias para o trabalho com o logador
include("calctemp.php");

// verifica se o acesso atual encontra-se em uma nova semana petiana
// caso estiver, atualiza dado referente ao in�cio da semana e atualiza o banco de dados
verifica_mesma_semana(); 

// verifica se o acesso atual encontra-se em um novo m�s petiano
// caso estiver, atualiza dado referente ao in�cio do m�s e atualiza o banco de dados
verifica_mesmo_mes();

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Logador PET - Computa��o </title>
<link rel="stylesheet" type="text/css" href="estilo.css">
<script type="text/javascript">setTimeout("location.href = 'logador.php';", 60000);</script>

</head>
<body>
<center>
	<h1>
		LOGADOR PET 
	</h1>
</center>

<table align="center" border=1 class="bordasimples">
	    <tr bgcolor="#990033" style="color: #fff;" align="center">
			<td width="200" align="left">Nome</td>
			<td width="70">Horas</td>
			<td width="70">Semana</td>
			<td width="70">M�s</td>
			<td width="70">Pendentes</td>
			<td width="50">Login</td>
			<td>Esqueci Logado</td>			
		</tr>
		
<?php		
			
		// abre conex�o com banco de dados 
	  include("conecta.inc");
    
		// procura por todos os usu�rios
		$pesq_usuario = mysql_query("SELECT * FROM usuario ORDER BY nome") or die("puxa!");
		
		//mostra dados de todos os usu�rios
		while ($usuario = mysql_fetch_array($pesq_usuario)) 
		{		
		    	// se $pesq_hist obtiver algum resultado, o usu�rio em quest�o est� logado (camp fim em 'historico' encontra-se com NULL)
					$pesq_hist = mysql_query("SELECT * FROM historico WHERE nome = '$usuario[0]' AND fim is NULL") or die("OGGGG");
					// se o usu�rio est� logado
					if ($hist = mysql_fetch_array($pesq_hist)) 
					{					
							$logado = true; 							
							// obt�m a diferen�a de tempo entre o tempo atual de acesso e o tempo que o usu�rio se logou							
				   		$datadif = tempo($hist[2],date("Y-m-d H:i:s"));	 	
													
							// as horas da semana e do m�s se comp�esm das horas encontradas na tebela usu�rio mais o tempo que o mesmo est� logado
							$semana = sectohour(datetosec($datadif['data']) + datetosec($usuario[1]));
							$mes =  sectohour(datetosec($datadif['data']) + datetosec($usuario[2]));
					} 
					else // se o usu�rio n�o est� logado
					{
							$logado=false;
							
							// as horas da semana e m�s se comp�em das horas encontradas na tabela 'usuario'									
						  $semana = sectohour(datetosec($usuario[1]));
							$mes = sectohour(datetosec($usuario[2]));
					}													 
				 
				  // caso $pesq_pend obtenha algum resultado, o usu�rio possui horas pendentes
					$pesq_pend = mysql_query("SELECT * FROM pendentes WHERE nome='$usuario[0]'");

					$str_pendente = "00-00-00 00:00:00";		
					
					// para cada pendente obtida do usu�rio, soma as horas contidas na vari�vel $str_pendente
					// para obter o tempo total de horas pendentes			
					while ($pend = mysql_fetch_array($pesq_pend))
					{
              $datapend = tempo($pend[2],$pend[3]);
						  $datasoma = somadata($datapend['data'],$str_pendente);	
						  $str_pendente = $datasoma;				  
          }
					// transforma no formato de 'horas' a data total contida em $str_pendente
 					$pendente = datetohour($str_pendente);
					
				?>
<!--	Cada coluna apresente os resultados pesquisados no banco de dados, conforme o usu�rio estiver logado ou n�o -->
			      	<form action=<?php if ($logado) echo "\"logout.php\""; else echo "\"login.php\""; ?> method="POST">
							<tr onMouseOver="this.bgColor='#<?php if (!$logado) echo "D9ECFF"; else echo "#000099";?>';" onMouseOut="this.bgColor='#<?php if (!$logado) echo "F3F3F3"; else echo "000";?>';" <?php if ($logado) { ?> bgcolor="#000" style="color: #f3f3f3;" <?php } else{ ?> bgcolor="#f3f3f3" style="color: #000;" <?php } ?> >
										<td width="200"><?php echo $usuario[0]; ?></td>
									    <td align="center" width="70"><?php if ($logado) echo datetohour($datadif['data']); else echo "00h 00min";?></td>										                                     
									    <td align="center" width="70"><?php echo $semana; ?></td>
										<td align="center" width="70"><?php echo $mes; ?></td>
										<td align="center" width="70"><?php echo $pendente; ?></td>
										<td align="center">
										  						
										    <input type="submit" name="log2" value=<?php if ($logado) echo "\"Logout\""; else echo "\"Login\""; ?>  />

										    <input type="hidden" name="log"  value=<?php echo "\"$usuario[0]\"";?> >
										</td>
												<?php
												  if ($logado)
													{
												?>
												 <td align="center"><input type="submit" name="esqueci" value=<?php echo "\"Esqueci Logado\""; ?>></td>
												 <?php
												  }
													else
													{
												?>
												 <td><input type="submit" name="esqueci" disabled value=<?php echo "\"Esqueci Logado\""; ?>></td>
												 <?php 
												  }
												 ?>										  
								
							</tr>		
							</form>
		<?php
   	}	
		
		//fecha a conex�o com o banco de dados
		mysql_close($index);		
		?>
	</table>
	<br><br>
	<center>
	<table>
	<tr>
	<td>
	<form action="submeter_p.php" method="POST">
    <input type=submit name="submeter" value="Submeter Pendente">
  </form>
	</td>
	<td>
	<form action="admin.php" method="POST">
    <input type=submit name="administrar" value="�rea do Administrador">
  </form>
	</td>
	</tr>
	</table>
  </center>
</body>
</html>
