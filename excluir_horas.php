<?php
/* Administrar Usu�rios (alterar_usu.php)
1. Op��es de criar, editar e remover usu�rios do logador.
*/
  header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
  header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1
	
require_once("Globals.php");
//verifica se o administrador est� logado
checkCookie();
// realiza conex�o com o banco de dados
conecta();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Excluir Horas</title>
<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>


<?php
if(!isset($_GET['id']))
{
?>
<center><h3>Excluir Horas</h2></center>

<center>
	<table border=2 class='bordasimples'>
	<thead>
	<th width=100>Nome</th>
	</thead>

<?php

$sql = "SELECT login, id_user FROM usuario ORDER BY login ASC";
$rs = mysql_query($sql);

while($row = mysql_fetch_array($rs))
{
	echo "<tr class='tbrow'>";
	echo "<td><a href='?id=". $row['id_user'] ."'>". $row['login'] ."</a></td>";
	echo "</tr>";
}
?>
</table>
</center>
<?php
}
else
{
	$sql = "SELECT login FROM usuario WHERE id_user = ". $_GET['id'];
	$rs = mysql_query($sql);
	$nome = mysql_result($rs, 0, 'login');
	echo "<center><h3>Excluir Horas - ". $nome ."</h2></center>";
	$sql = "SELECT 
					id AS id_hist,
					id_user,
					DATE_FORMAT(inicio, '%d/%m/%Y - %Hh %imin') AS inicio,
					DATE_FORMAT(fim, '%d/%m/%Y - %Hh %imin') AS fim,
					IF(pendente = 0, '', 'Sim') AS pendente,
					IF(esq_logado = 0, '', 'Sim') AS esq_logado
			FROM (historico JOIN config)
			WHERE 
				id_user = ". $_GET['id'] ."
				AND
					(DATE(inicio) >= config.mes_anterior
					AND DATE(fim) < config.prox_mes)
			ORDER BY historico.inicio";
	$pesq_horas = mysql_query($sql) or die(mysql_error());
	
	if(mysql_num_rows($pesq_horas) == 0)
	{
		echo "<center style='color: #ff0000;'>N�o h� dados!!!</center>";
	}
	else
	{
		echo "<table border=2 class='bordasimples' align='center'>\n";
		echo "<thead>\n";
		echo "<th>In�cio</th>\n";
		echo "<th>Fim</th>\n";
		echo "<th>Pendente</th>\n";
		echo "<th>Esqueceu Logado</th>\n";
		echo "<th>Deletar</th>\n";
		echo "</thead>\n";
		
		while($usuario = mysql_fetch_array($pesq_horas))
		{
			echo "<form action='exclui_horas.php' method='POST'>";
			echo "<tr class='tbrow'>\n";
			echo "<td style='padding:0 20px'>". $usuario['inicio'] ."</td>\n";
			echo "<td style='padding:0 20px'>". $usuario['fim'] ."</td>\n";
			echo "<td style='padding:0 20px' align='center'>". $usuario['pendente'] ."</td>\n";
			echo "<td style='padding:0 20px' align='center'>". $usuario['esq_logado'] ."</td>\n";
			echo "<td><input type='submit' name='apagar' value='Deletar'></td>\n";
			echo "</tr>\n\n";
			echo "<input type='hidden' name='id_hist' value='". $usuario['id_hist'] ."'>";
			echo "<input type='hidden' name='id_user' value='". $usuario['id_user'] ."'>";
			echo "</form>";
		}
		echo "</table>\n";
	}
echo "<center><a href='?'><< Voltar para listagem</a></center>";
}
?>
<br>
<center>
<a href="admin.php"><< Administrador</a><br>
<a href="index.php"><< Logador </a><br>
</center>

</body>
</html>
