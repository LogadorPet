<?php

class ListaPessoas{
	public $lista = array();

	public function addPerson($pessoa)
	{
		$this->lista[] = $pessoa;
	}
	
	public function printList()
	{
		foreach($this->lista as $person)
		{
			$alt = ($person->isLogado())? "alt" : "";
			
			$form = "<form action='log.php' method='post'>\n";

			$form .= "<tr onMouseOver='mostra(event, \"". $person->getFoto() ."\",\"".  $person->getNome() ."\");' onMouseOut='tira()' class='tbrow ". $alt ."'>\n";
			//onMouseOver='this.bgColor=\"". $colorOver ."\"; mostra(event, \"". $person->getFoto() ."\",\"".  $person->getNome() ."\");' onMouseOut='this.bgColor=\"". $colorOut ."\"; tira();' bgcolor='". $colorOut ."' style='color: ". $colorDefault ."' 

			if($person->hasUnreadMessage())
			{
				$foto = "comment";
				$title = "Mensagens n�o lidas";
			}
			else
			{
				if($person->hasMessage())
				{
					$foto = "comment2";
					$title = "Sem mensagens novas";
				}
				else
				{
					$foto = "comment3";
					$title = "Sem mensagens";
				}			
			}
			$form .= "<td width='200'>";
			$form .= "<img src='images/". $foto .".png' title='". $title ."'  style='float:left'> ";

			$form .= "<a href='enviar_mensagem.php?id_to=". $person->getId() ."' target='_blank' onclick='return popup(this);' class='person' style='float:left'>";
			$form .= "<img src='images/send_message.gif' title='". $title ."' width='16px'>";
			$form .= "</a>";

			$form .= "<a href='detail_person.php?id=". $person->getId() ."' target='_blank' onclick='return popup(this);' class='person' style='float:left;padding-left:5px'>";
			$form .= $person->getNome();
			$form .= "</a>";
			$form .= "</td>\n";
			
			$form .= "<td align='center' width='70'>". $person->getNow() ."</td>\n";
			$form .= "<td align='center' width='70'>". $person->getSemana() ."</td>\n";
			$form .= "<td align='center' width='70'>". $person->getMes() ."</td>\n";
			$form .= "<td align='center' width='70'>". $person->getPendente() ."</td>\n";
			$form .= "<td align='center'>";
			
			$caption = ($person->isLogado())?"Logout" : "Login";
			$form .= "<input type='submit' name='log2' value='". $caption ."'>";
			
			$action = ($person->isLogado())?"logout" : "login";
			$form .= "<input type='hidden' name='action' value='". $action ."'>";
			$form .= "<input type='hidden' name='log'  value='". $person->getId() ."'>";
			$form .= "</td>\n";
			
			$disabled = ($person->isLogado())?"" : "disabled";
			$form .= "<td align='center'><input type='submit' name='esqueci' ". $disabled ." value='Esqueci Logado'></td>\n";
			$form .= "<td align='center' width='40'>". $person->getFaltas() ."</td>\n";
			$form .= "</tr>\n";
			$form .= "</form>\n\n";
			
			echo $form;

		}
	}
}

?>
