<?php
$ano = $_POST['ano'];
$mes = $_POST['mes'];
$dia = $_POST['dia'];
$hora = $_POST['hora'];
$minuto = $_POST['minuto'];

// se todos os dados estiverem dispon�veis para a informa��o de quando o usu�rio saiu e esqueceu logado
if (!(empty($ano) OR empty($mes) OR empty($dia) OR empty($hora) OR empty($minuto)))
{

   // realiza a conex�o com o banco de dados
   require_once("Globals.php");
   conecta();
	 
	 // obt�m os tempos em segundos do tempo atual e do tempo de sa�da
	 $logout = "$ano-$mes-$dia $hora:$minuto:00";
	 //$tempo_agora = strtotime("now");
	 //$tempo_logout = strtotime($logout);
	
	 //consulta abaixo retorna 0 resultados se o solicitante nao estiver logado ou se o final solicitado � anterior ao in�cio do logado
	 $sql = "SELECT id_user FROM historico
			 WHERE
				id_user = '". $_POST['log'] ."'
				AND fim is NULL
				AND inicio < '". $logout ."'
				AND '". $logout ."' < NOW()";
	 $rs = mysql_query($sql);
	 
	 if(mysql_affected_rows()==0)
	 {
		//houve uma falha -> o solicitante informou que esqueceu logado em um per�odo anterior ao que se logou de fato
		echo("Data inv�lida! <br>");
	    echo "<a href='javascript: history.back(-1)'>Voltar</a></body></html>";
		die();	
	 }
	 else
	 { //proceder com 'deslogamento'
	 
		$sql = "SELECT inicio FROM config, historico
				WHERE 
					(
						(inicio > mes_inicio AND '". $logout ."' > mes_inicio) 
						OR
						(inicio < mes_inicio AND '". $logout ."' < mes_inicio) 
					)
					AND id_user = '". $_POST['log'] ."' 
					AND fim is NULL;";
		$rs = mysql_query($sql); //consulta retorna resultado caso o tempo atual (deslogado) esteja no mes atual

		if(mysql_num_rows($rs) > 0) //meses iguais para logado e deslogado
		{ //deslogou no mesmo mes em que logou
			$sql = "UPDATE 
				historico
			SET
				fim = '". $logout ."',
				esq_logado = 1
			WHERE
				id_user = '". $_POST['log'] ."'
				AND fim is NULL";
			mysql_query($sql);
		}
		else
		{
			$sql = "UPDATE 
				historico
			SET
				fim = (SELECT (mes_inicio - INTERVAL 1 SECOND) FROM config),
				esq_logado = 1
			WHERE
				id_user = '". $_POST['log'] ."'
				AND fim is NULL";
			mysql_query($sql);
			$sql = "INSERT INTO historico (id_user, inicio, fim, esq_logado) VALUES ('". $_POST['log'] ."', (SELECT mes_inicio FROM config), '". $logout ."', 1)";
			mysql_query($sql);
		}
	 }
}
else // se nem todos os dados estiverem dispon�veis para a informa��o de quando o usu�rio saiu e esqueceu logado
{
  		header("Location: index.php");
}
?>
<script type="text/javascript">setTimeout("location.href = 'index.php';", 0);</script>