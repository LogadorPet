<?php
/* Administrar Pendentes e Pendentes Esquecidas (adm_p.php) 
1. Mostra a tabela com todas as pendentes existentes
2. Oferece bot�es de 'Aceitar' e 'Rejeitar' para cada pendente
3. Caso admin
*/
header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1

require_once("Globals.php");
//verifica se o administrador est� logado
checkCookie();
// realiza conex�o com o banco de dados
conecta();
	
if(isset($_GET['passado']))
{
	$title = "Pendentes Esquecidas";
	$sinal_sql = "<=";
	$action = "valida_p.php?passado";
}
else
{
	$title = "Administrar Pendentes";
	$sinal_sql = ">";
	$action = "valida_p.php";
}
?>

<html>
<head>
<title><?php echo $title; ?> - Logador PET</title>
<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
<center><h2> <?php echo $title; ?> </h2></center>				


<?php

	
	// obt�m todas as pendentes existentes da tabela pendentes
	$sql = "SELECT 
				pendentes.id AS pendId,
				usuario.login AS nome, 
				DATE_FORMAT(pendentes.inicio, '%d/%m/%Y %H:%i:%s') AS inicio, 
				UNIX_TIMESTAMP(inicio) as unix_inicio,
				DATE_FORMAT(pendentes.fim, '%d/%m/%Y %H:%i:%s') AS fim, 
				TIMEDIFF(pendentes.fim, pendentes.inicio) AS diferenca, 
				local, 
				justificativa 
			FROM 
				config,
				pendentes
			JOIN 
				usuario 
			ON usuario.id_user = pendentes.id_user
			WHERE fim ".$sinal_sql." config.mes_anterior
			ORDER BY unix_inicio ASC, nome ASC";
  $pesq_pend = mysql_query($sql);	
	
	// se existir pelo menos 1 pendente, cria a tabela de pendentes
  if (mysql_num_rows($pesq_pend)>0)
	{
?>
		
		 	<table border=2 class="bordasimples" align="center">
			 <thead>
			 		 <th width="100">
					  <center>
					 		 Nome
							 </center>
					 </th>
					 <th>
					  <center>
					 		 In�cio
							 </center>
					 </th>
					 <th>
					  <center>
					 		 Fim
							 </center>
					 </th>	
					 <th>
					  <center>
					 		 Tempo Total
							 </center>
					 </th>					 
					 <th width="100">
					 <center>
					 		 Local
					 </center>
					 </th>
					 <th width="200">
					  <center>
					 		 Justificativa
							 </center>
					 </th>
					 <th>
					  <center>
					 		 &nbsp;
					  </center>
					 </th>
					 <th>
					  <center>
					 		 &nbsp;
					  </center>
					 </th>
				</thead>
 
       <?php
	
		while($usuario = mysql_fetch_array($pesq_pend))
		{
 		?>		
		<form action="<?php echo $action; ?>" method="POST">
		<input type="hidden" name="pendId" value="<?php echo $usuario['pendId'];?>" />
		<tr class='tbrow'>
				 <td><?php echo $usuario['nome']; ?></td>
				 <td><?php echo $usuario['inicio']; ?></td>
				 <td><?php echo $usuario['fim']; ?></td>
				 <td><center><?php echo $usuario['diferenca']; ?></center></td>
				 <td><?php echo $usuario['local']; ?></td>
				 <td><?php echo $usuario['justificativa']; ?></td>
				 <td>
					<input type="submit" name="val" value="Aceitar">									 
				 </td>
				 <td>
					<input type="submit" name="val" value="Rejeitar">
				 </td>							 
		</tr>
		</form>
		
		
		<?php
		}
		?>
		
		</table>
		<?php

	} // caso n�o haja nenhuma pendente
	else 
	{
		echo "<center  style='color: #ff0000;'>Sem pendentes!!!</center>";
	}	
	echo "<br><br>";
	?>
	<center>
			<a href="admin.php"><< Administrador</a><br>
			<a href="index.php"><< Logador</a>
	</center>






</body>
</html>
