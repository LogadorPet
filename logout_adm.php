<?php
/* Logout_adm.php
1- Realiza o logout do administrador, retirando o cookei do mesmo do pc do usu�rio
*/
  header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
  header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1
	
  // se cookie existe, administrador est� logado
  if (isset($_COOKIE['c_adm']))
	{	  
    // retira o cookie do pc do usu�rio
	  setcookie("c_adm","",time() - 300);
	// redireciona o browser do usu�rio para o logador
	header("Location: index.php");
	}
	else // caso cookie n�o exista, administrador j� n�o est� logado e redireciona o usu�rio para o logador
	{
			header("Location: index.php");
	}	  
?>


	
