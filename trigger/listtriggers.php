<?php

require_once('config.php');
require_once('triggerdao.php');

$triggerDAO = new TriggerDAO($db);
$root = array();
$at = $at_cfg->getTemplate("listtriggers.php");
$root["triggers"] = $triggerDAO->get();
$at->process($root);

print $at->getOut();
?>