<?php

require_once('config.php');
require_once('triggerdao.php');

$triggerDAO = new TriggerDAO($db);

if ($_POST['trigger'])
{
	$trigger = $_POST['trigger'];
	$sql = "create trigger ".$trigger['trigger']." ".$trigger['timing']." ".$trigger['event']." on ".$trigger['table']." FOR EACH ROW ".stripslashes($trigger['statement']);

	// delete first
	$triggerDAO->delete($trigger['trigger']);
	if ($triggerDAO->add($sql))
	{
		header("Location: ./listtriggers.php"); 
	}
	else
	{
		print mysql_error();
	}
}
else
{
	$root = array();
	$at = $at_cfg->getTemplate("addtrigger.php");
	$root['tables'] = $triggerDAO->getTables();
	$root['trigger'] = $triggerDAO->getTrigger($_GET['name']);
	//print_r($root);
	$at->process($root);
	print $at->getOut();
}
?>