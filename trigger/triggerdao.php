<?php

/**
 * 
 *
 */
class TriggerDAO
{

	private $dbh;

    public function __construct($dbsetting)
    {
		$this->dbh = mysql_connect($dbsetting["server"], $dbsetting["user"], $dbsetting["pwd"]);
		mysql_select_db($dbsetting['database']);
    }

	public function __destruct()
	{
		mysql_close($this->dbh);
	}

	public function get($like = '')
    {
		if ($like == '')
		{
			return $this->_fetchAll("show triggers");
		}
		else
		{
			return $this->_fetchAll("show triggers like '%".$like."%'");
		}
    }

	public function add($sql)
	{
		return $this->_query($sql);
	}

    public function update($sql)
	{
		return $this->_query($sql);
	}

    public function delete($trigger_name)
	{
		return $this->_query("drop TRIGGER ".$trigger_name);
	}

	public function getTables()
	{
		return $this->_fetchAll("show tables");
	}
	
	public function getTrigger($name)
	{
		$triggers = $this->_fetchAll("show triggers");
		foreach($triggers as $trigger)
		{
			if (strtolower($name) == strtolower($trigger[0]))
			{
				return $trigger;
				break;
			}
		}
		return null;
	}

	private function _query($sql)
	{
		return mysql_query($sql, $this->dbh);
	}

	private function _fetchAll($sql)
	{
		$ret = array();
		$query = mysql_query($sql, $this->dbh);
		if ($query)
		{
			while($row = mysql_fetch_row($query))
			{
				$ret[] = $row;
			}
		}
		return $ret;
	}
}

?>