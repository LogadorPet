<?php
include_once(dirname(__FILE__)."/header.php");
?>
<script language="JavaScript" src="./js/vf/vf.js" type="text/javascript"></script>
<div id="Region4">
<FORM METHOD="POST" ACTION="" name="mainform">
<fieldset>
<legend>Add Trigger Form</legend>
<table id="Login_Form" width="100%">
<tr>
	<td align="right">Trigger Name:</td>
	<td><input type="text" name="trigger[trigger]" size="30" value="<?php echo $trigger[0];?>"/></td>
</tr>
<tr>
	<td align="right">Table:</td>
	<td>
		<select name="trigger[table]">
			<?php
			foreach($tables as $table)
			{
			?>
			<option value="<?php echo $table[0]; ?>" <?=$trigger[2]==$table[0] ? "selected":"" ?>><?php echo $table[0]; ?></option>
			<?php
			}
			?>
		</select>
	</td>
</tr>
<tr>
	<td align="right">Event:</td>
	<td>
		<select name="trigger[event]">
			<option value="INSERT" <?=strtoupper($trigger[1])=='INSERT' ? "selected":"" ?>>INSERT</option>
			<option value="UPDATE" <?=strtoupper($trigger[1])=='UPDATE' ? "selected":"" ?>>UPDATE</option>
			<option value="DELETE" <?=strtoupper($trigger[1])=='DELETE' ? "selected":"" ?>>DELETE</option>
		</select>
	</td>
</tr>
<tr>
	<td align="right">Timging:</td>
	<td>
		<select name="trigger[timing]">
			<option value="BEFORE" <?=strtoupper($trigger[4])=='BEFORE' ? "selected":"" ?>>BEFORE</option>
			<option value="AFTER" <?=strtoupper($trigger[4])=='AFTER' ? "selected":"" ?>>AFTER</option>
		</select>
	</td>
</tr>
<tr>
	<td align="right">Definer:</td>
	<td><input type="text" name="trigger[definer]" size="30" value="<?php echo $trigger[7];?>" disabled/></td>
</tr>
<tr>
	<td align="right">Statement:</td>
	<td>
	<textarea NAME="trigger[statement]" ROWS="5" COLS="35"><?php echo $trigger[3];?></textarea>
	</td>
</tr>
<tr>
	<td align="center" colspan="2"><input type="submit" value="  Submit Module  " /></td>
</tr>
</table>
</fieldset>
</FORM>
</div>
<script language="JavaScript" type="text/javascript">
	var frm  = new Validator("mainform");
	frm.addV("trigger[trigger]","req","Please enter trigger name.");
	frm.addV("trigger[statement]","req","Please enter trigger statement.");
</script>
<?php
include_once(dirname(__FILE__)."/footer.php");
?>