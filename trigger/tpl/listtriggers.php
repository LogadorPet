<?php
include_once(dirname(__FILE__)."/header.php");
?>
<div id="Region4">
<div class="ListTitle">
	Triggers List
</div>
<div class="listdiv">
<table class="listtable" cellspacing="1" cellpadding="2">
	<tr>
		<th>
		Name
		</th>
		<th>
		Event
		</th>
		<th>
		Table
		</th>
		<th>
		Statement
		</th>
		<th>
		Timing
		</th>
		<th>
		Created
		</th>
<!--		<th>
		sql_mode
		</th> -->
		<th>
		Definer
		</th>
		<th style="width:100px">
		Operation
		</th>
	</tr>
<?php
$items = $triggers;
foreach($items as $item)
{
?>
	<tr>
		<td>
		<?php echo $item[0];?>
		</td>
		<td>
		<?php echo $item[1];?>
		</td>
		<td>
		<?php echo $item[2];?>
		</td>
		<td>
		<?php echo $item[3];?>
		</td>
		<td>
		<?php echo $item[4];?>
		</td>
		<td>
		<?php echo $item[5];?>
		</td>
<!--		<td>
		<?php echo $item['sql_mode'];?>
		</td> -->
		<td>
		<?php echo $item[7];?>
		</td>
		<td align="center">
		<a href="./edittrigger.php?name=<?php echo $item[0];?>">Edit</a>
		|
		<a href="./deletetrigger.php?name=<?php echo $item[0];?>" onclick="javascript: return confirm('are you sure to delete this trigger?');">Delete</a>
		</td>
	</tr>
<?php
}
?>
</table>
</div>
</div>
<?php
include_once(dirname(__FILE__)."/footer.php");
?>