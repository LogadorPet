<?php
/* Administrar Usu�rios (alterar_usu.php)
1. Op��es de criar, editar e remover usu�rios do logador.
*/
  header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
  header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1
	
require_once("Globals.php");
//verifica se o administrador est� logado
checkCookie();
// realiza conex�o com o banco de dados
conecta();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Alterar Usu�rios</title>
<link rel="stylesheet" type="text/css" href="estilo.css">
<script language="JavaScript" type="text/javascript">
function confirma_exclusao(nome)
{
	if(confirm('Voc� confirma que "'+ nome +'" n�o � mais Petiano?'))
		return true;
	else
		return false;
}
</script>
</head>
<body>
<center><h3>Alterar Usu�rios</h2></center>

<center>
	<table border=2 class='bordasimples'>
	<thead>
	<th width=100>Nome</th>
	<th width=50>Editar</th>
	<th width=50>Excluir</th>
	</thead>

<?php

$sql = "SELECT login, id_user FROM usuario ORDER BY login ASC";
$rs = mysql_query($sql);

while($row = mysql_fetch_array($rs))
{
	echo "<tr class='tbrow'><td>";
	echo $row['login'];	
	echo "</td>";
	echo "<td align='center'><a href='form_usu.php?id=". $row['id_user'] ."'><img src='images/page_edit.png'></a></td>";
	echo "<td align='center'><a href='altera_usu.php?id=". $row['id_user'] ."&deleta' onclick='return confirma_exclusao(\"". $row['login'] ."\")'><img src='images/delete.png'></a></td>";
	echo "</tr>";
}
?>
</table>
</center>

<br>

<center><h3>Cadastrar Novo</h3></center>


<center>
<form action='altera_usu.php' method="post">
<input type='hidden' name='action' value='ins'>
<table border=2 class='bordasimples'>
<tr bgcolor="#990033" style="color: #fff;" align="center">
<th width=200 colspan="2">Novo usu�rio</th>
</tr>
 
<tr class='tbrow'>
<td>Apelido</td>
<td><input type="text" name="user"></td>
</tr>

<tr class='tbrow'>
<td>Nome Completo</td>
<td><input type="text" name="nome"></td>
</tr>

<tr class='tbrow'>
<td>Telefone</td>
<td><input type="text" name="telefone"></td>
</tr>

<tr class='tbrow'>
<td>E-mail</td>
<td><input type="text" name="email"></td>
</tr>

<tr class='tbrow'>
<td>Foto*</td>
<td><input type="text" name="foto"></td>
</tr>

<tr class='tbrow' align=center>
<td colspan=2><input type="submit" value="Enviar">&nbsp;&nbsp;<input type="reset" value="Apagar Dados"></td>
</tr>

<tr class='tbrow'>
<td colspan=2 style='font-size:14px'>* deve copiar a foto para a pasta <i>fotos</i> do logador</td>
</tr>


</table>
</form>



<center>
<a href="admin.php"><< Administrador</a><br>
<a href="index.php"><< Logador </a><br>
</center>

</body>
</html>
