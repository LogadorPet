<?php
/* Valida Pendentes (valida_p.php)
1. Verifica se a pendente � v�lida
2. Se a op��o for aceitar a pendente, insere-a no banco de dados e atualiza hor�rios da semana correspondente
3. Se a op��o for rejeitar a pendente, simplesmente deleta-a do banco de dados
*/
require_once("Globals.php");
//verifica se o administrador est� logado
checkCookie(REDIRECT_ONLY);


   // se todas as informa��es foram enviadas corretamente da p�gina adm_p.php
   if (!empty($_POST['pendId']) && !empty($_POST['val']))
	{
	// realiza conex�o com o banco de dados
	conecta();
	
	// realiza a pesquisa sobre a pendente selecionada em adm_p.php
	 $pesq_pend = mysql_query("SELECT id_user, inicio, fim FROM pendentes WHERE id='". $_POST['pendId'] ."'");
	 
	 // se a op��o escolhida para a determinada pendente foi 'Aceitar', trata este caso
	 if ($_POST['val']=="Aceitar") 
		{ 
			// se a pendente pesquisa pelo id realmente existe
			if ($pendente = mysql_fetch_array($pesq_pend) )
			{								 	  
				$sql = "SELECT 1 FROM config
				WHERE 
					(
						('". $pendente['inicio'] ."' > mes_inicio AND '". $pendente['fim'] ."' > mes_inicio) 
						OR
						('". $pendente['inicio'] ."' < mes_inicio AND '". $pendente['fim'] ."' < mes_inicio) 
					);";
				$rs = mysql_query($sql);
				if(mysql_num_rows($rs) > 0) //inicio e fim estao no mesmo mes
				{
					$sql = "INSERT INTO historico (id_user, inicio, fim, pendente) VALUES ('". $pendente['id_user'] ."', '". $pendente['inicio'] ."', '". $pendente['fim'] ."', 1)";
					mysql_query($sql);
				}
				else
				{
					$sql = "INSERT INTO historico (id_user, inicio, fim, pendente) VALUES ('". $pendente['id_user'] ."', '". $pendente['inicio'] ."', (SELECT (mes_inicio - INTERVAL 1 SECOND) FROM config), 1)";
					mysql_query($sql);
					$sql = "INSERT INTO historico (id_user, inicio, fim, pendente) VALUES ('". $pendente['id_user'] ."', (SELECT mes_inicio FROM config), '". $pendente['fim'] ."', 1)";
					mysql_query($sql);
				}
				mysql_query("DELETE FROM pendentes WHERE id = '". $_POST['pendId'] ."'") or die ("Erro!! Query n�o efetuado!!");
				
				// Redireciona o browser do usu�rio para a p�gina adm_p.php
				$action = "adm_p.php";
				if(isset($_GET['passado']))
					$action .= "?passado";
				header ("Location: ". $action);												
			}		
			else // se pendente procurada n�o foi encontrada
			{ 
				die ("erro!!! pendente n�o encontrada no BD!!!");
			}	
		 }				
		else // caso o administrador tenha 'Rejeitado' a pendente espec�fica
		{
			 // se pendente realmente existe
			 if ($pendente = mysql_fetch_array ($pesq_pend))
			 {
				// deleta a pendente do banco de dados
				mysql_query("DELETE FROM pendentes WHERE id = '". $_POST['pendId'] ."'");

				// redireciona o browser do usu�rio para a p�gina adm_p.php				 				 
				header ("Location:adm_p.php");					
			 }		
			 else // se a pendente n�o existia, informa erro
			 { 
				die ("erro!!! pendente n�o encontrada no BD!!!");
			 }
		}					
   }								
?>