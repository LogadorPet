<?php
header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1

require_once("Globals.php");
//verifica se o administrador est� logado
checkCookie();
// realiza conex�o com o banco de dados
conecta();

if(isset($_GET['passado']))
{
	$title = "Ver Horas Semanais Passadas";
	$campo_sql = "mes_anterior";
	$prox_mes = "<td class='hover' onClick=\"document.location.href='?';\"> >> </td>";
	$mes_anterior = "";
}
else
{
	$title = "Ver Horas Semanais";
	$campo_sql = "mes_inicio";
	$prox_mes = "";
	$mes_anterior = "<td class='hover' onClick=\"document.location.href='?passado';\"> << </td>";
}
?>
<html>
<head>
<title>�rea do Administrador</title>
<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
<?php
	$sql = "SELECT
				(DATEDIFF(semana_inicio, $campo_sql) div 7)+1 AS num_semana,
				DATE_FORMAT(config.$campo_sql + INTERVAL 0 WEEK, '%d/%m/%Y') AS iniSem1,
				DATE_FORMAT(config.$campo_sql + INTERVAL 1 WEEK - INTERVAL 1 DAY, '%d/%m/%Y') AS fimSem1,
				DATE_FORMAT(config.$campo_sql + INTERVAL 1 WEEK, '%d/%m/%Y') AS iniSem2,
				DATE_FORMAT(config.$campo_sql + INTERVAL 2 WEEK - INTERVAL 1 DAY, '%d/%m/%Y') AS fimSem2,
				DATE_FORMAT(config.$campo_sql + INTERVAL 2 WEEK, '%d/%m/%Y') AS iniSem3,
				DATE_FORMAT(config.$campo_sql + INTERVAL 3 WEEK - INTERVAL 1 DAY, '%d/%m/%Y') AS fimSem3,
				DATE_FORMAT(config.$campo_sql + INTERVAL 3 WEEK, '%d/%m/%Y') AS iniSem4,
				DATE_FORMAT(config.$campo_sql + INTERVAL 4 WEEK - INTERVAL 1 DAY, '%d/%m/%Y') AS fimSem4
			FROM config";
	$rs = mysql_query($sql);
	?>
	
	<center><h2><?php echo $title ." (". mysql_result($rs, 0, 'iniSem1') ." - ". mysql_result($rs, 0, 'fimSem4') .")";  ?></h2></center>
	
	<?php

	$select = array();
		for($i=1;$i<5;$i++)
		{
			$col = "tempoSem". $i;
			$select[] = "IF( $col is NULL, '<span class=\'red\'>00h 00min</span>', (IF($col < '06:00:00', TIME_FORMAT($col, '<span class=\'red\'>%Hh %imin</span>'), TIME_FORMAT($col, '%Hh %imin')))) AS $col";
		}	
		 $sql = "SELECT login, 
						". implode($select, ', ') ."
				FROM
						(
							(SELECT id_user, login
								FROM usuario) AS usuarios
						LEFT JOIN
							(SELECT id_user, sec_to_time(sum(tempo)) AS tempoSem1 FROM 
							(
								SELECT 
									id_user, time_to_sec(timediff(fim, inicio)) AS tempo 
								FROM 
									historico JOIN config
							WHERE ( ( DATE(historico.inicio) >= DATE(config.$campo_sql + INTERVAL 0 WEEK) ) AND ( DATE(historico.inicio) < DATE(config.$campo_sql + INTERVAL 1 WEEK) ) )
) AS sem1
							GROUP BY id_user) AS sem1
						USING (id_user)	
						LEFT JOIN
							(SELECT id_user, sec_to_time(sum(tempo)) AS tempoSem2 FROM 
							(
								SELECT 
									id_user, time_to_sec(timediff(fim, inicio)) AS tempo 
								FROM 
									historico JOIN config
								WHERE ( ( DATE(historico.inicio) >= DATE(config.$campo_sql + INTERVAL 1 WEEK) ) AND ( DATE(historico.inicio) < DATE(config.$campo_sql + INTERVAL 2 WEEK) ) )
							) AS sem2
							GROUP BY id_user) AS sem2
						USING (id_user)
						LEFT JOIN
							(SELECT id_user, sec_to_time(sum(tempo)) AS tempoSem3 FROM 
							(
								SELECT 
									id_user, time_to_sec(timediff(fim, inicio)) AS tempo 
								FROM 
									historico JOIN config
								WHERE ( ( DATE(historico.inicio) >= DATE(config.$campo_sql + INTERVAL 2 WEEK) ) AND ( DATE(historico.inicio) < DATE(config.$campo_sql + INTERVAL 3 WEEK) ) )
							) AS sem3
							GROUP BY id_user) AS sem3
						USING (id_user)
						LEFT JOIN
							(SELECT id_user, sec_to_time(sum(tempo)) AS tempoSem4 FROM 
							(
								SELECT 
									id_user, time_to_sec(timediff(fim, inicio)) AS tempo 
								FROM 
									historico JOIN config
								WHERE ( ( DATE(historico.inicio) >= DATE(config.$campo_sql + INTERVAL 3 WEEK) ) AND ( DATE(historico.inicio) < DATE(config.$campo_sql + INTERVAL 4 WEEK) ) )
							) AS sem4
							GROUP BY id_user) AS sem4
						USING (id_user)
						)
				ORDER BY login";
	  $pesq_horas = mysql_query($sql);	
	  
	if(mysql_num_rows($pesq_horas) == 0)
	{
		echo "<center style='color: #ff0000;'>N�o h� dados para este per�odo!!!</center>";
		echo "<table align='center'>$mes_anterior $prox_mes</table>";
	}
	else
	{
		echo "<div style='width: 900px;margin:0 auto;'>\n";
		echo "<table border=0 class='bordasimples' align='center'>\n";
		echo "<tr>\n";
		echo $mes_anterior;
		echo "<td>\n";
		echo "<table border=2 class='bordasimples' align='center'>\n";
		echo "<thead>\n";
		echo "<th>Nome</th>\n";
		
		  $num_semana = mysql_result($rs, 0, 'num_semana');
		  for($i=1;$i<5;$i++)
		  {
			if($num_semana == $i)
				$class = "style='background-color:#000099'";
			else
				$class = "";
			echo "<th ". $class .">Sem. ". $i ."<br>";
			echo "(". mysql_result($rs, 0, 'iniSem'.$i) ." - ". mysql_result($rs, 0, 'fimSem'.$i) .")";
		  }
		echo "<th>&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;</th>";
		echo "</thead>\n";
		while($usuario = mysql_fetch_array($pesq_horas))
		{
			echo "<tr class='tbrow'>\n";
			echo "<td>". $usuario['login'] ."</td>\n";
			for($i=1;$i<5;$i++)
				echo "<td>". $usuario['tempoSem'.$i] ."</td>";

			$total = calcTotal($usuario);

			echo "<td>".$total."</td>";

			echo "</tr>\n";
		}
		echo "</table>\n";
		echo $prox_mes;
		echo "</tr>\n";
		echo "</table>\n";
		echo "</div>";
	}

	function calcTotal($usuario) {

		$total = 0;
		for($i=1;$i<5;$i++) {

			$total+=getMinutes($usuario['tempoSem'.$i]);
		}
		$horas = (int) ($total/60);
		$minutos = $total%60;
		if($horas < 48)
			$res = "<font color=#FF0000>".$horas."h ".$minutos."min</font>";
		else
			$res = $horas."h ".$minutos."min";
		return $res;
	}

	function getMinutes($stringHoras) {

		$horas = 0;
		$minutos = 0;
		$n = 0;
		$res = 0;

		$n = sscanf($stringHoras, "%dh %dmin", $horas, $minutos);

		if($n == 2) {

			$res+=$horas*60 + $minutos;
		}
		return $res;
	}

	?>
	
	<br>
	
	<div style="clear:both"></div>
	<center>
			<a href="admin.php"><< Administrador</a><br>
			<a href="index.php"><< Logador</a>
	</center>
</body>
</html>
