<!-- Log (login/logout)
1. Insere a informa��o de usu�rio logado/deslogado no banco de dados
2. Redireciona o browser para o logador (index.php) novamente
-->

<?php	
	require_once("Globals.php");
	
	$title = "";
	$head = "";
	$body = "";
	
	// realiza a conex�o com o banco de dados
	conecta();
	
	//se o usuario nao esqueceu logado
	if(!isset($_POST['esqueci']))
	{
		//verifica se � para logar ou deslogar
		switch($_POST['action'])
		{
			case 'login':
					$pesq_usuario = mysql_query("SELECT inicio FROM historico WHERE id_user='". $_POST['log'] ."' AND fim is NULL");
					if ($usu_logado = mysql_fetch_array($pesq_usuario))
					{
						//usuario ja estava logado 
						$body .= "Voc� j� estava logado.<br>";
					}
					else
					{	  
						// insere no banco de dados uma linha contendo a informa��o de que o usu�rio se logou
						// campo fim fica com NULL (o que determinar� que o usu�rio ainda est� logado)
						$result = mysql_query("INSERT INTO historico ( id_user, inicio , pendente ) VALUES ('". $_POST['log'] ."' , NOW() , 0 )") or die ("n�o inseriu");	
					}
					break;
					
			case 'logout':
					// realiza uma pesquisa para encontrar uma linha no hist�rico com valor NULL em `fim` para o determinado usu�rio
					 // se encontrar, usu�rio estava, de fato, logado
					$pesq_hist = mysql_query("SELECT inicio FROM historico WHERE id_user='". $_POST['log'] ."' and fim is NULL");
					if (mysql_num_rows($pesq_hist)!=0)
					{
						$sql = "SELECT mes_inicio FROM config, historico WHERE inicio >= mes_inicio AND id_user = '". $_POST['log'] ."' and fim is NULL;";
						$rs = mysql_query($sql); //consulta retorna resultado caso o tempo atual (deslogado) esteja no mes atual
					
						if(mysql_num_rows($rs) > 0)
						{ //deslogou no mesmo mes em que logou
							mysql_query("UPDATE historico SET fim = NOW() WHERE id_user = '". $_POST['log'] ."' and fim is NULL") or die ("n�o deu update");
						}
						else
						{
							$sql = "UPDATE historico SET fim = (SELECT (mes_inicio - INTERVAL 1 SECOND) FROM config) WHERE id_user = '". $_POST['log'] ."' and fim is NULL";
							mysql_query($sql);
							$sql = "INSERT INTO historico (id_user, inicio, fim) VALUES ('". $_POST['log'] ."', (SELECT mes_inicio FROM config), NOW())";
							mysql_query($sql);
						}
						
					}
					else
					{
						$body .= "Voc� j� estava deslogado.<br>";
					}
					break;
		}
		$title = $_POST['log2'];
		// redireciona o browser do usu�rio para o logador.php novamente
		$head = "<script type='text/javascript'>setTimeout(\"location.href = 'index.php';\", 0);</script>";
		$body .= "<a href=\"index.php\"><< Logador</a>";

	}	
	else
	//deve imprimir a pagina para 'esqueci logado'
	{
		$sql = "SELECT 
					usuario.login AS 'nome', 
					DATE_FORMAT(historico.inicio, '%d/%m/%y - %H:%i') AS desde,
					DATE_FORMAT(NOW(), '%d/%m/%y - %H:%i') AS agora
				FROM 
					usuario, 
					historico 
				WHERE usuario.id_user = '". $_POST['log'] ."' 
					AND usuario.id_user = historico.id_user 
					AND historico.fim is NULL";
		$rs = mysql_query($sql);
		$nome = mysql_result($rs, 0, 'nome');
		$data = mysql_result($rs, 0, 'desde');
		$now = mysql_result($rs, 0, 'agora');
		
		$meses = array("janeiro","fevereiro","mar�o","abril","maio","junho","julho","agosto","setembro","outubro","novembro","dezembro");
		
		$title = "Esqueci Logado";
		$head .= "<link type='text/css' rel='stylesheet' href='dhtmlgoodies_calendar/dhtmlgoodies_calendar.css'></LINK>";
		$head .= "<script type='text/javascript' src='dhtmlgoodies_calendar/dhtmlgoodies_calendar.js'></script>";
		
		$body .= "<center><h2>Esqueci Logado</h2></center>\n";
		$body .= "<form action='esqc_log.php' method='POST'>\n";
		$body .= "<center>\nNome: ". $nome ."<br>\n";
		$body .= "Logado desde: ". $data ."<br>\n";
		$body .= "Hor�rio atual: ". $now."<br>\n";
		$body .= "<br>";
		
		$body .= "<table border=1>
					 <tr>
						<td width='50'></td>
						<td>Dia</td>
						<td>M�s</td>
						<td>Ano</td>
						<td>Hora</td>
						<td>Minutos</td>
					</tr>
					<tr>
						<td>Saiu �s</td>
						<td><select name='dia'>";
		for($x=1;$x<=31;$x++)
			{
				$body .= "<option value='". $x ."'";
				if($x == date('j')) $body .= " selected";
				$body .= ">";
				if($x<10) $body .= "0";
				$body .= $x;
				$body .= "\n";
			}
						
		$body .= "		</select></td>
						<td><select name='mes'>";
		for ($i = 1; $i <= sizeof($meses); $i++)
		{
	    if ($i < 10)
			$str = "0$i";
		else
			$str = $i;
		$body .= "<option value='$str' ";
		if ($str == date('m'))
			$body .= "selected";
		$body .= ">" . $meses[$i-1];
		}
		
		$body .= "		</select></td>
						<td><select name='ano'>";
		for ($x=date('Y')-1;$x<=date('Y')+1;$x++)
			{
				$body .= "<option value='". $x ."'";
				if($x==date('Y')) $body .= " selected";
				$body .= ">$x";
			}
		$body .= "		</select></td>
						<td><select name='hora'>";
						
		for ($x=0;$x<=23;$x++)
			{
				if ($x < 10)
					$str = "0$x";
				else
					$str = $x;
				$body .= "<option value='". $str ."'";
				if($str == date('H')) $body .= " selected";
					$body .= ">";
				$body .= $str;
				$body .= "\n";
			}		
	
		$body .= "		</select></td>
						<td><select name='minuto'>";
						
		for ($x=0;$x<=59;$x++)
			{
				if ($x < 10)
					$str = "0$x";
				else
					$str = $x;
				$body .= "<option value='". $str ."'";
				if($str == date('i')) $body .= " selected";
					$body .= ">";
				$body .= $str;
				$body .= "\n";
			}	
						
						
		$body .= "		</select></td>
						<td><input type=\"button\" value='calend&aacute;rio' onclick=\"displayCalendarSelectBox(document.forms[0].ano, document.forms[0].mes, document.forms[0].dia, document.forms[0].hora, document.forms[0].minuto, this)\"></td>
						<!--
						<td><input type=\"text\" readonly name=\"date_complete\"></td>
						<td><input type=\"button\" onclick=\"displayCalendar(document.forms[0].date_complete,'dd/mm/yyyy hh:ii',this,true)\"></td>
						-->
						</tr></table>
						<input type='submit' value='Enviar'>
						<input type='hidden' name='log' value='". $_POST['log'] ."'>";
	}

	
	$html .= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
	$html .= "<html xmlns=\"http://www.w3.org/1999/xhtml\">";
	$html .= "<head>";
	$html .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />";
	$html .= "<title>". $title ."</title>";
	$html .= $head;
	$html .= "</head>";
	$html .= "<body>";
	$html .= $body;
	$html .= "</body>";
	$html .= "</html>";

	echo $html;
?>



