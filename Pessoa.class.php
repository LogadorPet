<?php

class Pessoa{
	private $id;
	private $nome;
	private $logado;
	private $foto;
	private $agora;
	private $semana;
	private $mes;
	private $pendentes;
	private $faltas;
	private $message = false;
	private $unreadMessage = false;
	
	public function setNome($name) { $this->nome = $name; }
	public function getNome() { return $this->nome; }
	
	public function setId($id) { $this->id = $id; }
	public function getId() { return $this->id; }
	
	public function setNow($time) { $this->agora = $time; }
	public function getNow() { return $this->agora; }

	public function setSemana($time) { $this->semana = $time; }
	public function getSemana() { return $this->semana; }
	
	public function setMes($time) { $this->mes = $time; }
	public function getMes() { return $this->mes; }
	
	public function setPendente($time) { $this->pendentes = $time; }
	public function getPendente() { return $this->pendentes; }
	
	public function setFaltas($num) { $this->faltas = $num; }
	public function getFaltas() { return $this->faltas; }
	
	public function setFoto($src) { $this->foto = $src; }
	public function getFoto() { return $this->foto; }
	
	public function isLogado(){ return $this->logado; }
	public function setLogado($value) {
		if($value)
			$this->logado = true;
		else
			$this->logado = false;
	}
	
	public function setMessage() { $this->message = true; }
	public function hasMessage() { return $this->message; }

	public function setUnreadMessage() { $this->unreadMessage = true; }
	public function hasUnreadMessage() { return $this->unreadMessage; }
 
}

?>