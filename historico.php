<?php
/* Visualizar o historico dos usu�rios (historico.php)
1. Apresenta em uma tabela cada registro da tabela historico do banco de dados
*/
header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1

require_once("Globals.php");
//verifica se o administrador est� logado
checkCookie();
// realiza conex�o com o banco de dados
conecta();

if(isset($_GET['passado']))
{
	$title = "Hist�rico M�s Passado";
	$inicio_sql = "mes_anterior";
	$fim_sql = "mes_inicio";	
	$prox_mes = "<td class='hover' onClick=\"document.location.href='?';\"> >> </td>";
	$mes_anterior = "";
}
else
{
	$title = "Hist�rico M�s Atual";
	$inicio_sql = "mes_inicio";
	$fim_sql = "prox_mes";
	$prox_mes = "";
	$mes_anterior = "<td class='hover' onClick=\"document.location.href='?passado';\"> << </td>";
}
?>

<html>
<head>
<title><?php echo $title;?></title>
<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>


<?php
  $sql = "SELECT
				DATE_FORMAT(config.$inicio_sql, '%d/%m/%Y') AS inicio,
				DATE_FORMAT(config.$fim_sql, '%d/%m/%Y') AS fim
			FROM config";
	$rs = mysql_query($sql);
	$inicio = mysql_result($rs, 0, 'inicio');
	$fim = mysql_result($rs, 0, 'fim');
	
	?>
	<center><h2><?php echo "$title ($inicio - $fim)";  ?></h2></center>
	
	<?php
	$sql = "SELECT login,
						DATE_FORMAT(horas.inicio, '%d/%m/%Y - %Hh %imin') AS inicio,
						DATE_FORMAT(horas.fim, '%d/%m/%Y - %Hh %imin') AS fim,
						IF(horas.pendente = 0, '', 'Sim') AS pendente,
						IF(horas.esq_logado = 0, '', 'Sim') AS esq_logado
				FROM
						(
							(SELECT id_user, login
								FROM usuario) AS usuarios
						JOIN
							(
							SELECT id_user, inicio, fim, pendente, esq_logado
							FROM historico JOIN config
							WHERE (DATE(inicio) >= config.$inicio_sql
								AND DATE(fim) < config.$fim_sql)
							) AS horas
						USING (id_user)
						)
				ORDER BY login, horas.inicio";
	$pesq_horas = mysql_query($sql);
	
	if(mysql_num_rows($pesq_horas) == 0)
	{
		echo "<center style='color: #ff0000;'>N�o h� dados para este per�odo!!!</center>";
		echo "<table align='center'>$mes_anterior $prox_mes</table>";
	}
	else
	{
		echo "<div style='width: 900px;margin:0 auto;'>\n";
		echo "<table border=0 class='bordasimples' align='center'>\n";
		echo "<tr>\n";
		echo $mes_anterior;
		echo "<td>\n";
		echo "<table border=2 class='bordasimples' align='center'>\n";
		echo "<thead>\n";
		echo "<th>Nome</th>\n";
		echo "<th>In�cio</th>\n";
		echo "<th>Fim</th>\n";
		echo "<th>Pendente</th>\n";
		echo "<th>Esqueceu Logado</th>\n";
		echo "</thead>\n";
		
		$name_old = "";
		while($usuario = mysql_fetch_array($pesq_horas))
		{
			if($name_old!=$usuario['login'])
				{
					$name_old = $usuario['login'];
					echo "<tr class='tbrow alt'><td colspan=5></td></tr>";
				}
			echo "<tr class='tbrow'>\n";
			echo "<td style='padding-right:20px'>". $usuario['login'] ."</td>\n";
			echo "<td style='padding:0 20px'>". $usuario['inicio'] ."</td>\n";
			echo "<td style='padding:0 20px'>". $usuario['fim'] ."</td>\n";
			echo "<td style='padding:0 20px' align='center'>". $usuario['pendente'] ."</td>\n";
			echo "<td style='padding:0 20px' align='center'>". $usuario['esq_logado'] ."</td>\n";
			echo "</tr>\n\n";
		}
		echo "</table>\n";
		echo "</td>\n";
		echo $prox_mes;
		echo "</tr>\n";
		echo "</table>\n";
		echo "</div>";
	}
	?>
	<br>
	
	<div style="clear:both"></div>
	<center>
			<a href="admin.php"><< Administrador</a><br>
			<a href="index.php"><< Logador</a>
	</center>
</body>
</html>