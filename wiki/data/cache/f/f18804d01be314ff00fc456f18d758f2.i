a:53:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1;}i:3;a:3:{i:0;s:12:"section_edit";i:1;a:4:{i:0;i:-1;i:1;i:0;i:2;i:1;i:3;s:0:"";}i:2;i:1;}i:4;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:32:"Instalação dos clientes Ubuntu";i:1;i:1;i:2;i:1;}i:2;i:1;}i:5;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:6;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:48;}i:7;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:48;}i:8;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:48;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:329:" A instalacao dos clientes Ubuntu é extremamente simples, bastando informar a ele o idioma (Português do Brasil), o teclado (Português Brasil ABNT2), o nome da máquina (ex: Katyuchia), e editar o esquema de partições, que é assistido pelo computador. Apenas é preciso liberar um espaço livre contínuo e depois escolher ";}i:2;i:52;}i:10;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:381;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:43:"Particionar automaticamente o espaço livre";}i:2;i:382;}i:12;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:425;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:69:". Quando finalizado, ele descompactará os arquivos no disco rígido.";}i:2;i:426;}i:14;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:495;}i:15;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:495;}i:16;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:495;}i:17;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:496;}i:18;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:496;}i:19;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:496;}i:20;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:96:" Uma vez a instalação concluída, ainda há coisas a fazer para o cliente integrar-se à rede:";}i:2;i:500;}i:21;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:596;}i:22;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:596;}i:23;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:596;}i:24;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:596;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:53:"
1- Deve-se habilitar a senha de root, pelo comando:
";}i:2;i:597;}i:26;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:650;}i:27;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:18:"$ sudo passwd root";}i:2;i:650;}i:28;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:650;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:180:"
2- Deve-se adicionar ao arquivo /etc/hosts os hosts das máquinas da rede. ALTERNATIVO: se já tiveres feito isto em alguma das máquinas, basta entrar com o comando (como root):
";}i:2;i:672;}i:30;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:852;}i:31;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:40:"$ scp root@maquina:/etc/hosts /etc/hosts";}i:2;i:852;}i:32;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:852;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:54:"
3- Deve-se adicionar ao /etc/fstab a seguinte linha:
";}i:2;i:896;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:950;}i:35;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:55:"pet://home	/home	nfs	nolock,hard,intr,rw,users,exec 0 0";}i:2;i:950;}i:36;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:950;}i:37;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:275:"
4- Deve-se adicionar todos os usuÃ¡rios com o mesmo UID e GID do servidor. Isto deve ser alterado no arquivo /etc/passwd. Após este processo reinicia-se o computador. ALTERNATIVO: se tu já tiveres feito isto em uma máquina, basta dar os seguintes comandos (como root):
";}i:2;i:1009;}i:38;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1284;}i:39;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:85:"$ scp root@maquina:/etc/passwd /etc/passwd
$ scp root@maquina:/etc/shadow /etc/shadow";}i:2;i:1284;}i:40;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1284;}i:41;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:100:"
5- Deve-se logar com a conta criada durante a instalação e efetuar as atualizações disponíveis";}i:2;i:1375;}i:42;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1475;}i:43;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1475;}i:44;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:158:"6- Deve-se editar (como root) o arquivo /etc/apt/sources.list e descomentar (tirar o #) da linha do Universe, e acionar multiverse. As linhas ficarão assim:
";}i:2;i:1477;}i:45;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1635;}i:46;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:153:"deb http://br.archive.ubuntu.com/ubuntu &lt;versao&gt; universe multiverse
deb-src http://br.archive.ubuntu.com/ubuntu &lt;versao&gt; universe multiverse";}i:2;i:1635;}i:47;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1635;}i:48;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:222:"
7- Deve-se instalar os softwares utilizados por todos (apt-get install pacote, onde pacote é o software desejado, ou usando o synaptic que se encontra em sistema-&gt;administração-&gt;Gerenciador de Pacotes Synaptic):
";}i:2;i:1794;}i:49;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2016;}i:50;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:266:"blender
build-essential
flashplayer-mozilla
glade-2
geda
gstreamer0.8-ffmpeg
gstreamer0.8-mad
gstreamer0.8-plugins
gstreamer0.8-plugins-multiverse
indent
j2re1.4
j2re1.4-mozilla-plugin
mit-scheme
mpich
mpi-doc
ssh
tetex-base
tetex-bin
tetex-doc
tetex-extra
vim-gnome";}i:2;i:2016;}i:51;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2326;}i:52;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:2326;}}