====== Ata da reunião de 14/08/2007 ======

**Presentes: **Antônio, Arthur, Bruno, Fabiano, Felipe, Gabriel, Ismael, Marcela, Marcelo, Matheus, Thiago e Mizusaki

**Ausentes: ** Dante

**Relator: **Marcelo

==== 1) Explicações Thiago : ====

Thiago explica que faltou a reunião e ao PET Cinema, ocorrida há duas semanas, por problemas familiares. Fabiano o repreende por não ter avisado e ter deixado a rede em situação crítica. Também explica sobre como era correlacionado o sistema de atribuição de tarefas burocráticas com o histórico de faltas. O supremo gigantesco líder Ismael xinga Thiago publicamente.

==== 2) Decisão de quem será responsável pela presença nos Interpet's: ====

Os novos representantes do grupo nos Interpet´s serão POrtal e Bruno (Arthur será o reserva). Eles verificarão quando o ocorrerá o próximo Interpet e sobre a possibilidade de acontecer um novo INterpetão tendo em vista a criação de um novo grupo PET na UFRGS.

==== 3) PET Café: ====

Flores, em mais uma de suas relevantes participações na reunião, avisa que sobrará açúcar da caipira para o PET CAfé.

==== 4) Lembrança para completar o TRAC - Horários, Desempenho Acadêmico e atas antigas ====

Mizu questiona sobre o espaço livre no servidor. É comentado sobre a possibilidade de novo HD SCSI para o servirdor. Fica decidido que Antônio procurará maiores informações sobre o servidor e Hds SCSI. Thiago fala sobre a possibilidade de liberar o acesso à PET-DOCS a partir do TRAC. Por sua vez, Portal lembra o pessoal de atualizar o TRAC (horários, desempenho acadêmico, etc...).

==== 5) Desktop Isabela: ====

Arthur comenta que a área de trabalho da Isabela está muito ocupada e desorganizada. Antônio criará uma pasta para cada usuário na Isabela e Mizu comenta que o pessoal poderá acessar como administrador esse PC por apenas mais duas semanas.

==== 6) Palestra do Rualdo -&gt; Certificados: ====

Mizu já encaminhou o pedido de certificados dessa palestra. Custará R$4 a cada um, sendo que Mizu fará essa cobrança.

==== 7) Atividades da semana acadêmica : ====

Ribas pediu a Fabiano que fosse organizada pelo PET um seminário com pessoas de empresas da área e produtos. E que nesse evento, fosse comentado o que não é aprendido na faculdade que é importante para o mercado de trabalho e também qual a reação do mercado ao formando. Mizu e Fabiano organizarão essa atividade.

==== 8) Horário da reunião: ====

Ismael verificará com Dante a possibilidade das reuniões do grupo serem nas segundas das 15h15 às 16h45. Mas é provável que a próxima reunião será nesse dia.

==== 9) HD Visão ====

Antônio garante que o HD está são e salvo.

==== 10) NETFLIX: ====

Marcelo comenta sobre o novo projeto que ele e Thiago, em parceria com Buriol e Vivi, tentarão realizar até o dia 27 para ser enviado a [[/trac/wiki/WebMedia|WebMedia?]]. Trata=se de analisar os dados de recomendação de um site de uma locadora dos EUA e tentar melhorar o algoritmo deles.

==== 11) Licitação: ====

Flores comenta que pode ter havido confusão nas compras da Faurgs e que só foi comprado um PC, ao invés dos 2 desejados. Flores verificará melhor a situação.

==== 12) Logador: ====

Isma fala sobre criar um novo logador em PHP para o PET. Ele, Flores e Antônio ficarão encarregados dessa tarefa e darão um estimativa do término do trabalho na próxima reunião.
