====== Ata da reunião de 20/08/2007 ======

**Presentes: **Antônio, Arthur, Bruno, Fabiano, Felipe, Gabriel, Ismael, Marcela, Marcelo, Matheus e Thiago

**Ausentes: ** Dante e Mizusaki (justificado: reunião do I Ciência)

**Relator: **Matheus

==== 1. Logador (Ismael, Flores e Antônio) ====

Talvez fique pronto essa semana. Thiago pediu pra deixar bem documentado para as futuras gerações de petianos, para manter a continuidade.

==== 2. E-mail Silvia (Marcela) ====

Marcela mandou para ela para ver quando poderia ir para Rio Grande. A resposta foi a qualquer hora.

==== 3. Definição de quem irá a Rio Grande (Dante) ====

Foi decidido que os que iriam para Rio Grande são a Marcela e o Arthur. O grupo deve levantar questões para levar para lá.

==== 4. Organização dos CDs (Mizu) ====

O Mizu pretende organizar os CDs que estam muito bagunçados.

==== 5. MIC (decisões, o que fazer, datas?) (Ismael) ====

O robopet travou todos outros projetos. Foi levantado pelo Ismael a volta da construção do museu. Também foi discutido sobre organizar melhor o tempo entre os projetos.

==== 6. Limpeza da sala / mural (Ismael) ====

A sala estava razoavelmente organizada, mas o mural está cheio de folhas inuteis. O Ismael vai fazer uma limpeza.

==== 7. Orçamento (Flores) ====

Para compra de um novo computador o Flores teve que renovar os orçamentos que tinha antes. O projetor já foi encaminhado.

==== 8. Texto do Matheus e Marcelo relatando sobre experiências no PET (Ismael) ====

Os textos devem ser entregues de preferência esta semana.

==== 9. Lembrança de Interpet amanhã (Ismael) ====

O Portal e o Bruno vão ao InterPET, mas não foram no ENAPET. Derrepente a Marcela e o Ismael vão ao InterPET também.

==== 10. Planejamento PROGRAD (sim...já...) (Ismael) ====

Mês passado foi férias na época da entrega e o Planejamento foi entregue atrasado. Dessa vez entregar até dia 25 para isso não ocorrer denovo. Material de consumo ainda não chegou.

==== 11. Comentários reunião ROBOEDU/ROBOPET (Ismael) ====

Devido ao churrasco a reunião ficou muito bagunçada. Gente preparando churrasco, tocando violão e pouca gente na reunião em si. Da próxima vez que ocorrer reunião seguida de churrasco, deixar bem separado.

==== 12. Comentários do churras (Felipe+Mizu) ====

Foi bom a reunião de petianos e ex-petianos. Estava bem organizado. Flores esperava elogios do Dante, mas teve que se contentar com as palmas do grupo.

==== 13. Comentários da palestra da esc. tec.(Mizu) ====

Na palestra de introdução, Mizu e Arthur discutiram o que era e o que não era robótica. A turma pareceu boa. Gostaram da proposta do Thiago, que deu um resumo do cronograma das aulas. Também falou que é necessário arrumar o laborátorio e que é bom não confiar no curso de java dado lá.

==== 14. Rotatividade da Reunião ====

Não será feita. A reunião continuará as segundas 15:15.

==== 15. Finanças do churras ====

Gastos -&gt; R$ 314,00

  * R$ 214,00 -&gt; comes e bebes
  * R$ 100,00 -&gt; reserva do salão

Receita -&gt; RS 260,00

  * R$ 260,00 -&gt; ingressos

Total -&gt; R$ -54,00
