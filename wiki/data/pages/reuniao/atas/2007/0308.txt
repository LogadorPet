====== Ata da reunião de 08/03/2006 ======

**Presentes: **Antonio, Bruno, Dante, Fabiano, Felipe, Gabriela, Gabriel Portal, Ismael, Mizusaki, Marcelo, Marcos, Paulo, Thiago, Victor.

**Ausentes:** Ricardo (recém-desligado)

**Relator: **Felipe.

==== 1) Recepção ao Paulo ====

Paulo se apresentou aos petianos. Ele acaba de retornar da Alemanha - onde ficou um ano - e gostaria agora de trabalhar como colaborador. Ele conta um pouco de sua experiência lá fora, e se compromete a vir um dia dar um seminário sobre o assunto.

==== 2) Eleição do líder ====

Felipe se despede como líder do grupo, recebendo elogios pelo seu trabalho. Ele, Fabiano e Dante fazem a propaganda sobre a importância do papel do líder e suas atribuições. Thiago e Mizusaki se candidatam e fazem seus discursos de candidatura. A eleição secreta acontece com o seguinte resultado: \\ -&gt; Mizusaki: 10 votos \\ -&gt; Thiago: 3 votos \\ -&gt; Brancos e nulos: 0 votos

Assim sendo, Mizusaki é eleito o novo líder para 2007/1, recebendo a faixa de líder de Felipe e fazendo seu discurso de posse. Além disso, Marcelo e Gabi se lançam como pré-candidatos para 2007/2.

==== 3) Palestra/encontro de apresentacao dos programas de Mestrado Internacional (em Ingles) da Universidade Tecnica de Dresden (Alemanha), pelo Diogo de Brum, entre dias 13 a 15 ou 20 a 22. (Dante) ====

Dante diz que Diogo de Brum acaba de voltar de um Mestrado Internacional em Dresden, Alemanha e que estaria disponível para falar sobre suas experiências. Fica marcada a data de 14 de março (quarta) às 17h e Felipe é o responsável por organizá-la.

==== 4) Projeto LIFAPOR ====

Dante comenta sobre o projeto, que está sem bolsista PIBIC atualmente. Antonio diz que tem um amigo para indicar para a posição.

==== 5) Resultado do ofício sobre aumento de bolsas ====

Dante diz que tem uma informação extra-oficial de que as bolsas devem aumentar para R$350,00 a partir de março. A desculpa dessa vez para o não-recebimento das bolsas no início do ano é devido ao envio tardio dos planos de trabalho entre a UFRGS e o MEC. Dante diz também que no novo manual aparece que só poderia haver um curso de graduaçaõ vinculado a cada PET, o que prejudicaria nosso grupo, formado por 2 cursos.

==== 6) Discussão dos relatórios semanais ====

Por falta de tempo, foi adiado para a reunião de semana que vem

==== 7) Nova regulamentacao proposta para o Ensino de Graduacao na UFRGS ====

Dante diz que acaba de ser elaborada a Nova regulamentacao proposta para o Ensino de Graduacao na UFRGS, que os petianos tiveram a oportunidade de tomar conhecimetno e fazer sugestões. Felipe comenta sobre algumas, como recuperação e semana acadêmica. Todos que quiserem devem enviar sugestões sobre o assunto para Dante.

==== 8) Cumprimento de horas da Gabi ====

Felipe diz que semana passada a Gabi não cumpriu as suas 6h horas semanais (apenas 5h 4 min). Ele se justifica dizendo que estava muito donente na semana, tendo inclusive ido ao hospital. O grupo aceita sua justificativa. Ela se comprometne, então, a recuperar as horas nesse mês.

==== 9) Semana Acadêmica ====

Fabiano comenta que, a partir do 2º semestre desse ano, o Instituto de Informática finalmente terá a Semana Acadêmica, junto com a pós, coordenado pela Direção. Nessa semana, não haveria aulas regulares. Por conseguinte, o grupo pode pensar em atividades para a tal Semana.

==== 10) Projeto RoboEDU 2007 ====

Foi comentado sobre o papel que o pessoal da PUC e o Daniel da Psico teriam no projeto, além de outros detalhes sobre o projeto e seu planjamento, como quando começaríamos a ir às escolas. foi marcada uma reunião para decidir sobre os futuros do projeto na terça (13/03), às 17:10. Thiago pede que todos vão lá já com as idéias prontas na cabeça e que todos se comprometam a ficar no projeto o ano todo. Sobre as compras, Gabi e Victor estão em processo de finalização do documento, falta apenas especificar o notebook. Assim que acabarem, vão entregar na FAURGS. Quanto ao Sistema de Extensão, Mizusaki e Dante vão acabar na semana que vem, aogra que Thiago trouxe os nomes dos participantes. Thiago mandará e-mail a eles perugntando quem que pagou pelos certificados. Dante também mostra o mindstorms que comprou na Alemanha. Todos ficam fascinados.

==== 11) Comentários sobre a recepção aos Calouros ====

Gabi, Victor, Zambiasi, Portal, Antonio comentam sobre como foi a recepção dos calouros, onde apresentaram o Museu e falaram sobre como é o PET.

==== 12) Novas aulas com o Itamir. ====

Será, provavelmente, na última semana de março.

==== 13) Projeto do pet ed.fisica no IAPI ====

Portal fala um pouco mais sobre o projeto e o e-mail que mandou a ESEF sobre o assunto. Será comentado sobre o projeto no InterPET.

==== 14) Posição do grupo em relação a estagios - Interpet especifico sobre o assunto em breve ====

Adiado para a próxima reunião, pois ainda não deve se tratado nesse interpet próximo.

==== 15) Café ====

Bruno diz que já comprou as coisa do café e colocará o preço que caba um deve pagar em breve.

==== 16) Interpet sobre Sulpet ====

Portal diz que ocorrerá semana que vem um Interpet sobre o Sulpet, que ocorrerá de 28 de abril a 1º de maio. Foi visto que 7 pessoas do grupo (incluindo Dante) estão muito interessadas em ir. Os outros ainda não sabem.

==== 17) Futibas PET ====

Marcelo diz que marcou para quinta que vem (15/03) às 20:30. Como é quadra de futeobl 7 (14 jogadores), serão procurados outros interessados em jogar.

==== 18) Desligamento Ricardo ====

Ricardo manda dizer que está se desligando do PET, porque não estava conseguindo conciliar muito bem os estudos com o trabalho, tanto que quase rodou semestre passado. Assim, ficará esse semestre sem trabalhar. Na verdade, o verdadeiro motivo é que seu irmão gêmio Capra saíra na reunião anterior e ele se sentiria muito sozinho aqui.

==== 19) Etiquestas, envelopes e tachinhas do pet ====

Portal conta que achous nos armários antigo etiquets e envelopes; colocou no armário grande. Felipe diz que comprou as tachinhas para o mural que tinha ficado responsável, já colocou a nota fiscal no mural, e já recebeu o ressarscimento do tesoureiro Fabiano.

==== 20) Horário das reuniões ====

Com o desligamento do Ricardo, ficou decidido que as reuniões serão sempre nas quintas-feiras, às 17:20.

==== 21) Planejamneto Anual ====

Felipe diz que nem todos enviaram a sua parte do planejamento, como haviam ficado responsáveis. Eles se comprometem a enviá-las na sexta ou segunda. Portal e Zambiasi ajudarão na confecção do Planejamento. Até a próxima reunião, ele deve estar pronto.

==== 22) Convites CENAPET ====

Dante diz que haverá uma reunião da CENAPET dia 21 e que precisa que alguém o ajude na sexta à tarde na confecção de convites para a mesma. Marcos e Antônio se comprometem a ajudá-lo.
