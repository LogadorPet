====== Ata da reunião de 07/08/2007 ======

**Presentes: **Antônio, Arthur, Bruno, Fabiano, Felipe, Gabriel, Ismael, Marcela, Marcelo, Matheus e Mizusaki

**Ausentes: ** Thiago e Dante

**Relatora: **Marcela

==== 1) Horário da próxima reunião: ====

As reuniões serão realizadas nas terças-feiras, 18 horas.

==== 2) Mudança de Líder: ====

Arthur é pré-candidato para o semestre que vem. Os candidatos a líder foram: Antônio, que queria ser líder porque acha interessante a experiência de liderança, mas estava um pouco preocupado com o final do semestre. Zambiasi, que queria ser líder para aprender a se organizar, mas tem medo de diminuir os conceitos, o que é importante para ele, já que ele quer ir para a França. Ismael, que queria ser líder para aprender a se organizar, mas se preocupa com o final do semestre. \\  -&gt; Antônio: 1 voto \\  -&gt; Zambiasi: 3 votos \\  -&gt; Ismael: 7 votos \\  Ismael é o novo líder.

==== 3) Escola Técnicas (Aulas e Datas): ====

Dante dará uma palestra no dia 17/08. O curso será dado, a princípio, nos dias 14/09, 28/09, 05/10 e 19/10 e a aula do Mizusaki será dada no dia 26/10.

==== 4) II SACO (ciclo de palestras): ====

Ribas sugeriu discussões sobre as redundâncias de conteúdos das disciplinas do curso de Engenharia da Computação. Fabiano disse que iria ver o que a COMGRAD queria e sugeriu que fosse mostrada a visão de mercado por ex-alunos e que fosse explicado sobre o que é o trabalho de conclusão.

==== 5) Comissão de Seleção: ====

A comissão de seleção queria informar a todos que o Matheus e a Marcela serão bolsistas, no lugar, de Victor e Gabriela. Matheus e Marcela deverão escrever sobre o porquê de quererem continuar no PET, o que foi relevante e o que não foi neste tempo em que foram voluntários e entregar para a comissão dentro de duas semanas. Regras para efetivação de voluntários devem ser criadas e colocadas no trac.

==== 6) Churrasco: ====

O churrasco ficou marcado para o dia 14/08 depois das reuniões (ponto 11). Mizusaki ficou de agendar a FAURGS. Flores e Fabiano comprarão o que for necessário para o churrasco pela manhã e caso falte alguma coisa, Portal irá comprar de tarde. Convidaremos os franceses, alguns ex-petianos, Itamir, Alessandro, Basso, Fabiano e Gabriel.

==== 7) Sofá: ====

Marcela viu capas para o sofá e sugeriu que fosse feito um orçamento para refazer o estofamento do sofá.

==== 8) Licitações: ====

O notebook ainda não chegou, mas deverá chegar até o dia 16/08. Os desktops já deverão chegar, mas o Flores irá ligar para a FAURGS para confirmar. Os três orçamentos dos projetores foram entregues na FAURGS.

==== 9) Projeto Cultural: ====

Sobre o filme "Memórias de uma Gueixa", a maioria gostou do filme. Foi sugerido que, nas próximas seções de filmes, fossem escolhidos temas, para que haja mais discussões. Mizusaki sugeriu que ele lesse o livro o "O Mundo é Plano" e fizesse um seminário sobre o mesmo. O grupo gostou da idéia. Flores também demonstrou interesse e foi sugerido que outros lessem e ajudassem no seminário.

==== 10) Curso de Java: ====

O curso de Java será dado a partir do dia 27/10 e Thiago, Portal e Zambiasi vão se reunir para decidir detalhes do curso. Mizusaki se ofereceu para ajudar no material e Arthur se ofereceu para ajudar nas aulas.

==== 11) Reunião ROBOPET/ROBOEDU: ====

As reuniões do ROBOPET e do ROBOEDU começarão por volta das 19:30, após a reunião do PET, que deverá começar 18:00 no salão da FAURGS.

==== 12) Explicações Thiago: ====

O grupo queria explicações do Thiago por ele ter faltado na reunião passada, mas ele também não veio nesta reunião.

==== 13) Situação do Servidor/trac/logador: ====

O trac voltou, então mês que vem os relatórios mensais e semanais deverão ser feitos. Todos deverão colocar seus horários no trac, assim como seus conceitos.

==== 14) PPC: ====

Arthur se interessou e vai ser convidado para a próxima reunião. Por enquanto, Mizusaki vai ajudar as escolas com experimentos simples e materiais.

==== 15) Certificado Paula: ====

O certificado de participação da Paula no PET foi feito e entregue.

==== 16) Monitores: ====

Escolhemos os monitores que seriam subtituídos pelos que chegaram.

==== 17) Curso de PIC/Microcontroladores ====

Arthur e Matheus se interessaram em dar o curso, mas precisam achar um diferencial. Mizusaki e Thiago podem ajudar porque tem alguma experiência no assunto.
