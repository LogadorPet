====== Ata da reunião de 21/12/2005 ======

**Presentes**: Dante, Diego, Estevão, Fabiano, Felipe, Gustavo, Marcos, Nondillo, Robert, Paula, Paulo, Thiago e Vicente. OBS: Thiago chegou 25 minutos atrasado, Paulo saiu antes (1h).

**Relator**: Diego

==== 1. Novos petianos ====

  * Foi feita a apresentação do PET aos quatro novos petianos (Estevão, Felipe, Marcos e Vicente). Foram abordados itens como controle de horários, férias, TRAC, relatórios semanais, Estatuto do PET, enfim, a estrutura do PET.

==== 2. Padrinhos ====

  * Foram definidos "padrinhos" para os novos petianos, que irão auxilia-los nos primeiros meses do PET.
    * Estevão: Nondillo
    * Felipe: Gustavo
    * Marcos: Diego
    * Vicente: Paulo

==== 3. Museu de Informática ====

  * Ficaram responsáveis por catalogar e se envolver no projeto os 4 novos petianos (Estevão, Felipe, Marcos e Vicente). Os padrinhos ficaram responsáveis por auxilia-los no que for necessário.

==== 4. Site do PET ====

  * Gustavo deu idéia de disponibilizar temporariamente o site do PET através do endereço www.inf.ufrgs.br/pet.
  * Dante ficou responsável por entrar em contato com a administração de rede, tentando obter este endereço provisório.

==== 5. Controle das horas - número de semanas ====

  * Foi decidido que a contabilização das horas continuará como está, com as horas não cumpridas em Dezembro sendo compensadas em Janeiro (após as 7 semanas, cada petiano deverá ter 7 x 12 = 84 horas).

==== 6. Folha de efetividade ====

  * Paulo ficou responsável por entrega-la à PROGRAD no começo de Janeiro.

==== 7. Planejamento de Janeiro ====

  * Foi sugerida a idéia de fazer um planejamento semana a semana, para termos melhor controle sobre o cumprimento de cada tarefa, e poder avaliar se o cronograma de atividades está sendo cumprido no prazo.
  * O planejamento deverá ser entregue até a primeira reunião de Janeiro, que realizar-se-á no dia 4 do mês em questão.

==== 8. Piso da sala ====

  * Robert ficou responsável por enviar um e-mail à Silvania (com cópias para Navaux e Otacílio), informando sobre o problema do piso e solicitando que o conserto seja feito em Fevereiro, no período de férias.

==== 9. Planejamento PROGRAD (Dezembro) ====

  * Os petianos que ainda não o fizeram, devem faze-lo o mais rápido possível, enviando por e-mail para o Robert.
  * Vicente fica responsável de aqui por diante envia-lo a PROGRAD.
  * Padrinhos irão ajudar os seus apadrinhados a escrever seu primeiro planejamento.

==== 10. Discussões culturais sugeridas para Janeiro ====

  * Dante sugeriu 3 temas para discussão:
    * Censo do Ensino Superior (feito pelo MEC)
    * Reformulação que será realizada no Manual
    * Edital de expansão do PET
  * Paula ficou responsável de procurar informações no site do MEC sobre o primeiro tema e disponibilizar os documentos para os outros petianos
