ATA 26 / 04 / 05

Presentes: Rubin, João, Nondillo, Robert, Paulo, Maurício, Mateus, Fabiano, Gustavo, Prof. Dante Obs: Irigon e Diego na reunião da Globaltech, Lilian na reunião do CLA

1. Reuniões (Nondillo):

1.1 Mostra Inova UFRGS:

- Infra-estrutura do evento;

- Localização do estande, material necessário, revisão de textos;

- Apresentações do RoboPET em horários marcados;

- Presença do Itamar é necessária para a finalização do projeto;

- Cronograma afixado no mural.

1.2 CETA:

- Necessidades da indústria;

- Integração universidade / empresa;

- Vinculado ao Senai.

2. Informar-se sobre possível parceria com Alemanha: Paulo.

3. Relatórios semanais de atividades: perda de pontos por não fazer o relatório; ter o relatório pronto até o início da reunião.

4. Cursos:

4.1 Curso de Java e orientação a objetos: João e Gustavo. Previsão: início do 2o. semestre. Público: alunos da graduação;

4.2 Curso de Linux: Paulo. Previsão: início do 2o. semestre. Aberto a todos;

4.3 Idéia do curso de C++ adiada;

4.4 Idéia de fazer cursos destinados aos alunos de outras faculdades da UFRGS.

5. Projeto do Sistema Operacional: Paulo, João, Mateus.

6. Possível aplicação para os algoritmos genéticos: João.

7. SulPET (Diego, Paulo, Robert, Rubin, Nondillo):

- Motivador - troca de experiências;

- Certificação Iso 9001 - custo alto;

- Preocupação com a imagem do PET na graduação;

- Sugestão de criação de novas linhas de pesquisa;

- Sugestão da empresa júnior;

- Desconhecimento do PET Fórum;

- Sugestão da elaboração de um cartão.

8. ENECOMP: Sugestão de propor um encontro de petianos de computação.

9. Conclusão do folder para o Portas Abertas: Mateus, Fabiano.
