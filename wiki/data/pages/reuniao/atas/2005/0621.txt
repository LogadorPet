====== Ata Reunião 21/06/05 ======

**Presentes:** Diego, Maurício, Gustavo, Robert, Rubin, Irigon, Nondillo, Lilian, Paulo, Fabiano, Dante, João. **Ausências Justificadas:** Matheus (problemas estomacais) **Relator:** Diego

  - **Dinheiro do [[/trac/wiki/SulPet|SulPet?]]** - Diego entregou, ainda falta Paulo entregar, até semana que vem.
  - **Divisão dinheiro da festa** - Após votação, foi decidido que o grupo é favorável a divisão igualitária entre os grupos. Maurício ficou responsável por ir no Interpet amanhã (22/6) e defender o ponto de vista do grupo, além de sugerir que seja prestada contas dos valores gastos.
  - **Ida Enapet** - Paulo e Robert ainda não fizeram o ofício para solicitar verbas para o II.
  - **I Desafio de Robôs** - Grupo não irá participar.
  - **Pontos de pauta** - Organizar até segunda as 12h.
  - **Contabilização das horas** - De modo geral, todos os integrantes cumpriram as horas na quadrisemana. Lilian, Maurício e Irigon tinham justificativas plausíveis para o não cumprimento total das horas.
  - **Relatório Mensal** - Foi decidida a realização de um relatório mensal do que foi feito no referenciado mês.
  - **Robopet** - Paulo não é mais responsável pela IA. Maurício se propôs a pesquisar sobre o assunto.
  - **Próxima reunião** - todos deverão fazer o planejamento trimestral até a próxima reunião. Também ficou decidido que todos deveriam trazer uma sugestão de livro para a atividade cultural.
