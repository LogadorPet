====== Ata da reunião de 03/03/2009 ======

  * Presentes: Matheus, Fábio, Lucas Fialho, Leonardo, Bruno Guedes, Matheus Proença, Portal, Bruno Jurkovski, Dante.
  * Ausentes: Marcos, Felipe (saiu mais cedo).
  * Relator: Matheus

===== 1. Saída antecipada (felipe) =====

Ele tinha aula no centro e teve que sair mais cedo.

===== 2. RoboPET =====

  * Sala 114

Não podemos usar ela mais. Tentaremos arranjar outra sala. Proença e Portal falaram com o Heuser para ver se conseguimos uma sala no Ceitec, e ele levará isso para a próxima reunião do Instituto.

  * Também veremos a possibilidade de conseguir algum lugar na ESEF para usar nos coletivos.
  * O Proença e o Portal procurarão um lugar que possa fazer o campo em compensado.
  * Lente: Leonardo e Fábio Cavinato vão conversar com o Manuel sobre lentes, já que é possivel que ele tenha conhecimento sobre o assunto.
  * Patrocinio:
    * Mara: O Jurk irá falar com ela.
    * Pró-Reitor de Pesquisa e/ou Graduação
    * Diretor do Instituto: Proença

* Objetivos

  * 9 de março
    * robos de 3 rodas prontos
    * visão pronta
  * 10 de abril
    * robos de 4 rodas prontos
  * 25 de maio
    * tentaremos fazer um amistoso contra FURG durante a semana acadêmica
  * 29 de junho
    * competição
  * Felipe programa mal

===== 3. Saída de PETianos (bruno) =====

Cassiana saiu e não temos mais PETianas. Leonardo irá para Micro$oft e ele mesmo fará seu documento de saída. Procuraremos interessados bons que passem do meio do ano para dar continuidade no RoboPET, senão deixaremos a seleção para o meio do ano.

===== 4. Documentos para serem assinados =====

O Dante assinou-los-os-os.

===== 5. Extensão =====

Bruno A. precisa colocar informações sobre [8818] PROGRAMA DE EXTENSÃO EM ROBÓTICA EDUCATIVA II, Dante precisa colocar informações sobre SERIE DE SEMINÁRIOS DESAFIOS DE SOFTWARE E HARDWARE NAS INDÚSTRIAS

===== 6. escolha de lider (portal) =====

Próxima semana.

===== 7. Horário da Reunião =====

Próxima Reunião: 12 de março, quinta, 18:00 Talvez revezemos as reuniões entre quinta as 18:00 e sexta as 17:00

===== 8. Semac =====

Adiada.

===== 9. Reunião CLA =====

Portal e Fialho irão na reunião.

===== 10. Tesouraria =====

Portal viu com o Dante como ressarcir o caixa, já que foi muito dinheiro gasto nessas férias. Bruno vai falar com a Claudia sobre como pode ser ressarcido.

===== 11. Férias =====

  * Portal, Proença, Jurk, Fábio e Fialho ganharão uma semana de férias para ser usada já que trabalharam uma semana das férias coletivas.
  * O Proença ganhará um mês

===== 12. Mes PET =====

  * Todos deverão ter 36h no logador no fim do mes, exceto o Bruno e o Felipe que deverão ter 48h.

===== 13. Cubo =====

O cubo será adiado para nos concentrarmos no RoboPET.

===== 14. Comgrad CIC =====

Marcos e Bruno J são os representantes da CIC

===== 15. PETPizzaria =====

Sábado. Onde? Pallatos? Cia das Pizzas?
