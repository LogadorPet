Dia: 17/12/04 Presentes: Diego, Paulo, Nondillo, Vinícius, João, Lilian, Gustavo, Robert, Wagner, Dante, Hartmann. Relator: Gustavo

  * 1. Dias de Férias:

>> Aprovado as férias de 17/01/2005 até 04/03/2005

  * 2. Reuniões nas férias:

>> Marcada as próximas reniões para as terças-feiras à 13:30.

  * 3. Material de Consumo:

  * 4. Sujeira da Sala:

>> Os petianos devem colocar os copos usados no lixo.

  * 5. Projetos para 2005:
    * Desenvolvimento de um software com parceria do Ministério do Esporte.
    * Software de integração de powerpoint e captura de movimento.
    * Projeto de realidade virtual juntamente com IA _ Simulaçao

6- Certificados da SNCT:

> Diego encarregado em verificar quem tem interesse nos certificados.

7- ENADE:

> Criação de uma comissão para fazer uma palestra com os escolhidos para fazerem o provão.

8- Seminário Técnico

> Comentado a possibilidade de cada petiano realizar um seminário técnico.

9- Reunião do Robopet

> Marcada para o dia 11/01/05.

10- Roteiro da avaliação do Pet Computação

> Apresentado pelo nondillo o dia da visita da comissão de avaliação dos pet.

11- Dupla diplomação.

> Hartmann relatou como foi a reunião que tratou sobre o assunto da dupla diplomação.

12- Discussão da pesquisa do IBGE sobre Fome

> Foi discutida a pesquisa do IBGE e a questão do programa do governo "Fome Zero".
