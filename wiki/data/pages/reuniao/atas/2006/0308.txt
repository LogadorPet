====== Ata da Reunião de 08/03/2006 ======

Presentes: Felipe, Thiago, Marcos, Vicente, Estevão, Nondillo e Fabiano

Ausentes: Dante(Justificado)

Relator: Vicente

Ps: Esta é a minha primeira Ata, qualquer erro que houver ou assunto que estiver faltando, é só me avisar.

===== Novo PC do Pet =====

Fabiano enviou e-mail para o Luís Otávio. Este, na segunda-feira, encaminhou a compra e na quinta-feira o almoxarifado irá catalogá-lo. Ver se o Dante enviou o e-mail para fazer a nota fiscal.

===== Seleção dos Bolsistas =====

Marcos, Thiago, Estevão e Felipe imprimiram 10 anúncios e distribuíram ao redor do campus. Ainda foi enviado anúncios por e-mail, pelo orkut e também será avisado nas salas de aulas das turmas do segundo, terceiro e quarto semestre. Seria importante contratar bolsistas da ECP e de programadores web.

===== Justificativa do Vicente =====

Defido à chegada de sua mãe de viagem, ele teve que ficar em casa para ajudar a organizar a casa. O grupo verificará se foi justificado ou não.

===== Relatórios Semanais =====

Foi lembrado que deve ser feito o relatório semanal, do contrário será registrado no histórico. Ficou sugerido que fosse colocado ao lado das atividades desenvolvidas o total de tempo trabalhado em tal atividade.

===== Responsável pelo hardware =====

Estevão ficou responsável.

===== Portaria =====

Foi debatido um item que diz respeito à pesquisas. Nondillo avisou que seria interessante alguns petianos desenvolverem pesquisas de seus interesses. Foi sugerido também buscar auxílio dos professores em caso de dificuldade. Aqueles que se interessarem devem apresentá-la em outubro.

===== Ex-Petiano de Maringá =====

Um ex-petiano da computação está na UFRGS fazendo mestrado. Fabiano conversou com ele a respeito de ofertas de bolsas e incentivo à pesquisas.

===== Linux =====

Thiago sugeriu que os petianos migrassem para o Linux. Foi discutido os pós e os contras dessa opção.

===== Planejamento Anual =====

  * Dar andamento no [[/trac/wiki/RoboPet|RoboPet?]]
  * Sugeriu-se ministrar cursos de HTML e PHP
  * Apresentar o Museu na Globaltech

===== Planejamento Trimestral =====

Entregar (no trac) até terça-feira a noite da semana que vem.

===== [[/trac/wiki/SulPet|SulPet?]] e [[/trac/wiki/EnaPet|EnaPet?]] =====

Há pouco interesse por parte dos Pets da UFRGS. Ver como os petianos estão se organizando para ir. Ver um ônibus. Seria interessante se 7 petianos do Pet-Computação comparecessem aos eventos.
