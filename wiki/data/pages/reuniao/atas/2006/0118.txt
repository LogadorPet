====== Ata da Reunião de 18/01/2006 ======

Presentes: Dante, Diego, Estevão, Fabiano, Gustavo, Marcos, Nondillo, Paulo, Robert e Vicente

Ausentes: Felipe (Justificado), Paula (Justificado), Thiago

Relator: Nondillo

===== 1. Barulho na sala =====

Foi feita uma reclamação formal sobre o barulho excessivo na sala do PET no dia 17/01/2006, devido inclusive a presença de não petianos na sala.

===== 2. Planejamento mensal para fevereiro =====

Férias para todos os petianos. Para quem não trabalhou em janeiro e vai trabalhar em fevereiro fica valendo o planejamento do mes de janeiro.

===== 3. Planejamento trimestral =====

O grupo novamente considerou importante a existência de um planejamento trimestral consistente. Os planejamentos mensais devem ser retirados diretamente do planejamento trimestral, que deve ser elaborado com mais cuidado e seriedade. O primeiro de 2006 será para os meses de março, maio e junho e deverá ser elaborado e concluído no começo de março, juntamente com o planejamento anual.

===== 4. Computador lendário =====

Diego entrará em contato ainda nesta semana com Camila da PROPESQ para verificar o procedimento a ser tomado para finalmente efetivar a compra do novo PC do PET.

===== 5. Saída do Gustavo =====

Gustavo recebeu uma proposta da PROCEMPA e, por motivos financeiros, estará deixando o grupo PET no mês de março de 2006.

===== 6. LIFAPOR =====

Gustavo deverá continuar como colaborador neste projeto. Vicente participará de reunião, dia 19/01/2006 as 10:30h na elétrica com a presença de Dante e Gustavo pelo PET, para inteirar-se dos trabalhos e definir sua possivel futura participação no projeto.

===== 7. Museu da Informática =====

Foi criado um modelo de ficha de cadastramento do acervo e também um método para esse cadastramento. Gustavo fará uma banco de dados para automatizar o cadastro das peças. O cadastro deverá conter mais detelhes. Deverá ser criada alguma atração interativa para expor no museu.

===== 8.Relatório de Atividades =====

Todos devem colocar no TRAC seus conceitos no ano de 2005. Será feita uma média aritmética ponderada pelo número de créditos de cada cadeira para obter-se um valor final que represente o desempelho do aluno. O valor de cada conceito fica assim definido: A=10; B=8; C=6; D=4; FF=2;

===== 9.Relatórios de Extensão =====

Os relatórios de extensão foram feitos mas não foram incluídos no sistema pois é necessário o número do processo. Gustavo e Paulo ficaram responsáveis por obter este número e incluir os relatórios no sistema.

===== 10. Discussão da Portaria =====

Diego, como integrante da comissão que discutirá a portaria, registrou as decisões do grupo para bem representá-lo.
