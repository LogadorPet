====== Ata da reunião de 10/10/2006 ======

**Presentes: **Alexandre, Dante, Fabiano, Felipe, Gabriela, Lucas Mizusaki, Marcelo, Marcos, Pablo, Ricardo, Thiago.

**Ausentes: ** Nenhum.

**Relatora: **Gabriela.

==== 1) Dinheiro do projeto RoboEDU - cetec ====

O grupo ficou de fazer uma lista de consumo, no valor aproximado de R$30.000 até segunda-feira no máximo e o Alessandro ficará responsável por repassá-la ao órgão responsável.

==== 2) Comissão de Organização dos 18 anos do PET ====

Marcos e Flores ficarão responsáveis pela organização, contando com o apoio do Marcelo e do Thiago.

  * 06/11 - 10 anos do CEI
  * 10/11 - Palestra Palazzo
  * 17/11 - Museu ( ? )
  * proposta de mini-salão de IC.

Todos devem continuar buscando algum tipo de contato com os ex-petianos, para convidá-los para a comemoração.

==== 3)InterPET ====

Felipe e Marcelo comentam a pauta que será discutida hoje no InterPET, às 19h30.

==== 4)Comisão de seleção ====

Foi comentado que enviaram 17 currículos para a seleção, sendo que 2 desses serão eliminados por conterem mais de 1 reprovação. A pricípio a entrevista do candidatos ocorrerá dia 20, sexta, às 15h30. Felipe ficou de convidar algum representante da direção do II para participar da seleção. Gabriela e Capra ficaram responsáveis por mandar um email para os candidatos avisando o dia e a hora da entrevista.

==== 5)Devolução da Bolsa ====

  * Estevão - 2 bolsas (setembro/outubro)
  * Cavalheiro - 1 bolsa (outubro).

==== 6) Curso de HTML ====

Pablo comenta sobre o curso de html que ele ministrou no salão de extensão. Ele mostrou para o grupo um gráfico mostrando o interesse dos alunos em outros cursos para serem ministrados.

==== 7) Planejamento das Atividades ====

Felipe comenta que o planejamento das atividades deverá ser feito, Pablo ficará resposável. Deverá ser incluso: Centro e vivência, escola técnica, cursos, roboEDU e os 18 anos do PET.

=== 8) Site do LRI = Marcos mostra que existe um link para a página do LRI, que está fora do ar. Ele dá a idéia de resgatar o site e ficará responsável por isso.

==== 9) Curso de C ====

==== 9.1) Instituto Flores comenta sobre o curso ministrado aqui no II, comentando a experiência positiva de lecionar. Marcos mostra a avaliação do curso feito pelos alunos, mostrando as notas altas que receberão. O curso foi muito bem visto pelos alunos que já estão esperando o próximo. ==== 9.2) Escola Técnica Flores comenta que talvez tenha havido alguma confusão sobre a temática do curso, pois na escola técnica eles acreditam que será um curso de "introdução à robótica" e a nossa inteção era fazer um curso de C. Flores mandará um email esclarecendo isso.

==== 10) Cumprimento de Horas ====

Marcos ficou devendo 2h12min que deverão ser cumpridas neste mês. Todos os demais petianos cumpriram sua carga horária.

==== 11)Relatórios semanais ====

A reunião para a discussão dos relatórios semanais deverá ser realizada semana que vem, dia 17/10, terça, após a reunião do grupo.

==== 12)Material de Consumo ====

Gabriela comenta que foi buscar o material de consumo lá na prograd e tudo que foi pedido eles compraram e o valor excedeu em R$891 que será descontado da verba do ano que vem. Comenta também que está faltando 1 carregador de pilhas. Gabriela e Fabiano ficarão encarregados de comprar os gabintes.

==== 13) Salão Jovem ====

Mizu ligará para saber da nossa participação no salão.

==== 14) Ações de Inclusão Digital ====

Thiago visitou o CV, onde foi montado um laboratório com alguns computadores para integrar a "vila-universitária" com a UFRGS . Opfojeto visa fazer uma inclusão social das crianças da vila. Dante faz uma proposta de fazer o lançamento do museu e chamar as crianças para assistir.

==== 15)Audiência de Robótica no senado ====

Dante comenta que terá uma audiência pública no senado sobre robótica.

==== 16) Atividades Culturais ====

Dante propõe que todos assistam "torres Gêmeas" para depois fazermos uma discussão sobre o assunto.

==== 17) Férias ====

Thiago disse que fará uma viagem de 2 meses durante as férias e que pretende cumprir 18h por mês para cobrir as horas do mês que estará ausente. Fabiano comenta que deverá tirar férias no início de janeiro. As férias serão provavelmente em fevereiro. Teremos 1 semana de férias no final de novembro/dezembro ficará a cargo de cada um avisar ao grupo a semana escolhida para tirar essas férias.
