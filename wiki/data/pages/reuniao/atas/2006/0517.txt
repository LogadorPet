===== Ata da reunião de 17/05/2006 =====

Presentes: Alexandre, Eduardo, Estevão, Fabiano, Felipe, Gabriela, Lucas Cavalheiro, Lucas Mizusaki, Marcelo, Marcos, Nondillo, Pablo, Ricardo e Thiago

Ausentes: Professor Dante

Relator: Lucas Mizusaki

===== 1)Limpeza da sala =====

Estevão varreu a sala antes da reunião. Ficou resolvido com as faxineiras que seria feita a limpeza toda quarta-feira de manhã (ou ao menos uma vez por semana); devemos reclamar diretamente para as faxineiras ou levar para a secretaria em caso de problemas.

===== 2)Portas Abertas =====

Marcos ressaltou a participação e o sucesso do pessoal envolvido com o museu, mesmo com a correria de última hora. Ele comentou que o projeto deu certo graças ao estabelecimento de uma meta para o programa do Portas Abertas. Fabiano também comentou que as estandes de dúvidas não receberam muitas visitas, por causa das palestras (que já esclareceram muitas das dúvidas dos visitantes sobre os cursos) e que deveríamos continuar com a iniciativa do Museu da informática ao menos para algumas visitas ou demonstrações, principalmente para os alunos de Arq II. Segundo Marcelo, a única crítica recebida ao Museu foi a falta de uma escala nas fotos dos cartazes.

===== 3)Site do Pet =====

Pablo foi ao CPD para negociar um domínio para uma página de todos os PETs da UFRGS para que cada um dos grupos possua uma pasta própria. Ficou decidido que usar o nosso próprio zservidor não era o mais adequado e a iniciativa foi apoiada.

===== 4)Material para a cafeteira =====

Como a cafeteira está com poucos copos e sem açúcar à algum tempo, foi decidido pela compra de novos materiais (mais copos, açúcar e um pano).

===== 5)Horas pendentes do ciclo de debates =====

Marcelo queria verificar se as horas pendentes do ciclo são válidas para o mês. Fica decidido que caso faltem horas para o pessoal envolvido, será realizada uma avaliação para o caso. Thiago abordou o problema da falta de alunos para prestar depoimento na palestra de mobilidade acadêmica. Agora, ele vai mandar um e-mail para a lista de graduação pedindo por voluntários.

===== 6)Data e Hora para o Interpetão e proposta para o Tema =====

O encontro foi passado para o dia 10 às 9:00 no PET Odonto, e é esperado que cada grupo leve um tema sobre dinâmica de grupo para ser debatido. Foram propostos dois temas para o encontro: o trac (pelo Thiago) e a rotatividade (pelo Marcos). O tema escolhido foi o trac, sob o título de "ferramentas que os grupos usam para se organizar", que será apresentado pelo Thiago.

===== 7)Curso de Linux no CEUE =====

Segundo o Fabioano, Sérgio, o administrador das redes do CEUE, que dá aulas de inglês no curso pré-vestibular deles, está interessado em dar um curso para melhorar a base de informática para os alunos. O curso seria sobre Linux e poderia ser dado por nós (já possuímos um material). Fabiano também comentou que eles estariam montando um laboratório no CEUE para o curso, e Thiago propôs que fizéssemos o curso misto com a informática (pela procura qe há no II). Será feita uma reunião com o Sérgio para planejar o curso que será administrado por Marcelo e Thiago provavelmente no semestre que vem.

===== 9)Seminário =====

Fabiano finalmente apresentou seu seminário sobre a Gasolina.
