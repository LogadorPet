====== Ata da Reunião de 22/03/2006 ======

Presentes: Dante, Estevão, Fabiano, Marcos, Nondillo, Thiago, Vicente

Ausentes : Felipe (justificado)

Relator: Estevão

===== 1. Seleção dos novos Bolsistas do PET =====

Ao todo, irão ser entrevistadas 17 pessoas. Foram excluídos 3 candidatos (uma caloura e duas pessoas com muitas reprovações). Fabiano sugeriu dividir os candidatos em dois grupos.

===== 2.Planejamento Semestral e Anual =====

Nondillo expõe as atividades de ensino, pesquisa e extensão a serem realizadas e pede confirmação dos petianos.

> EXTENSÃO :

  * Catalogação de peças do museu de informática.
  * Participação no Portas Abertas.

>> PESQUISA :

  * Modelo 3D do II.
  * Pesquisa histórica do Museu de Informática.
  * RoboEDU e RoboPET.
  * Visão computacional.
  * Simuladores de Arquitetura.
  * Seminários internos;

>> ENSINO :

  * Sofware para Arquitetura 0 e 1.
  * Workshop com alunos que voltaram da França;
  * Mini-Curso de GTK.

Novos bolsistas serão encorajados a entrar no museu de informática.

===== 3. Robos da Bahia =====

Thiago comenta sobre o seu contato da Bahia, que realizou projetos educacionais através de Robos. libertas.pbh.gov.br/~danilo/

===== 4. Devolvimento de Bolsas =====

Robert, Paula, Diego e Paulo serão contactados (pelo Nondillo) à fim de devolverem suas bolsas referentes ao mês de março.

===== 5. Seminários =====

Fabiano propõe um semináio sobre a discrepância entre os preços da gasolina e do álcool entre os países da América Latina. Dante propõe um seminário sobre a história da informática (museu). Nondillo porpõe fazer seminário sobre seu trabalho de diplomação.

===== 6. SulPET =====

Fabiano comenta sobre o problema dos atrasos das Bolsas. Psico e Esef pensam em não ir ao SulPET devido ao atraso das bolsas. O grupo decide pesquisar até semana que vem, alternativas para ir ao SULPET, tais como ônibus fretado, carro ou comprar passagem individual.

===== 7.Clareza nos Relatórios =====

Fabiano pede maior clareza nos relatórios feitos pelos petianos em relação aos estudos de C, JAVA, CG, OPENGL e Blender.

===== 8.Relatório do Curso de JAVA =====

Estevão e Tiago ficam de falar com o João (dinheiro e dados) e com Gustavo (dados) a fim de obter os dados necessários e o dinherio para encaminhar o mais rápido possível os certificados.

===== 9.PC Velho =====

Estevão fica a cargo de falar com André e Luiz Otavio sobre o que fazer com a carcaça do PC roubado na [[/trac/wiki/GlobalTech|GlobalTech?]].]

===== 10. Jornada Portas Abertas 13/05 =====

Foi sugerido :

  * Plantão de dúvidas.
  * Tentar apresentar o RoboPET
  * Cartazes em referências sobre o ROBOEDU
  * Folder (novo folder, com novas fotos e infs. atualizadas).
  * Confecção de camisetas.

Uma comissão foi criada a fim de tratar do Portas Abertas :

  * MUSEU = Estevão
  * RoboPET = Fabiano
  * Quiosques = Tiago

É importante salientar que todos os Petianos deverão comparecer ao Portas Abertas.

===== 11. Museu =====

Dante pede que a se "retome" o museu de informática, continuando a catalogação. Pede-se organização, planejamento e conteúdo para o museu de informática. Na próxima reunião será decidio o responsável.

===== 12. Moça de Manaus =====

Tutora de Manaus encaminha e-mail a Dante que re-encaminha a Fabiano sobre uma aluna de 2 grau que pede informações sobre a UFRGS (Casa de Estudante ,Bolsas oferecidas ,SAE ...) Estevão e Tiago Ficam responsável por juntar este material para enviar a estudante de 2 grau.
