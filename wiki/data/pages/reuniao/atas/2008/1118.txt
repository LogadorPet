===== Ata da Reunião de 18 de Novembro de 2008 =====

  * Presentes: Antônio, Bruno Albrecht, Bruno Guedes, Bruno Jurkovski, Cassiana, Fábio, Gabriel Portal, Kauê, Leonardo, Lucas F., Marcos, Matheus, Matheus Proença e Tyron;
  * Atrasados: Bruno Guedes e Proença;
  * Ausentes: Dante, Felipe (auto-escola);
  * Relator: Bruno J.

===== 1. Dante ausente há 5 semanas =====

  * Dante não veio porque está doente.

(neste ponto da reunião chegaram Guedes e Proença)

===== 2. Interpet =====

  * Fábio, Fialho, Kauê e Cavinato (no final) se fizeram presentes.
  * Ata foi mandada para o PET-UFRGS.
  * Interpetão será no dia 6 de dezembro. Todos os membros do CLA devem comparecer.
    * Cassiana vai mandar e-mails para tentar mudar para o dia 13.

===== 3. PET-DOCS =====

  * Cassiana organizou as pastas.
  * Todos devem olhar seus documentos e organizar o que for importante até a próxima reunião.
  * Leonardo organizará a pasta da IA.

===== 4. Desktops =====

  * Charlize, Fernanda, Gabriela, Juliana e Katyuchia estão com problemas. Proença vai arrumar.

===== 5. Mini Férias =====

  * Bruno vai tirar essa semana, Antônio, Leonardo, Cassiana e Tyron na semana que vem. Portal também vai usar suas horas bônus.
  * Os novos ganham um vale 6h para ser usado até o natal (não precisando comparecer na reunião na semana de férias).
  * Deverão ser completadas 84h de 30/11 até 31/1 (lembrando do recesso na semana entre o natal e o ano novo).

===== 6. Planejamento Mensal =====

  * Fazer até dia 20 (quinta-feira).

===== 7. Troca de Sala =====

  * Antônio é o responsável pela comunicação.

===== 8. Permissão para usar o osciloscópio no final de semana (mizu) =====

  * Mizu tem a permissão do grupo.

===== 9. PETCafé =====

  * Até a hora da reunião, Fialho ainda não havia pagado a parcela de outubro.
  * Todos paguem o de novembro.

===== 10. Horas faltando para Cassiana =====

  * Ponto de falta justificado -&gt; estava doente.

===== 11. Distribuição de ponto de falta =====

  * Fialho adquire mais um ponto e dispara na liderança de indisciplinariedade.

===== 12. Ponto adiado para a reunião do dia 2/12 =====

  * Trabalhos inadequados para o PET (grupo PET)
