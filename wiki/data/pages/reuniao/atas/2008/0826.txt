====== Ata da Reunião de 26 de Agosto de 2008 ======

  * Presentes: Antonio, Bruno, Bruno G., Bruno J., Cassiana, Dante, Fábio, Felipe, Guilherme, Kaue, Leonardo, Lucas, Lucas F., Marcos, Matheus, Matheus P., Tyron, Portal
  * Ausentes: -

  * Relator: Portal

===== 1. Recepção aos novos =====

  * O grupo deu as boas-vindas aos novos petianos. São eles: Bruno Guedes, Bruno Jurkovski, Fábio, Lucas Fialho, Marcos, Matheus Proença e Tyron. Foi feito procedimento padrão de apresentações dos novos e antigos. Em seguida, foram dadas algumas recomendações básicas em relação a horários, deveres, etc. Após isso, ocorreu o apadrinhamento. Os padrinhos são os nomes entre parênteses: Bruno Guedes (Antônio), Bruno Jurkovski (kauê), Fábio (Portal), Lucas Fialho (Leonardo), Marcos (Felipe), Matheus Proença (Bruno) e Tyron (Cassiana). Em relação aos lanches, o primeiro novato que deve trazer lanche para a reunião é o Fábio. Cassiana ficou responsável em fazer os contratos dos novos petianos. Bruno avisou que a Juliana será formatada na quinta-feira, às 15h. Em relação a data da reunião, foi estipulado que será nas quintas-feiras, no mesmo horário (17h20) devido a problemas com horário da reunião da IA.

===== 2. saida da reunião do portal =====

  * Portal avisa que possivelmente sairá antes da reunião em função de tarefas familiares.

===== 3. tesouraria =====

  * Portal assumirá efetivamente a tesouraria do PET. Mizu e Portal se reunirão na quinta-feira, às 15h, para acertar tudo.

===== 4. Apresentação do sistema de extensão para os novos =====

  * Mizu se responsabilizou de fazer uma palestra sobre o sistema de extensão para os novos petianos. A data marcada ficou após a próxima reunião.

===== 5. PET na SBPqO =====

  * Dante realizará uma apresentação na SBPqO (Sociedade Brasileira de Pesquisa Odontológica) e pediu para dois petianos ficarem responsáveis em organizar a apresentação. Os responsáveis serão Leonardo e Fábio.

===== 6. Sistematização do material do PET =====

  * Dante quer que material antigo em relação ao próprio grupo PET e em relação a CENAPET sejam organizados. Os responsáveis por essa tarefa ficaram Portal, Lucas Fialho, Matheus Proença e Bruno Guedes.

===== 7. Projeto com Lego =====

  * Cassiana falou sobre o novo projeto com o lego Mindstorms. A idéia é resolver um cubo mágico utilizando as ferramentas do lego. Por enquanto, os envolvidos no projeto são Felipe, Cassiana e kauê. Os novos demonstraram interesse no projeto. A meta é poder mostrar isto no Portas Abertas do ano que vem.

===== 8. Semana Acadêmica =====

  * Cassiana falou sobre a série de palestras que está organizando, e que já conseguiu pessoas interessadas em realizar palestras. Mizu comentou que devem ter ao menos 10 palestras de empresas falando de suas áreas de atuação para profissionais e desafios na área. Ficou decidido que serão organizadas palestras das empresas e mais palestras sobre outros temas afins. Tudo será melhor esclarecido quando houver uma reunião com a comissão da semana acadêmica. Lucas Fialho falou da possibilidade de fazer um mini-curso de Ruby, visto que aprendeu a linguagem em um curso pela internet. O grupo gostou da idéia, e Bruno Jurkovski se dispôs a aprender e a ajudá-lo no mini-curso. Bruno disse que Shell Script lhe foi muito válido nas cadeiras da graduação e disse que pretende dar um mini-curso sobre o assunto na semana acadêmica. Felipe irá ajudá-lo. Sobre o curso de látex, Matheus Proença disse que sabe um pouco sobre o assunto. kauê e Matheus ficaram de organizar um mini-curso de látex para a semana acadêmica. Portal se livrou da responsabilidade. Tyron comentou sobre fazer um curso de PHP na semana acadêmica, visto que já domina a linguagem. O grupo decidiu que a semana acadêmica já estaria muito cheia. O curso foi adiado para logo após a semana acadêmica, uma ou duas semanas depois. Os responsáveis pelo curso ficaram Tyron, antônio e Felipe. Ficou decidido que o curso não deverá ser tão básico como os anteriores, devendo ter como pré-requisito saber programar em C. Existe a possibilidade da Léa, do LEC, fazer uma palestra sobre robótica educativa.

===== 9. Interpetão =====

  * Leonardo é responsável do nosso grupo pela organização do Interpetão. Está tendo uma polêmica sobre datas. Ficou decidido que para o nosso grupo, o dia 20 é impossível, o dia 13 também não fica muito bom, o ideal seria dia 06, mas ainda poderia ser no dia 27, como última data.

===== 10. Museu =====

  * Cassiana fez os textos do SED e do CP-500 e mostrou ao Dante. Ela queria uma reunião com todo mundo, mas o Dante achou melhor marcar essa reunião quando tivessemos coisas mais concretas. Então, ficou combinado que Portal e Cassiana irão na comunicação quinta pela manhã e trarão para a próxima reunião material mais concreto para podermos marcar uma reunião.

===== 11. Salão de Extensão =====

  * Felipe falou sobre o Salão de Extensão: datas, sua participação no evento apresentando o curso de linux do PET e dados do evento. A data de abetura é 03/09, felipe fará sua apresentação no dia 04/09 e o encerramento é no dia 05/09.

===== 12. Reunião com Rodrigo sobre projeto [[/trac/wiki/RoboEdu|RoboEdu?]] =====

  * Cassiana, Mizu e Bruno ficaram de reunir para fazer o edital. Na quarta-feira, terá uma reunião com o Rodrigo para se acertar os detalhes do edital.

===== 13. Colaboração com Fundação Pensamento Digital =====

  * Dante falou sobre o projeto. A idéia é o PET fazer cursos como EAD (Ensino A Distância). Marcos, Fábio e Bruno Jurkovski ficaram responsáveis de ir na

aula de Computador e Sociedade do Dante, dia 05/09, na sala 107, para ver o que a responsável pelo projeto tem a dizer.

===== 14. Projeto Primeiras Ciências - Elétrica =====

  * Serão utilizados nas aulas kits da lego da elétrica. Contudo, o pessoal da elétrica quer algum reconhecimento por isso. Bruno e Mizu ficaram de resolver isso. A aula já é na sexta-feira.

===== 15. Atualização da prestação de contas =====

  * Bruno pediu os detalhes para o Dante, sobre o que atualizar sobre as contas do projeto [[/trac/wiki/RoboEdu|RoboEdu?]]. Dante pediu ao Bruno um documento formal, pedindo a prorrogação da rubrica do projeto [[/trac/wiki/RoboEdu|RoboEdu?]].

===== 16. PED =====

  * Cassiana falou sobre o que é o PED apra os novos - PET English Day. Fica combinado que o novo dia de PED é na quinta-feira, começando pela próxima, 28 de agosto.

===== 17. Planejamento =====

  * Felipe alertou a todos que o planejamento deve ser feito logo. Ele levará o planejamento para o centro na quarta-feira.

===== 18. Blog do PET =====

  * A CENAPET lançou o 'Blog do PET'. Os novos ficaram responsáveis de dar um parecer sobre este na próxima reunião. Site: www.petbrasil08.blogspot.com

===== 19. Confraternização com os novos =====

  * Surgiu a possibilidade de fazermos uma confraternização com os novos petianos. Antônio e Matheus ficaram responsáveis de ver as possibilidades do 'Porto Alegre em Cena'. Leonardo ficou responsável por uma possível atividade esportiva.
