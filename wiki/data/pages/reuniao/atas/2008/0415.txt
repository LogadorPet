**== Ata da Reunião de 15 de abril de 2008 ==**

Presentes: Antônio, Arthur, Bruno, Portal, Matheus, Kauê, Cassiana, Zambiasi, Rosália, Marcela e Leonardo. Ausentes: Dante (viagem), Felipe (curso de C), Lucas (curso de C).

Relator: Kauê

**1) Discussão das Regras Internas - TRAC:**

  * RI - 105: modificada de 3 horas para 15 minutos.
  * Atual: "A pauta da reunião deve estar no TRAC até 15 minutos antes do início da mesma, para

que todos possam tomar conhecimento dos assuntos a serem debatidos, tornando a reunião mais produtiva."

  * RI - 107: adicionado ", ficando a cargo de quem coloca o assunto na pauta fazê-lo na posição apropriada em relação aos outros assuntos.".
  * Atual: "Os assuntos devem ser debatidos sempre por ordem de prioridade, ficando a cargo de quem coloca o assunto na pauta fazê-lo na posição apropriada em relação aos outros assuntos."

  * RI - 205: modificada de "no arquivo do PET (um pequeno gaveteiro localizado atrás do sofá)" para "".
  * Atual: "Deve-se evitar o acúmulo de materiais nas prateleiras, mesas e sobre os computadores.

Se necessário deve-se manter esses objetos de maneira organizada na estante. Papéis antigos e documentos importantes devem ser guardados para não serem perdidos e não ocuparem espaço útil."

  * RI - 206: adcionado "Manter uma caixa de papéis renegados, a qual será esvaziada todas as segundas-feiras pela manhã.", que ficará a cargo de Arthur.
  * Atual: "Deve-se evitar o acúmulo de lixo e rascunhos. Cabe ao bolsista controlar jogar fora

seus papéis antigos, copos usados... O líder pode pedir uma limpeza da sala e se livrar do material em excesso. Manter uma caixa de papéis renegados, a qual será esvaziada todas as segundas-feiras pela manhã."

  * RI - 207: adicionado "Pede-se que haja bom senso em relação ao lugar em que são colocados os objetos pessoais (mochilas, bolsas, sacolas, etc.). Fica permitido (e encorajado) aos membros do grupo trocar de lugar os objetos que estejam atrapalhando."
  * Atual: "Mochilas, pastas, sacolas... Devem ser colocadas nas prateleiras, e não sobre as

cadeiras, mesas ou sofás, para evitar o desperdício de espaço útil da sala. Pede-se que haja bom senso em relação ao lugar em que são colocados estes objetos. Fica permitido (e encorajado) aos membros do grupo trocar de lugar os objetos que estejam atrapalhando."

  * RI - 501: Apagada ("Um planejamento trimestral deve ser feito por cada petiano e este deve ser aprovado em reunião.").

  * RI - 502: Apagada ("Cada planejamento trimestral deve ser avaliado, em reunião, após término de sua vigência. Deve-se determinar o porquê do eventual não cumprimento das atividades, visando a adequação dos futuros planejamentos.").

  * RI - 504: modificada de "quatro em quatro semanas" para "duas em duas semanas".
  * Atual: "De duas em duas semanas esses relatórios devem ser rapidamente avaliados em

reunião. Deve-se observar os porquês de eventuais baixas produtividades."

  * RI - 505: modificada de "Deve-se especificar o tempo aproximado de duração de cada tarefa descrita nos relatórios semanais." para "Deve-se especificar o(s) dia(s) da semana no(s) qual(is) foi realizada cada tarefa.".
  * Atual: "Deve-se especificar o(s) dia(s) da semana no(s) qual(is) foi realizada cada tarefa."

  * RI - 600 à RI - 606: voltarão a ser obedecidas quando houver servidor.

**2) Volta do TRAC:**

  * Os membros do grupo devem colocar suas informações atualizadas no TRAC e que os relatórios devem voltar a ser escritos no mesmo.

**3) Semana Acadêmica:**

  * Atividades que serão realizadas pelo grupo: Maratona de Programação, "curso de Python", oficina de Robótica, (talvez) palestra de Robótica.
  * Portas Abertas: banca, museu, nômade e seguidor.

**4) Curso de C++:**

  * Será realizado na Semana Acadêmica ou após. A apostila deverá ser terminada até o fim do mês.

**5) Reunião da IA:**

  * Será realizada terça-feira (22/04) ás 15 horas e 30 minutos.

**6) Churrasco do PET:**

  * Fica marcado para o dia 09/05. Marcela fica responsável de falar com Flores.

**7) Reunião do projeto RoboEDU com a professora Extráuzaluas:**

  * Tópico adiado para a próxima reunião devido à ausência do Lucas.

**8) Sobre o servidor:**

  * A placa mãe do servidor teve que ser trocada, portanto os orçamentos terão que ser refeitos. Bruno está encarregado.

**9) SulPet:**

  * Inscrição até dia 18 por R$ 70.00, após será mais caro. O auxília será de R$ 160.00.

**10) Aniversário da Cassiana:**

  * O grupo a parabeniza pelo seu aniversário.

**11) Contribuição Felipe:**

  * Felipe trouxe sua contribuição para a geladeira do PET. O grupo agradece.

**12) Discussão de Relatórios (últimas duas semanas):**

  * Foram discutidos.
