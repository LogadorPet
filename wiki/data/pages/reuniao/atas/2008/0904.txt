====== Ata da reunião de 04/09 ======

  * Presentes: Antonio, Bruno, Bruno G., Bruno J., Cassiana, Dante, Fábio, Felipe, Guilherme, Kaue, Leonardo, Lucas, Lucas F., Marcos, Matheus, Matheus P., Tyron, Portal
  * Ausentes: -

  * Relator: Leonardo

===== 1. Lanches =====

  * O lanche que o fábio trouxe estava bastante bom e ficou defindo que o Cavinato sera o proximo a trazer

===== 2. Material antigo do PET =====

  * Portal conclama o pessoal a arrumar as coisas. Acusaram-se: Proenca e Fialho e Guedes. Trabalharao amanha à 1:30

===== 3. Museu =====

  * O pessoal foi na comunicacao. Eles estao muito enrolados e o lancamento do museu foi adiado para Dezembro.
  * Por nosso lado precisa-se de gente para limpar e material. O caixa do pet pagara os artefatos de limpeza.
  * Como não houve entusiastas, ficou definido que a Cassiana tem o direito de conclamar a comunidade petiana para um mutirao de limpeza. Bruno pode vetar!

===== 4. Aula do Primeira Ciências =====

  * Portal pergunta como foi.
  * Resposta: Só foi uma profa e um cara da psico. Ficaram vendo a aula. Para a proxima aula temos 4 confirmados mais um pessoal da psico. Emfim, as aulas vao comecar mesmo amanha.

===== 5. Lembrança sobre a fundação do Pensamento Digital =====

  * Marcos nao vai poder ir na aula do Dante, a qual contará com a presenca da mulher da fundacao do pensamento digital. Irao Jurk e Fabio

===== 4. Blog do PETBrasil - relato dos novos =====

  * Fabio diz que os primeiros posts falam sobre a historia do PET. Depois os posts se dividem em várias:
    * Reunioes
    * Exigências de melhoras
    * Filosóficos
    * Shakespeare
    * Maluquices
    * Os acessos (a maioria)
    * Charges sem graca
  * A ideia do blog era melhorar a comunicação entre a CENAPET e os petianos, acontece que as novidades acabam e nao se sabe o que postar. Sugeriu-se a criacao de um forum ou de um googlegroup.

===== 5. Chaves dos armarios =====

  * Bruno pergunta quem nao tem a chave do armario. Os sete novos levantaram as maos. Mizu entrega a sua chave para o Bruno em um ato recheado de simbolismos. Na proxima reuniao Bruno trara as chaves.

===== 6. PETCafé =====

  * "Paguem!", foi a mensagem de nosso líder.

===== 7. Limpeza da sala =====

  * Sexta feira a sala estava uma porquice (Bruno). Bruno relembra o funcionamento do sistema de documentos e purgatorios. Os copos devem ser jogados fora ou entao, ... , TRAGAM SUAS CANECAS (é a solucao mais ecologica).

===== 8. Organização do Palm =====

  * Cassiana e Tyron estao usando o Palm para cadastrar o museu. O palm esta cheio de porcarias e cassiana pede para que o palm seja limpo de porcarias residuais. Como ninguem tem nada de interessante la, o palm sera limpo.

===== 9. Sugestão Curso Introdução a Análise de Circuitos =====

  * Guedes e Proenca conversaram e pensam em fazer um curso de introducao a analise de circuitos, que é uma das cadeiras mais dificeis do curso (essa frase gerou polemica, visto que sempre quem já passou por qualquer cadeira acha que ela é fácil).
    * Guedes e Proenca elaboraram uma proposta de curso, que e mais voltado para as ferramentas do que para o conteudo. A proposta foi lida. Foi sugerido que esse curso fosse dado na cadeira de introducao à ECP.
    * Ficou decidido que Proenca e Guedes falarao com o Renato a respeito do curso. Bruno também sugeriu que o curso fosse dado antes de estar vinculado a qualquer cadeira, para que fosse tida uma ideia da procura e depois...

===== 10. Novas Camisas PET =====

  * Felipe sugere que seja feita uma nova camisa. Todos concordadam.
  * Ficou decidido que o modelo sera polo e sera igual a atual exceto pela cor e por uma bandeirinha do RS no braco direito. A cor ficou azul escuro.
  * Felipe olhara no bourbon, matheus no carrefour e cassiana no shopping. Mizu procurará por bordados (falando com a Gabi).

===== 11. Curso/Aula para comunidade(acadêmica/geral) =====

  * Cavinatto sugere que os cursos possam ser dados para a comunidade em geral (ufrgs e alrededores). Portal sugere um curso de Latex para a comunidade em geral e para o pessoal da Engenharia um curso de C, porque eles programam muito mal. Dante diz que a fundacao pensamento digital ja tem o publico alvo para os cursos desejados. Felipe levanta o ponto de que dar cursos agora desviaria a atencao dos projetos atuais do PET.
  * Mizu, que ja teve experiencias nesse sentido ficou de falar com o Cavinatto para dar-lhe algumas dicas.
  * O grupo concordou que dar cursos para a engenharia é mais facil e mais apropriado que para a comunidade academica.
  * Dante diz que o PET deve ser cada vez mais um elo de ligação com a sociedade.
  * Sera mais discutido este ponto na proxima reunião;

===== 12. Certificados ENAPET =====

  * Matheus enviou para a lista que temos de mandar para o Ezequiel copias dos comprovantes do ENAPET. Ficou decidido que o matheus respondera o email com os comprovantes (que podem ser encontrados no site do enapet).

===== 13. Compra de mais materiais =====

  * Dante disse que falou com o ezequiel e que podemos mandar comprar mais coisas materiais. O grupo acha uma boa ideia mandar comprar 3 desktops novos, sendo que destes 1 iria para a sala do PET e 1 iria para o LRI e 1 iria para a sala do Dante. Apenas 1 computador viria com monitor. Essa grana vem de taxas academicas 2008.

===== 14. Tesouraria =====

  * Pois é. Portal é efetivamente o novo tesoureiro do PET. Foram-lhe passados 645 reais. Faltam pagar:
    * Bruno (com 20 reais deduzidos)
    * Antonio (sulpet e enapet)
    * Thiago
    * Zambiasi

===== 15. Sugestão Curso PHP Avançado =====

  * Tyron já tinha comentado na semana passada e pergunta por pessoas interessadas. Ele ja tem um planejamento de 5 dias com 3 ou 4 horas diarias. Antonio e Felipe disponibilizaram-se a ajudar e sugerem um curso de 2 semanas. Seria interessante a elaboracao de uma apostila nova.

===== 16. Ruby on rails na informática - Portal CENAPET =====

  * Antonio falou com o pessoal da USP e eles disseram que todo o sistema esta em RoR. Nao sabemos se a informatica suporta RoR. Fialho ficou encarregado de verificar a existencia de RoR na UFRGS.

===== 17. Curriculum Lattes =====

  * Felipe diz relata aos novos que é muito interessante fazer seu curriculo Lattes. Ficou a dica para todos.

===== 18. Efetividade =====

  * A efetividade sera entregue juntamente com o planejamento todo dia 20.

===== 19. Futebol de Robos ESEF =====

  * Felipe relata que viu na ESEF uma placa enorme escrito: "Futebol de Robos"
  * Dante conta que dois anos atras na ESEF houve um evento cientifico e que isso podem ser resquicios desse evento.
  * Podemos verificar se lá nao é um lugar interessante para treinar nossa propria equipe

===== 20. Lista de E-mails PPC =====

  * Bruno diz que enviara um email para o Luís Otávio para criar uma lista de email
  * Será pedido também que Tyron tenha autorização para acessar o www./pet

===== 21. Insercao dos novos nos projetos =====

  * Portal pergunta se os novos gostaram de onde entraram e se querem entrar onde nao entraram:
    * Cavinatto: Quer entrar na IA, entrou na visao e a visao nao trabalhou -&gt; acabou estudando c++ a semana toda;
    * Fabio: Quer entrar na visao, acha que a IA ja esta saturada;
    * Jurkowski: Gostou da IA;
    * Fialho: Fez um pouco na IA. Preparou ruby. Esta interessado no cubo.
    * Proenca: Mexeu bastante na eletronica essa semana. Gostou da mecanica. Acha interessante ajudar no museu.
    * Guedes: Mecanica, e o projeto de curso para a ECP que ele está planejando dele.
    * Tyron: Programou no Lego. Esta estudando C++ e esta dando olhadelas no codigo da IA. Pensou no Curso de PHP. Sente-se atraido pelo lego, pelo museu e pelo cubo.

===== 22. Sistema de extensão =====

  * Depois da reuniao o mizu quer dar um cursinho sobre o sistema de extensao. Foi prorrogado nesta quinta as 5, antes da reuniao.

===== 23. Ultima reunião do mizu =====

  * Mizu decidiu nao largar nada de mao, e seguir ajudando o pessoal e o Dante.
  * Apos um discurso comovedor, dante ofereceu-lhe uma bolsa bic, mas ele diz preferir uma bolsa que lhe facilite o tcc.
