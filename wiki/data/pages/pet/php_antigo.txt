====== Curso de PHP ======

==== Cronograma ====

  * Aula 1 - Revisão de HTML, Conceitos básicos de PHP, Sintaxe, Sep. de Instruções e Comentários (Cap 1 e 2) - Antônio
  * Aula 2 - Tipos de Dados (Integer, Strings, Arrays, Listas, Objetos, Booleanos), Transformação de tipos e Constantes, Operadores, Estruturas de Controle (até switch)(Cap 3, 4, 5 e 6 - até 6.2.2) - Flores
  * Aula 3 - Comandos de Repetição (While, Do...While, For, Foreach), Quebra de Fluxo (Break e Continue) e Funções (Cap 6.3 até 6.4.2 e Cap 7) - Ismael
  * Aula 4 - Variáveis e Includes (Cap 8 e 9) - Ismael e Antônio
  * Aula 5 - Formulários (Cap 10) - Antônio e Ismael
  * Aula 6 - Manipulação de Arquivos Remotos e Programação Orientada a Objetos (Cap 11 e 12) - Flores
  * Aula 7 - SQL, Tipos de Dados e Instruções de acesso a dados do MySQL (Cap 14) - Ismael e Flores
  * Aula 8 - Cookies (Cap 15) - Antônio e Flores

-&gt; Sugestão de Felipe: Aula 11 - Cookies, Sessões e E-mails --&gt; possible

==== Decisão das datas a serem ministradas as aulas ====

  * Dias 7,8 e 10 de maio
  * Dias 14,15 e 17 de maio
  * Dias 22 e 24 de maio

==== Aulas ministradas por: ====

  * Antônio
  * Flores
  * Ismael

==== Data pretendida das aulas ====

  * de 7 a 24 de Maio

==== Duração ====

  * 3 semanas

----

Curso de PHP - SEMANA ACADÊMICA

Duração: 12 horas Período: 22 a 25 de outubro de 2007

Idéias:

- Retirar Revisão de HTML Consequencia: Pré-Requisito de saber HTML (eliminando alguns bichos, quem sabe). Assim o curso vira essencialmente PHP e já tira praticamente 1h30min da 1ª aula que foi dada pelo Antônio. Sugestão: na divulgação do curso, disponibilizar a apostila de HTML pela net para quem quiser revisar, já que não terá revisão.

- Retirar Programação Orientada a Objetos Todos os conteúdos são cumulativos e usados, mas POO não é uma matéria mega-necessária pra ser dada, visto que dá pra se programar sem saber POO. Sugestão: Deixar POO na apostila, mas não dar o conteúdo, que já economiza +- 1h da aula do Flores.

O resto, terá que ser dado em um tempo menor infelizmente... Tentei organizar as aulas da seguinte forma:

*** AULA 1 *** - Início de PHP (Cap 1 e 2) -&gt; Explicar tb o Wamp - Tipos e Constantes (Cap 3 e 4) - Operadores (Cap 5) - Estruturas de Controle (If e Switch) PS: Equivale as aulas 1 e 2 do nosso curso, mas sem a revisao de HTML

*** AULA 2 *** - Estruturas de Controle (While, Do...While, For, Foreach, Break, Continue) (Cap 6) - Funções (Cap 7) - Includes (Cap 9)

*** AULA 3 *** - Variáveis (Cap 8) - Formulários (Cap 10) - Manipulação de Arquivos Remotos (Cap 11)

*** AULA 4 *** - Bancos de Dados (Cap 14) - Cookies (pq o Antônio adora XD) (Cap 15) PS: Equivale as aulas 8 e 9 do nosso curso, levando em consideração que Cookies nao leva tanto tempo assim e sobro tempo da outra vez...

Apostila a ser usada está terminada. Encontra-se no pet-docs e na sala do PET para visualização.
