===== Lançador de Projéteis =====

Site de empresa que fabrica solenóides: http://www.soletec.com.br

  * Idéias de como fazer o mecanismo de lançamento:
    * Usar a solenóide, mas se não der impulso suficiente fazer um esquema usando momento de força;
    * Usar a solenóide apenas para disparar uma tranca que permite o travamento de uma mola (o travamento da mola seria manual, já que o carregamento da bolinha tamb seria manual não haveria muito problema)
    * Usar uma massa presa a um motor, que após estar totalmente acelerado, faz a massa colidir com a bolinha a ser lançada;
  * Teste, com sucesso, de um lançador com mola. A bola atravessa a sala do LRI. Agora será necessário elaborar o sistema de destravamento da mola.
  * Utilizando as idéias discutidas no lRI na quinta, tentei fazer um protótipo. Melhorá-lo.

===== Circuito Eletrônico =====

  * Componentes Utilizados:
    * Fotoacoplador til 817
    * Transistor de potência
    * Resistores

Em breve mais informações do projeto do circuito.
