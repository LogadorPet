**Planejamento PROGRAD Maio 2008**\\ \\

===== Antônio Augusto Fontoura =====

  * Ensino
    * Oficina de Robótica na semana acadêmica.

  * Pesquisa
    * Desenvolvimento do layout da versão final do circuito do ROBOPET.

  * Extensão
    * Participação do SulPET
    * Realização da Semana Acadêmica do II

===== Arthur Vicente Ribacki =====

  * Ensino
    * Oficina de Robótica na semana acadêmica.
    * Desenvolvimento de apostila e slides de C++.

  * Pesquisa
    * Estudo sobre movimentos omnidirecionais.
    * Construção do interpretador de encoder dos motores.
    * Estudo sobre software Proteus e montagem de simulação do circuito inteiro.

  * Extensão
    * Ajuda na construção de um Poster para Salão de Graduação.
    * Montagem de palestra sobre Robótica para Semana Acadêmica.

===== Bruno Landau Albrecht =====

  * Pesquisa
    * Desenvolvimento do RoboPET
      * Estudo de movimentos omnidirecionais
      * Estudo de comunicação serial do juiz da competição RoboCUP
    * Curso de PIC
      * Estudo sobre microcontroladores
      * Estudo sobre o software de simulação Proteus
    * Oficina de Robótica da Semana Acadêmica
      * Montagem de Robôs pequenos e baratos
  * Extensão
    * Portal do MIC
    * Oficina de Robótica para a Semana Acadêmica

===== Cassiana Chassot Fulber =====

  * Ensino

  * Pesquisa
    * Estudo da linguagem C++
    * Aprimoramento do código da Visão do ROBOPET
    * Participação no desenvolvimento da Mecânica do ROBOPET

  * Extensão
    * Realização da Semana Acadêmica do II

===== Felipe Augusto Chies =====

  * Ensino
    * Oficina de Robótica na semana acadêmica.

  * Pesquisa
    * Programação da [[/trac/wiki/GoGoboard|GoGoboard?]];
    * Estudo e conclusão de tutoriais para [[/trac/wiki/SolidWorks|SolidWorks?]];
    * Término dos esquemas mecânicos do ROBOPET;
    * Esquema do chutador para o robô.

  * Extensão
    * Desenvolvimento e apresentação de trabalho para Salão de Graduação;
    * Oficina de Robótica para a Semana Acadêmica.

===== Gabriel Marques Portal =====

  * Ensino
    * Desenvolvimento de apostila e slides para o Curso de C++

  * Pesquisa
    * estudo da Linguagem C++
    * Aprimoramento do código da Visão do RoboPET
    * Desenvolvimento da Inteligência Artificial do RoboPET

  * Extensão
    * Participação no SulPET
    * Organização da Semana Acadêmica do II / Realização da Semana Acadêmica do II

===== Kauê Silveira =====

  * Ensino:

  * Pesquisa:
    * estudo da linguagem de programação C++;
    * desenvolvimento e documentação da Inteligência Artificial do projeto RoboPET.

  * Extensão:
    * realização da Semana Acadêmica do Instituto de Informática.

===== Leonardo Piletti Chatain =====

  * Ensino:
    * Preparar o curso de C++

  * Pesquisa:
    * Estudo do codigo da IA
    * Desenvolvimento da IA

===== Lucas Eishi Pimentel Mizusaki =====

  * Pesquisa
    * Programação da [[/trac/wiki/GoGoboard|GoGoboard?]]
    * Término dos esquemas mecânicos do ROBOPET
    * Esquema do chutador para o robô
  * Extensão
    * Organização da Semana Acadêmica do II
    * Auxílio com a robótica no colégio para competição de robótica com LEGO

===== Marcela Macedo Vieira =====

  * Ensino
  * Pesquisa
    * Estudo da Linguagem C++ para desenvilvimento da Inteligência Artificial do RoboPET
    * Desenvolvimento da Visão do RoboPET
    * Documentação da Visão do RoboPET
    * Desenvolvimento da Inteligência Artificial do RoboPET
  * Extensão
    * Confecção de pôster para o Salão de Graduação
    * Participação no SulPET

===== Marcelo Zambiasi =====

  * Ensino
    * Correções nos slides do Curso de Linux para futuras edições
  * Pesquisa
    * Estudos sobre LDAP, NFS, SAMBA (entre outros tópicos) para tarefas administrativas no servidor do PET
  * Extensão
    * Participação no SulPET
    * Organização da Semana Acadêmica do II / Realização da Semana Acadêmica do II

===== Matheus Vogel Pinto =====

  * Pesquisa
    * Estudo de movimentos omnidirecionais
    * Programação de PICs para o roboPET e um curso de PIC
    * Desenvolvimento do domínio do roboPET

  * Extensão
    * Desenvolvimento do Site do Museu da Informatica e Computação
    * Realização da Semana Acadêmica do II

  * Ensino
    * Oficina de Robótica na semana acadêmica.

===== Rosalia Galiazzi Schneider =====

  * Ensino
    * Desenvolvimento de apostila e slides de C++.

  * Pesquisa
    * Estudo sobre colisão de objetos
    * Desenvolvimento do Simulador da Inteligência Artificial do RoboPET
    * Desenvolvimento da Inteligência Artificial do RoboPET
