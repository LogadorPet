====== Planejamento Mensal PROGRAD: MÊS DE NOVEMBRO ======

Este planejamento deve ser realizado até o dia 25 de OUTUBRO! Colocá-lo na ordem alfabética.

===== Diego =====

  * Ajuda para o Paulo na implementação de uma ferramenta de automatização da avaliação do trabalho da disciplina de Inteligência Artificial
  * Integração das 2 cameras do [[/trac/wiki/RoboPet|RoboPet?]]
  * Curso GTK

===== Fabiano =====

  * PESQUISA:
    * [[/trac/wiki/RoboPet|RoboPet?]]
      * Montagem e testes no novo chassi (continuação);
      * Estudo sobre microcontroladores PIC afim de entender programa atual dos robôs (continuação);
      * Estudo do chip LM3647 para auxílio (ao Maurício) na fabricação do carregador de pilhas NI/MH;

===== Gustavo =====

  * Pesquisa:
    * Estudar a atual IA do RoboPET e tentar colocar para funcionar o goleiro desenvolvido.
  * Ensino:
    * Terminar os slides do curso de Java.
    * Curso de Java

===== João =====

  * Pesquisa:
    * Continuar estudos de jsp para possível reformulação da página
  * Ensino:
    * Ministrar o curso de Java.

===== Mateus =====

  * Pesquisa:
    * Projeto do visualizador de imagens em Java.
  * Ensino:
    * Curso de Java.

===== Maurício =====

===== Nondillo =====

  * RoboPET
    * Gravação do PIC e testes - se possível utilizando linux;
    * Estudo para adaptação mecânica do encoder;
    * Definição de componentes para novo projeto do robopet;
      * PESQUISA
  * PET
    * Criação de documentos para auxiliar o aumento da produtividade no PET;
      * CUNHO BUROCRÁTICO

===== Paula =====

  * Ensino:
    * Planejamento e criação do curso de HTML

===== Paulo =====

  * Continuar a implementação de um servidor com autenticação multi-plataforma centralizada escalável para um grande número de usuários, suportando hospedagem de páginas dinâmicas com separação de privilégios.
    * PESQUISA
  * Desenvolvimento de uma ferramenta de automatização da avaliação do trabalho da disciplina de inteligência artificial.
    * PESQUISA
    * ENSINO

===== Robert =====

  * RoboPET
    * Dar continuidade nos estudos acerca do rádio para aprimoramento do RoboPET;
    * Aprender alguns conceitos da parte do hardware dos Robôs.
      * PESQUISA

===== Thiago =====

  * Ensino:
    * Curso de Introdução a Ciência da Computação no Anne Frank
  * Pesquisa:
    * Estou aprendendo programação de GUI para implementação dos simuladores em ambiente Linux

===== Vinao =====
