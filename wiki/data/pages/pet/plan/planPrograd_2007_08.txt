===== Antônio =====

===== Arthur =====

===== Bruno =====

  * Pesquisa
    * Estudos sobre o Rádio, Protocolos de Comunicação e comunicação serial a ser utilizado no roboPET
    * Estudo sobre Banco de Dados para futuro Portal do Museu de Informática e Computação
  * Extensão
    * Implementação do site do Museu de Informática e Computação

===== Fabiano =====

  * Pesquisa
    * Programação em pic (desenvolvimento do robopet)
    * Especificação dos motores e redutores do robopet
  * Ensino
    * Preparação de material e apresentação de artigo no cobenge2007
  * Extensão
    * Início da organização do "Ciclo de debates sobre o mercado de trabalho do Engenheiro de computação"
      * Planejamento das atividades e início dos contatos

===== Felipe =====

===== Gabriel Portal =====

  * Ensino
    * Curso de JAVA
  * Pesquisa
    * Inteligência Artificial para Futebol de Robôs
    * JAVA
    * C++

===== Ismael =====

===== Marcela =====

  * Pesquisa
    * Visão do Futebol de Robôs
    * HTML e PHP
  * Extensão
    * Estudo de Egressos
    * Site do Museu da Informática da Computação

===== Marcelo =====

===== Matheus =====

===== Mizusaki =====

===== Thiago =====
