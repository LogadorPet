====== Planejamento Mensal PROGRAD: MÊS DE AGOSTO ======

Este planejamento deve ser realizado até o dia 25 de JULHO!

===== Diego =====

  * PESQUISA:
    * RoboPET (PESQUISA)
      * Continuar a desenvolver o módulo de imagem (especificamente, botar pra rodar)
      * Ajudar o Gustavo e o Maurício a integrarem a IA com a imagem
  * ENSINO
    * Revisão do material para o mini-curso GTK para a graduação

===== Fabiano =====

  * RoboPET:
    * Projeto do novo chassi dos robôs;
    * Estudo e montagem das placas eletrônicas do rádio;

===== Gustavo =====

  * RoboPET:
    * Início do desenvolvimento da IA;
  * ENSINO
    * Desenvolvimento da apostila para o mini-curso JAVA;
  * Pesquisa
    * Desenvolvimento de uma ferramenta para atualizar sites mais facilmente;

===== Irigon =====

  * ENSINO
    * Curso Linux na escola Infante Dom Henrique.

===== Mateus =====

  * ENSINO
    * Curso Linux na escola Infante Dom Henrique.

===== Maurício =====

  * R oboPET:
    * Testes com novos tipos de baterias e posterior definição dos tipos de baterias a serem usadas nos robôs;
    * Ajuda na implementação do controle de velocidade nos robôs;
    * Aprender linguagem C como passo inicial nos trabalhos com a IA;

  * Aprender a administrar a rede com o Paulo;

===== Paulo =====

  * RoboPET
    * Continuar a desenvolver o módulo de imagem (especificamente, botar pra rodar)
    * Ajudar o Gustavo e o Maurício a integrarem a IA com a imagem
  * Atualização das máquinas da rede
  * Implementação de um servidor de horas

===== Robert =====

  * Pesquisa
    * RoboPET
      * Estudos acerca do rádio -&gt; apresentar avanços no conjunto até 15 de agosto para o Grupo PET;
      * Estudos acerca do encoder para os motores dos robôs.

===== Nondillo =====

  * Projeto do novo chassi dos robôs;
  * Início do projeto da eletrônica do novo robo, implementando o controle de velocidade;
  * Reinstalação do Windows em todas as máquinas, buscando melhor desempenho.
