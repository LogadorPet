===== Algoritmo de ajuda ao Ezequiel nas compras do Material de Consumo =====

  * Descubra se a empresa da qual quer comprar vende com empenho para a UFRGS e se possui cadastro na SICAF
    * Pode-se perguntar à empresa
  * ou
    * Pegue o CNPJ da empresa e pergunte ao Ezequiel (melhor solução).
  * Se tiver, peça um orçamento em nome da "Pró-Reitoria de Graduação", com o produto especificado e sua quantidade.
  * Envie o orçamento ao Ezequiel.

----

===== Compras de Materiais com dinheiro de Projetos pela FAURGS =====

  * Até R$1000,00: Compra direta, sem licitação, nem orçamentos
    * O material pode ser comprado com o próprio dinheiro e depois ser reembolsado
  * De R$1000,01 a R$8000,00: necessários 3 orçamentos
  * Acima de R$8000,01: necessária licitação, com todos os produtos muito bem especificados

  * Contratação de Pessoa Jurídica: Se for a única fazendo tal serviço, deve haver uma carta de exclusividade

===== Algoritmo para compras de R$1000 a R$8000 =====

  * Se o material a ser comprado não tiver sido especificado no projeto original, deve-se redigir um ofício à "Diretora em Exercício do Departamento de Apoio a Convênios / PROPLAN" solicitando a liberação da verba para o referido produto, lembrando que não devem ser especificadas marcas dos produtos, apenas dados genéricos a respeito dele.
    * Obs.: Peça ajuda ao Dante
  * Tendo a verba liberada, deve-se pedir, para 3 empresas diferentes, um orçamento, em nome da "FAURGS", CNPJ 74704008/0001-75, com a especificação detalhada do produto a ser comprado (agora podem ir as marcas de tudo).
    * Obs.: Os três orçamentos devem ser para o mesmo produto (as especificações dos produtos devem ser tão semelhantes quanto possível, ou, preferencialmente, iguais)
  * Com os três orçamentos em mãos, faça uma visita à Helen, no 3º andar da FAURGS, e entregue para ela os orçamentos.
  * Acompanhe com o Dante a liberação da Ordem de Compra (se demorar mais de 2 dias úteis, ligue e pergunte se a ordem já foi liberada e para qual empresa)
  * Acompanhe, com a empresa escolhida, a entrega do produto no Patrimônio.
  * Acompanhe, com o Patrimônio, a liberação do produto para entrega (provavelmente virá para o Luis Otávio)
  * Quando o produto for liberado para entrega, veja com o Luis Otávio se chegou! se chegou, peça para ser entregue no PET (ou vá buscar).
  * Com o produto entregue, veja se está dentro das especificações. Se estiver, peça para o Dante avisar a Faurgs que o produto está em ordem (dê o número da Nota Fiscal), para que o dinheiro seja liberado para a empresa.
