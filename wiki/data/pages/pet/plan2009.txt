====== Inserir as atividades almejadas para 2009 ======

==== * Ao lado de cada atividade inserir o nome dos participante ====

===== Ensino =====

  * Curso de C++
  * Curso de Java
  * Cursos de C
  * Curso de PIC
  * Curso de Linux
  * Curso de PHP
  * Curso de Análise de Circuitos
  * Curso de Ruby
  * Curso de Ruby On Rails
  * Oficina de Latex

===== Pesquisa =====

As atividades de Pesquisa planejadas para o ano constituem os deveres do grupo em aprimorar os conhecimentos dos integrantes nas áreas específicas de cada projeto, além de proporcionar à comunidade acadêmica em geral diversas contribuições a respeito.

  * ROBOPET

Projeto de pesquisa e desenvolvimento em robótica voltada à competição de futebol de robôs (RoboCUP) para robôs de tamanho pequeno (Categoria F180). O projeto contará com a participação dos integrantes para o desenvolvimento total dos robôs, incluindo o sistema de processamento de imagens, inteligência artificial, desenvolvimento de um simulador dos jogos para fins de teste da inteligência artificial, elaboração do esquemático eletrônico e placa de circuito impresso de controle do robô e, finalmente, modelagem e desenvolvimento do chassi-base do robô. No projeto, foram utilizadas as linguagens de programação C++, para implementação da inteligência artificial; Java, para implementação do software simulador; e C, para programação do microcontroladores PIC que controlam os robôs.

  * Projeto 3L-CVRP (3D Loading – Capacitated vehicle Routing Problem)

Atividade de pesquisa que envolve técnicas de otimização combinatória com o intuito de resolver o problema 3L-CVRP da melhor forma possível. Este problema foi proposto por um grupo de pesquisa italiano e, desde então, já foi abordado por uma tese de doutorado, contudo ainda é um problema bastante recente. A idéia do problema é juntar dois problemas bastante difíceis: o roteamento de veículos em um grafo completo e o empacotamento de caixas num espaço tridimensional.

  * Sistemas Lineares com GPGPUs

A atividade consiste no desenvolvimento de algoritmos para resolução de sistemas lineares de grande porte, utilizando placas de vídeo para o processamento (GPGPU - General Purpose GPU). A finalidade é a solução de problemas relacionados com física nuclear, integrando-se com pessoas da física e da matemática. Os resultados dessa atividade podem ser bastante inovadores na área, uma vez que o que não existem soluções semelhantes. A atividade também proporciona um conhecimento muito importante para os integrantes, visto que o desenvolvimento de algoritmos paralelos não é diretamente ensinado na faculdade e representa uma grande tendência da computação atualmente.

  * PETCubo

O projeto visa habilitar o Lego Mindstorms a resolver o cubo mágico. Para isso, é necessário que monte-se-o de uma forma que permita a manipulação do cubo, assim como utilização do sensor de luz para identificar a posição inicial do mesmo, e técnicas de Inteligência Artificial para resolvê-lo efetivamente, as quais serão programadas numa linguagem compatível com o Lego. Com isso, melhora-se o domínio sobre planejamento, montagem e programação deste instrumento de robótica. Neste projeto aliam-se os conhecimentos dos cursos de Ciência da Computação e Engenharia da Computação, sendo o mesmo, portanto, interdisciplinar. Ao final da execução, pretende-se fazer apresentações demonstrando o seu produto final, ou seja, o Lego Mindstorms resolvendo um cubo mágico, partindo de uma posição qualquer, e explanando sobre os conhecimentos envolvidos no funcionamento do mesmo, traçando paralelos com as disciplinas presentes no currículo dos cursos de Ciência e Engenharia da Computação e fazendo uma demonstração prática de suas aplicações, para a Graduação, além de aumentar o interesse em Computação por parte dos alunos do Ensino Médio, através da participação no evento chamado "Portas Abertas".

===== Extensão =====

  * Portas Abertas
  * Semana Acadêmica
  * [[/trac/wiki/RoboEdu|RoboEdu?]]
  * Oficina de Latex para outros Cursos
  * Curso de C para outros Cursos
  * Museu De Informática e Computação

===== Atividades de caráter coletivo =====

  * Participação nos Interpet's
  * Participação no Sulpet
  * Participação no Enapet
  * Participação da SBC
  * Participação no FISL

===== Atividades Culturais =====

  * Atividades desportivas
  * Peças de Teatro
  * Sessões de Cinema

===== CRONOGRAMA PROPOSTO =====
