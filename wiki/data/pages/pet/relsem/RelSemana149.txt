==== Antônio ====

===== 27/07 a 02/08 =====

  * Desenvolvimento novo layout do site do PET (segunda)
  * Organização dos resistores do LRI (segunda)
  * Inicio do novo site do PET (idéias) (terça)
  * Tentativa de arrumar algumas coisas no LRI (terça)
  * Reunião Comissão de Seleção (terça)
  * Desenvolvimento do novo site (quarta, quinta, sexta)
  * Discussões com o Fabiano sobre um kit pra Micros
  * Tratativas com a admrede para hospedar o site do PET (quarta)
  * Reunião para as aulas com o LEGO (sexta)

===== 04/08 - 08/08 =====

  * Desenvolvimento do site (segunda - quinta)
  * Consultas a admrede a respeito da hospedagem do site (segunda, terça)
  * Prendi os cartazes da seleção no insituto (quinta)
  * Início do desenvolvimento do esquemático do kit de micros (quinta)

**********************************************************************************

==== Bruno ====

===== 27/07 a 03/08 =====

  * Reunião Comissão de Seleção (terça)
  * estudo do protocolo i2c
  * Leitura dos livros sobre pic
  * Discussões com o Fabiano sobre um kit pra Micros
  * Reunião para as aulas com o LEGO (sexta)

===== 04/08 a 10/08 =====

  * Procura de novos projetos com o Mindstorms
  * Formatação da Fernanda
  * Ajeitei o servidor pra deixar o logador nele
  * cadastro do guilherme e cassiana no trac
  * procura de preços e emails pra helen sobre os kits do mindstorms que precisamos comprar
  * Atualização da parte de adminstração do trac e do servidor

**********************************************************************************

==== Felipe ====

===== 14/07 a 20/07 =====

  * ENAPET

===== 21/07 a 27/07 =====

  * Teste com novos motores
  * Estudando e revisando PHP

===== 27/07 a 02/08 =====

  * Semana de Férias

===== 04/08 a 10/08 =====

  * Teste de Motores;
  * Pesquisa de novos motores;
  * Novo Site do PET.

**********************************************************************************

==== Gabriel ====

===== 27/07 a 02/08 =====

  * reunião da comissão de seleção (terça)
  * Egressos (terça)
  * Reunião do museu distribuído (sexta)
  * texto da visão (sexta)

===== 03/08 a 10/08 =====

  * pesquisa do SED - Sistema de Entrada de Dados (segunda)

**********************************************************************************

==== Kauê ====

===== 27/07 a 02/08 =====

  * IA do RoboPET (qui, sex)
  * Simulação do RoboPET (qui, sex)

===== 03/08 a 10/08 =====

  * Leitura de artigos sobre a IA do CMDragons (seg)
  * IA do RoboPET (ter)
  * Simulação do RoboPET (ter, qui, sex)

**********************************************************************************

==== Leonardo ====

===== 27/07 a 02/08 =====

  * IA e Simulador do RoboPET

===== 04/08 a 10/08 =====

**********************************************************************************

==== Lucas ====

===== 27/07 a 02/08 =====

  * Trabalhei no Chassi do robô de novo (sexta)
  * Conversei com os profesores para a apresentação de áreas de trabalho e de conhecimento do engenheiro da computação (quarta)
  * Reunião para as aulas com o LEGO (sexta)
  * planejamento das aulas para os professores, início da criação dos slides

===== 04/08 a 10/08 =====

  * Continuei os trabalhos no chassi do robô;
  * Apresentação sobre as áreas de atuação da Engenharia da Computação;
  * Fui na reunião do primeiras ciências;
  * Comecei a estudar o robô do Itamir;

**********************************************************************************

==== Matheus ====

===== 27/07 a 02/08 =====

  * Desenvolvimento novo layout do site do PET (segunda)
  * Organização dos resistores do LRI (segunda)
  * estudo do protocolo i2c (quarta)
  * Reunião para as aulas com o LEGO (sexta)

===== 04/08 a 10/08 =====

  * atualizei e pedi mais material de consumo;
  * pesquisa de solenoide
