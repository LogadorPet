====== Relatorio semanal de 09 de Outubro de 2006 a ? de Novembro de 2006 ======

OBS.: Deixar em ordem alfabetica.

===== Alexandre =====

==== 09/10 a 15/10 ====

  * Avaliação dos currículos junto com o Flores (Segunda)
  * Alguma coisa para o [[/trac/wiki/RoboEdu|RoboEdu?]], daqui a pouco eu lembro (Segunda)
  * Ajuda ao mizusa com o programa em Ruby. Funcionou! LoL (Terça)
  * Troca dos teclados antigos pelos novos que chegaram (Terça)
  * Rápida conversa com Johann perguntando sobre o seu comparecimento na entrevista de seleção. Parece estar se esquivando... (Terça)
  * Confecção e envio de e-mail aos selecionados para a entrevista da seleção, e para os diretores pedindo para participar da mesma (Quarta)
  * Mais alguns testes e ajustes no funcionamento do joystick (Quarta)

==== 16/10 a 22/10 ====

  * Segundo encontro no Tubino (Segunda)
  * Pesquisa dos dados de ex-petianos (sem muito sucesso...) (Terça)
  * Leitura dos relatórios do mês passado (Terça)
  * Ajudei a montar o pc novo (Terça)
  * Participação no Salão Jovem (Quinta)
  * Enrevistas dos candidatos a bolsistas (Sexta)

===== 23/10 a 29/10 =====

  * Melhorias no 'Resumo PET Computação' para o Salão de Graduação (Terça)
  * Leitura da Ata da reunião passada (Terça)
  * Testes com o gravador de PIC (Terça)
  * Ajuda na montagem do 2º PC (Terça)
  * Inserção planejamento prograd (Quinta)
  * Testes com os robôs [4 PICs funcionando e com programa, 1 PIC aparentemente não funciona, 3 robôs funcionando, 2 placas com defeito] (Quinta)
  * Mostramos algumas coisas do LRI para o Antônio (Quinta)
  * Reunião RoboEDU (Quinta)

===== 30/10 a 06/11 =====

  * Ajuda na familiarização do Victor com o LRI, o TRAC, etc (Quarta)
  * Leitura da ata da reunião do dia 31/10 (Sexta)
  * Fui com o Ricardo e o Victor (e o Estevão =P) ao NELE novamente [faltam muitos componentes na placa, trouxemos para o LRI] (Sexta)
  * Leitura do capítulo 6 sobre PIC, e posterior 'aula' com Estevão sobre o código do PIC (Sexta)

***********************************************************************************************************************************

===== Antônio =====

==== 24/10 a 26/10 ====

  * Reunião de ingresso no PET (terça, ~2h).
  * Inserção dos dados pessoais no TRAC, bem como os horários e planejamento (quarta, ~1h).
  * Leitura parcial do Manual do PET (quarta, ~30min).
  * Visita ao LRI (quarta, ~1h).
  * Tomado conhecimento da estrutura do site do PET, bem como seus arquivos. (quinta, ~1h)
  * Troca de dominio das fotos do site do PET. (quinta, ~30min)
  * Reunião do RoboEDU, para tomar conhecimento dos detalhes do projeto. (quinta, ~1h30min)

==== 30/10 a 1/11 ====

  * Construção (junto com o Fabiano) do segundo lançador de projéteis do RoboEDU. (segunda, ~3h30min)
  * Análise do "HUB" com defeito no instituto de letras. (terça, ~1h)
  * Reunião PET. (terça, ~2h)
  * Tentativa de análise no "HUB" que está em funcionamento no laboratório da letras, para comparação com o defeituoso. (quarta, ~1h30min)
  * Ajuda na convecção de convites para a comemoração dos 18 anos do PET. (quarta, ~1h)

***********************************************************************************************************************************

===== Bruno =====

==== 30/10 a 06/11 ====

  * Reunião de ingresso no PET (terça).
  * Inserção dos dados no TRAC, bem como os horários (quarta)
  * Preparação do ofício da comemoração dos 18 anos do PET e busca dos e-mails dos convidados (quarta)
  * Leitura dos tópicos do TRAC (Projetos, Organização e Controle, ...) (quarta)
  * Descobrir e-mails das pessoas que receberão o ofício dos 18 anos d PET computação (sexta)

***********************************************************************************************************************************

===== Fabiano =====

==== 09/10 a 15/10 ====

  * Guardei as peças dos novos pcs no armário. (quarta,20 min);
  * Busca de problemas nas placas dos robôs. Achei um robô que estava funcionando. (quarta)
  * Ajustes no robo, na alimentação dos rádios, vários testes e preparativos para o encontro no tubino (mtos bons resultados.. quarta 3h)
  * Formulários setec-roboedu, busca de material,...(quarta, quinta 1h45)

===== 16/10 a 22/10 =====

  * Preparativo dos materiais (a serem levados) e segundo encontro do roboedu no tubino. (seg, 2h + 2h30)
  * Cotação e compra dos gabinetes (terça 1h, )
  * Ajustes no cronograma setec junto com o Dante (terça,30min)
  * Leitura (em aula) do relatório das atividades de setembro-outubro. (terça)
  * Contato com a Monica para acertar detalhes da mesa redonda que eu irei participar quinta às 15h30-17h no salão jovem.
  * Enviei pra Gabi o mail do andre (para buscar robo e devolver as cameras)(terça 5 min)
  * Apresentação do roboedu no I Salão Jovem UFRGS (quarta, quinta 7h30)

===== 23/10 a 29/10 =====

  * Acertos no caixa -&gt; ressarcimento Dante (gastos na viagem para ribeirão) e zambiasi (poster museu para sic puc).(seg, 30 min)
  * Mail para alunos do Tubino com link para as fotos. (seg, 5 min)
  * Busca de egressos do pet (seg bastante tempo)
  * Inserção planejamento prograd (seg, 5 min)
  * Confecção do segundo protótipo do lançador de projéteis (seg, mto tempo)
  * Ajustes na Planílha de horários da ecp. Sondar alunos sobre a necessidade de outros horários (terça, 1h);

===== 30/10 a 06/11 =====

  * Compra de material e montagem (junto com o Antonio) do segundo chutador, além de aperfeiçoamento do primeiro (seg, 5h);
  * Projeto do acionador eletrônico do lançador, não está completo ainda. (quarta,1h30)
  * Leitura e coposição de um texto sobre robótica educativa no "Programa Nacional de Robótica" (quarta 2h30)

***********************************************************************************************************************************

===== Felipe =====

==== 09/10 a 15/10 ====

  * Avaliação dos currículos junto com o Capra (Segunda)
  * Conferimento do cumprimento de horários pelos petianosno mês passado, com confecção de tabela correspondente (segunda)
  * Colocação do poster do Ciclo de Debates na parede com a ajuda de Marcelo, Marcos, Alexandre e Estevão / Pablo e [[/trac/wiki/LepMizusaki|LepMizusaki]] (segunda/terça)
  * Arrumação do Trac na parte dos relatórios semanais dos petianos (terça)
  * Participação no InterPET na ESEF com Marcelo (terça)
  * Envio de e-mail a Johann sobre sua participação na entrevista (terça)
  * Confecção e envio de e-mail aos selecionados para a entrevista da seleção, e para os diretores pedindo para participar da mesma (quarta)
  * Compilação dos relatórios semanis setembro_outubro, com envio do mesmo à lista (quarta)
  * Confecção e envio de e-mail para a Prof. Rute Vera em relação ao curso de c na escola técnica (quarta/sexta)
  * Entrega da Folha de Efetividade Mensal de Setembro e Planejamento Mensal de Outubro na PROGRAD (quarta)
  * Cadastramento no sistema de extensão do curso de C (sexta)
  * Envio de fotos do museu para zambiasi (sexta)
  * Procura por ex-petianos na internet (sexta)

==== 16/10 a 22/10 ====

  * Impressão dos documentos do relatório de extensão do curso de c e do ciclo de debate, e pegada da assinatura do dante e aprovação dele dos mesmos (segunda)
  * Revisão no poster e nos slides da apresentação do Museu de Informática no Salao de Iniciação da PUC (segunda)
  * Entrega na Lourdes dos documento do curso de C do sistema de extensão (quarta)
  * Procura por ex-petianos (quarta)
  * Análise dos currículos e perguntas para entrevista com Gabi e Zamba (quinta)
  * Entrega do planejamento das atividades do grupo do fim do ano prara Ezequiel na PROGRAD (quinta)
  * E-mail a Weber lembrando da seleção (quinta)
  * Verificação com Mizu e Ricardo e aprovação de diversas horas pendentes (sexta)
  * Envio de e-mail a Thiago a respeito de seu não-preenchimento do relatório semanal (sexta)
  * Envio de e-mail a Ezequiel a respeito do planejamento do grupo e do material de consumo (sexta)
  * Preparação da mesa e cadeiras para a seleção, e organização dos curriculos (sexta)
  * Entrevista de seleção de novos bolsistas e envio e confecção de e-mail aos selecionados (sexta)

==== 23/10 a 29/10 ====

  * Conferimento, confecção e envio de e-mails aos interessados em ser voluntários do pet (segunda)
  * Envio do planejamento de fim de ano do pet para Andrea, da prograd (segunda)
  * Assistida à apresentação do Zambiasi sobre o Museu, a ser apresentada no SIC PUCRS (segunda)
  * Verificação dos documentos da seleção (segunda)
  * Elaboração do Resumo(versão 0.98) do PET para o Salão de Graduação, junto com o Zamba... (segunda)
  * Verifiquei os documentos da portaria e Manual a ser entregue para os novos petianos (segunda)
  * Ida ao InterPET na Geografia (terça)
  * Explicação para Ismael de várias coisas do PET (terça/quarta)
  * Arrumação do Trac na parte do historico dos relatores de ata, com inclusão das duas ultimas atas, e dos novos petianos (quarta)
  * Confecção e envio de e-mail aos selecionados para voluntário (quinta)
  * Marcada com a Silvânia do Salão de Festas da FAURGS (quinta)
  * Conferimento dos relatórios da seleção (quinta)

==== 30/10 a 5/11 ====

  * Participação na reunião (1h30) sobre o Museu da COMEX/II (segunda)
  * Participação na reunião na FACED da Comissão do Salão de Graduação junto com os outros PETs da UFRGS (segunda)
  * Leitura rápida do Manual de Orientações Básicas (terça)
  * Estudo de Java (terça)
  * Pegada dos dados dos novos petianos (terça)
  * Conferimetno do e-mail do material de consumo (quarta)
  * Confecção e envio de e-mail para PET-UFRGS sobre os telefones dos pets (quarta)
  * Ajuda a Ismael com dados do Museu (quarta)
  * Ajuda a Portal com os e-mails do reitores e pró-reitores (quarta)
  * Entrega dos vários documentos da seleção na PROGRAD (quarta)

**********************************************************************************************************************

===== Gabriel (Portal) =====

==== 23/10 a 27/10 ====

  * Atualização dos dados pessoais no TRAC, assim como planejamentos e horários (quarta - 1h)
  * Ajudei o Dante em uma apresentação de Power Point (quarta - 30min)
  * Leitura do Manual de Orientações Básicas (quarta - 1h / quinta - 30min)
  * Assinatura do Termo de Compromisso de Aluno e do Relatório de Seleção como Bolsista (quinta - 5min)
  * Estudei a linguagem C (quinta - 2h30min)
  * Estudo de HTML/PHP - Peguei a apostila do curso oferecido pelo PET (quinta - 15min)
  * Ajudei o Dante em uma apresentação Power Point (quinta - 30min)

==== 30/10 a 05/11 ====

  * Estudei a linguagem C (segunda - 1h30min)
  * estudo de linux... (terça - 1h)

***********************************************************************************************************************************

===== Gabriela =====

==== 09/10 a 15/10 ====

  * Guardei o material de consumo e verifiquei se todos os pedidos se encontravam na caixa - faltava 1 carregador (terça)
  * Troca dos teclados antigos pelos novos que chegaram (Terça)
  * leitura dos currículos (terça)
  * Tentativas de ajeitar o "chutador" do robô (terça)
  * Reunião do projeto RoboEDU (terça)

==== 16/10 a 22/10 ====

  * Pesquisa dos antigos petianos (terça)
  * leitura de muitos emails, porque só agora o webmail voltou a funcuionar (terça)
  * leitura dos relatórios mensais (terça)
  * pesquisa de gabinetes para os pcs novos (terça)
  * pesquisa dos preços de monitores e frigobars (terça)
  * Montei, juntamente com Capra e Ricardo, um dos PCs novos (terça)
  * participação no salão jovem (quarta)
  * Passei a ata para o computador, ela já se encontra no Trac (quinta)
  * mandei um email para o André da [[/trac/wiki/TechRobot|TechRobot?]] para saber se nosso robô já se encontra lá (quinta)
  * Entrevista de seleção dos novos bolsistas(sexta)

==== 23/10 a 29/10 ====

  * montagem do segundo PC novo, tive problemas com jumpers (terça)
  * análise na planilha de horários da ecp (terça/quarta)
  * Inserção planejamento prograd (Quinta)
  * Reunião RoboEDU (quinta)
  * confecção do relatório da comissão de seleção (quinta)
  * relatório de troca de bolsista e termo de compromisso (quinta)
  * Pablo me ensinou algumas coisas do site do PET (quinta)

==== 30/10 a 5/11 ====

  * Ida ao centro para comprar acessórios para o protótipo de acoplamento do lançador ao robô (segunda)
  * Montagem do 1º potótipo de acoplamento (terça)
  * Novos projetos de protótipo de acoplamento, não achei que ficou ideal o primeiro (terça)

***********************************************************************************************************************************

===== Ismael =====

==== 23/10 a 29/10 ====

  * Conhecimento do LRI (outra sala do PET). (terça)
  * Edição dos dados pessoais e colocação dos horários no Trac (quarta)
  * Começo da leitura do Manual do PET (quarta)
  * Assinatura do Termo de Compromisso de Aluno e do Relatório de Seleção como Bolsista (quinta)
  * Leitura geral do Manual do PET (quinta)
  * Realização do Planejamento Mensal e Efetividade e impressão, com a ajuda do Pablo (quinta)

==== 30/10 a 05/11 ====

  * Leitura de e-mails e leitura do Planejamento Institucional enviado pelo Marcos, na parte referente a computação (tudo parece em ordem) (segunda)
  * Presença na Reunião envolvendo o Projeto e Desenvolvimento do Museu (segunda)
  * Estudo da linguagem C - uso de material feito pelo PET (segunda/terça)
  * Leitura da Portaria nº 3385 de 29 de setembro de 2005 (terça)
  * Leitura da Ata da Semana Passada (terça)
  * Reunião com o Dante sobre os eventos dos 18 anos do PET e do Museu (quarta)
  * Reservas das salas do prédio novo para os dias 14/11 e 24/11 para os eventos 18 anos PET/Museu (quarta)

***********************************************************************************************************************************

===== Marcelo =====

==== 09/10 a 15/10 ====

  * Dei umas olhadas na linguagem PERL pro projeto [[/trac/wiki/WikiGraph|WikiGraph?]] / Reunião(1h15) com as Prof. Buriol e Vivi / Pesquisei um pouco sobre a Escola Regional de BD(formato,datas,etc...) (segunda)
  * Enviei um e-mail com dúvidas(pôster e apresentação oral) sobre o Salão da PUC - obs: e-mail respondido em 8min(eficiência total) (segunda)
  * Pequena analisada nos currículos para a seleção do PET (segunda)
  * Pesquisas sobre Mysql(baixei o pacote, instalei e depois de muita luta consegui criar minha conta e talz) (terça)
  * Complementação e inserção de algumas atividades no planejamento PET do 2° semestre(quarta)
  * Testes com o código em PERL do projeto wikigraph - que sofrimento!(quarta)
  * Envio de e-mail para o cara da Kaizen(pequena pressão) para ver a data da próxima palestra (quarta)
  * Preparei algum material para iniciar o poster e slides do museu pro salão da PUC (quarta)
  * Fiz o poster o os slides para o salão da PUC (sexta)

==== 16/10 a 22/10 ====

  * Troca de e-mails com a prof. Buriol e com o Marcelo Messa(Kaizen) (segunda)
  * Tentando entender e arrumar o código em PERL(tá complicado!) (segunda)
  * Ajuda ao Dante no Skype, Yahoo Groups, etc... (segunda)
  * Dei umas dicas ao Flores e Pablo sobre o cadastro no Sistema de Extensão (segunda)
  * Dei uns últimos retoques no poster do Museu. (segunda)
  * Pesquisa dos antigos petianos (terça)
  * Mais tentativas de arrumar o código em PERL(com ajuda do Thiago) (terça)
  * Ajustes no código(junto com Thiago) e descobri algumas coisas bem interessantes / Reunião com a Buriol(45min) (quarta)
  * Consegui contactar a gráfica e enviei o pôster (amanhã, provavelmente, deve chegar) (quarta)
  * Código do pass1 tá funcionando(algumas peculiaridades ainda na expressão regular!) (quinta)
  * Análise do currículo e histórico escolar dos candidatos a entrevista do PET (quinta)
  * Entrevista da seleção (sexta)

===== 23/10 a 29/10 =====

  * Elaboração do Resumo(versão 0.98) do PET para o Salão de Graduação, junto com o Flores... (segunda)
  * Documentei os passos 0,1, Config e iniciei o 2 para o projeto [[/trac/wiki/WikiGraph|WikiGraph?]] (segunda)
  * Vericando os papéis da seleção e ajuda ao Flores no e-mail aos voluntários (segunda)
  * Documentei o passo 2 e comecei o passo 3 do código do projeto Wikigraph(terça)
  * Mostrei o Ismael a sala do LRI, algumas coisas na sala do PET e também o trac...(terça)
  * Participei do Salão de IC da PUC (quarta - parte da manhã e toda tarde)
  * Resolvi algumas pendências urgentes com a Kaizen e revisei os slides da apresentação para o SIC da PUC (quarta)
  * Apresentei o trabalho no salão da PUC (quinta -manhã)
  * Organização da palestra da Kaizen em tempo recorde (correria!) / Fiz os cartazes, colei...fiz uma propaganda na turma do Johann (quinta)
  * Reunião com a Buriol e a Vivi (quinta - 45min)

==== 30/10 a 05/11 ====

  * Primeira reunião com as Profs. Buriol e Viviane, junto com o Pablo, sobre o projeto do [[/trac/wiki/WebGraph|WebGraph?]] (segunda)
  * Ajudei o Fabiano a preencher mais algumas coisas sobre o relatório a ser enviado ao MEC (segunda)
  * Fiz umas pesquisas preliminares sobre o formato dos arquivos XML da Wiki (terça)

************************************************************************************************************************************

===== Marcos =====

==== 09/10 a 15/10 ====

  * Conversa e algumas coisas combinadas com o pessoal da ação de inclusão digital do CV (ter/45min)
  * Leitura do folder sobre a pesquisa na Austrália, do material do Study in Australia (ter/30min)
  * Email para a administração da rede sobre o site do LRI (qua/10min)
  * Email para os professores (Weber,Johann,Flávio,Lamb,Dante e Daltro) comunicando a palestra/debate sobre o ensino da computação (qua/20min)
  * Leitura de 10 páginas (seção "Australian Education System") do "Country Education Profiles - Australia 2006" (qua/30min)
  * Leitura do formulário sobre o Brasil no site do DFAT (Department of Foreign Affairs and Trade) da Austrália, para ler sobre a relação Brasil-Austrália (http://www.dfat.gov.au/geo/brazil/brazil_brief.html) (qua/25min)
  * Leitura dos relatórios do mês passado (qua/45min)
  * Leitura de 5 páginas (início da seção "Higher Education") do "Country Education Profiles - Australia 2006" (sex/20min)
  * Busca de informações sobre os ex-petianos (sex/30min)
  * Conversa com o Weber sobre a palestra/debate sobre o ensino da computação (sex/15min)
  * Inscrição do curso de C no sistema de extensão (sex/1h)

==== 16/10 a 22/10 ====

  * Leitura de 5 páginas da seção "Higher Education" do "Country Education Profiles - Australia 2006" (seg/20min)
  * Passeio pelos site do ESOS (Education Services for Overseas Students), CRICOS (Commonwealth Register of Institutions and Courses for Overseas Students) e AEI (Australian Education International) (seg/30min)
  * Passeio pelo site da Imigração Australiana, relativo à vistos estudantis (http://www.immi.gov.au/students/index.htm) (seg/20min)
  * Conversa com o Manuel e envio de email para o Comba e o Jorge, relativo ao Mini salão de Iniciação Científica (ter/25min)
  * Conferida nos grupos de pesquisa listados no site da inf (ter/20min)
  * Três quartos da catalogação de todos os posteres da INF que foram para o SIC (qua/1h45min)
  * Mineração de dados dos ex-petianos e colocação dos dados já conseguidos no trac (qua/1h)
  * Terminei a catalogação de todos os posteres da INF que foram para o SIC (qui/40min)
  * Início do estudo comparativo entre os currículos de computação da ANU e UFRGS - mineração de dados e colocação em planilha, cursos CS with Honours e CIC (qui/1h20min)

==== 23/10 a 30/10 ====

  * Coloquei o currículo da CIC lá no documento comparativo(seg - 15min &amp;&amp; ter/25min)
  * Início da comparação um-a-um das cadeiras da CIC e CS with Honours, primeiro e segundo ano da CS (ter/35min)
  * Conversa com a Silvânia sobre o Mini Salão de IC da Inf (ter/10min)
  * Mandei o email-convite para os integrantes do II com posters no SIC para o mini salão de iniciação do II (qua/15min)
  * Leitura do novo Manual de Orientações Básicas do PET (qua/40min)
  * Update no Dante sobre o andamento da integração ANU-UFRGS (qua/15min)
  * Conversa com o Weber sobre os dois currículos (ANU e UFRGS) e marquei um possível horário pra uma ajuda por parte dele (qua/20min)
  * Mandei denovo o email para os participantes do mini salão (qui/5min)
  * Mandei o email para o Palazzo, pro Dante e para os egressos relativo à palestra/debate sobre o ensino em computação(qui/15min)
  * Dei uma procurada em sites relativos à pesquisa da ANU e NICTA (qui/20min)
  * Tive uma idéia de projeto com processamento de vídeo em paralelo, e procurei referências e artigos na internet e imprimi alguns artigos (qui/40min)
  * Olhada no formato dos planejamentos semestrais dos grupos PET da UFRGS (sex/25min)
  * Leitura do artigo "An Optimal Scheduling Algorithm for Parallel Video Processing" (sex/20min)

===== 30/10 a 06/11 =====

  * Envio, pela terceira vez, do email para os bolsistas de IC do instituto, agora com mensagem pedindo um retorno por email(ter/5min)
  * Criação de uma lis com todas linhas de pesquisa da UFRGS, da ANU e do NICTA, para futura análise e determinação de oportunidades no programa de intercâmbio(ter/25min)
  * Leitura do artigo "An Investigation of JPEG Image and Video Compression Using Parallel Processing" (ter/20min)
  * Palestra da Kaizen sobre VMware (ter/1h15min)
  * Conversa com a administradora da rede sobre o domínio do lri (ter/10min)
  * Leitura do artigo "Efficient Parallel Video Processing through Concurrent communication on a Multi-port Star Network" (ter/25min)
  * Procura na internet por informações que ajudariam em definir e fazer um projeto com vídeo e/ou paralelismo (coisas como portas usb,formato de vídeo Ogg, vfw, que é uma biblioteca pra mexer em vídeo no windows e uma biblioteca chamada ffmpeg)(ter/40min)
  * Análise comparativa de 3/4 do curriculo da ANU, fazendo comparação com a UFRGS (qua/1h15)
  * Digitei a Ata (qua/2h)
  * Leitura do artigo "Gigabit Ethernet-Based Parallel Video Processing" (qua/35min)

***********************************************************************************************************************************

===== Mizusaki =====

==== 09/10 a 15/10 ====

  * Terminei a codificação do programa em Ruby para a comunicação do [[/trac/wiki/RoboEdu|RoboEdu?]] (terça);
  * Ida à reunião com o pessoal do CV para ver o curso de informática para a comunidade (terça -45m);
  * Mais trabaho sobre o código do [[/trac/wiki/RoboEdu|RoboEdu?]], documentação e muitos testes (quarta);

==== 16/10 a 22/10 ====

  * Mais trabalho sobre o código do [[/trac/wiki/RoboEdu|RoboEdu?]], implementação de um programa para controlar 2 robôs (segunda - 30m);
  * Ida ao salão jovem da UFRGS para saber a nossa participação, também fiz uma apresentação de slides e tive que arranjar um computador para levar para lá. Também participei da apresentação (terça, quarta e quinta de manhã);
  * Arrumação rápida do LRI ao trazer os equipamentos de volta (sexta - 30m);
  * Fiz o relatório (sexta - 1h);

==== 23/10 a 30/10 ====

  * Montagem do segundo PC (terça);
  * Procura por chaves para instalar o Windows nos novos computadores (terça);
  * Pesquisa sobre Ruby, que não é mais A linguagem de programação... (terça);
  * Atividades de administrador: Instalação dos Windows, Configuração, Mudança das Senhas (quarta+quinta);
  * Preparos para a palestra do [[/trac/wiki/RoboEdu|RoboEdu?]] + realização(quinta);

==== 30/10 a 6/11 ====

  * Prototipação do Acoplamento do [[/trac/wiki/RoboEdu|RoboEdu?]] (terça);
  * Instalação do Windows no Veryon, programas e atualização de senhas (terça - 40m, quarta);

***********************************************************************************************************************************

===== Pablo =====

==== 09/10 a 15/10 ====

  * Criei os graficos com as estatisticas do questionario aplicado no minicurso do salão de extensão(disponivel no pet-docs;
  * Calculei as médias do questionario de avaliação aplicado no curso de HTML/PHP e editei um documento(disponivel no pet-docs);
  * Passei todos meus arquivos que estavam espalhados em varios computadores pro pet-docs, inclusive o site do pet(pasta SITE);
  * Editei o planejamento 2006_2 (setembro a dezembro) e enviei pro grupo para complementos na descrição dos projetos;
  * Fiz o documento com os dados dos ex-petianos e enviei para o grupo, dividindo 7 ex-petianos para cada um do grupo pesquisar.

==== 16/10 a 22/10 ====

  * Fiz alterações no planejamento 2006_2 sugeridas pelo Dante, imprimi e coletei as assinaturas;
  * Andamento no processo de ação de extensão relativo ao curso de html, assinaturas, impressões e passagem pra fase registro;
  * Criei o link no trac referente ao histórico dos ex-petianos e a tabela com dados, para que cada petiano vá inserindo os dados dos 7 ex-petianos que ficou responsável pela pesquisa;
  * Inserção dos links do roboedu, iserção do link da geografia e outras atualizações no site do PET;
  * Pesquisa referente aos 7 ex-petianos que fiquei responsavel;
  * Coloquei as fotos do enapet no site do pet;

==== 23/10 a 30/10 ====

  * Edição do planejamento mensal junto ao Ismael, passando a tarefa a ele;
  * Explicação de como funciona o site do pet, sistema de arquivos, atualizações, dreamweaver, ftp, etc.. para Gabi e Antonio, passando eles a serem os responsaveis pelo site a partir de agora;
  * Edição da folha de efetividade junto ao Ismael, passando a tarefa a ele;
  * Mostrei o material do portal da Cenapet, email do vinicius da USP, etc.. ao Antonio;
  * Atualizações no site do pet, inserindo em alguns links os novos petianos e exclusão dos antigos;

***********************************************************************************************************************************

===== Ricardo =====

==== 09/10 a 15/10 ====

  * Teste de alguns componentes da placa que está com defeito [todos funcionam até agora] (Terça, muito tempo)

==== 16/10 a 22/10 ====

  * Segundo encontro no Tubino (Segunda)
  * Pesquisa dos dados de ex-petianos [coloquei no trac e adicionei 2 campos à tabela] (Terça)
  * Leitura dos relatórios do mês passado (Terça)
  * Montagem do PC novo (Terça)
  * Salão jovem (Quinta, toda a tarde)

==== 23/10 a 29/10 ====

  * Leitura da ata passada (Terça)
  * Testes com o gravador de PIC (Terça)
  * Ajuda na montagem do 2º PC (Terça)
  * Explicação para Ismael de algumas coisas do TRAC (Terça)
  * Apresentação do LRI ao Ismael (Terça)
  * Inserção planejamento prograd (Quinta)
  * Terminei de ler o artigo sobre Mindstorms da RITA (Quinta)
  * Testes com os robôs [4 PICs funcionando e com programa, 1 PIC aparentemente não funciona, 3 robôs funcionando, 2 placas com defeito] (Quinta)
  * Mostramos algumas coisas do LRI para o Antônio (Quinta)
  * Reunião RoboEDU (Quinta)
  * Mudei o vídeo do PC novo pra 128 e 256MB, mas nem assim melhorou, então voltei pra 64MB (Sexta)

==== 30/10 a 06/11 ====

  * Prototipação do Acoplamento do [[/trac/wiki/RoboEdu|RoboEdu?]] (Terça)
  * Comecei a ler o "Desbravando o PIC" (Terça)
  * Li um pouco do código do PIC do RoboPET/RoboEDU e algo mais sobre os registradores do PIC (Terça)
  * Fui com o Antônio ao NELE para ver o equipamento da Finlândia ([[/trac/wiki/Ter%C3%A7a/Quarta|Terça/Quarta?]])
  * Explicação de algumas coisas sobre o trac e o logador para o Victor (Terça)
  * Fui com o Capra e o Victor (e o Estevão =P) ao NELE novamente [faltam muitos componentes na placa, trouxemos para o LRI] (Sexta)

***********************************************************************************************************************************

===== Thiago =====

LEGENDA\\  Quasi-Newton = algoritmo numérico para obter a energia mínima de um sistema complexo\\  CV = Centro de Vivência, aquele lugar perto do RU\\  btw = by the way = por enquanto \\  Front-end = interface externa da entrada de um programa \\  SQL = Structured Query Language, linguagem para fazer consultas em um banco de dados (a grosso modo) \\

==== 09/10 a 15/10 ====

  * Ida à reunião com o pessoal do CV para ver o curso de informática para a comunidade (terça -45m)
  * E-mail para o pessoal do CV (terça 20min)
  * Procura por ex-petianos
  * Algumas simulações com o algoritmo Quasi-Newton
  * Programei mais do front-end SQL para o projeto do heuser

==== 16/10 a 22/10 ====

  * Ajuda ao Zambiasi com o código em PERL do projeto wikigraph (ter, qua, qui)
  * Ida ao salão jovem (qua e qui)

==== 23/10 a 30/10 ====

  * Reunião com a profª Rute
  * Tentativa de instalar o ubuntu no pc novo
  * Ajudei o Zambiasi com o código perl (quarta e quinta)
  * Reunião RoboEDU (Quinta)
  * Reunião com a Buriol e a Vivi (quinta - 45min)
  * Preparos para a palestra do [[/trac/wiki/RoboEdu|RoboEdu?]] + realização(quinta);

==== 30/10 a 06/11 ====

  * Alcancei o Zambiasi na documentação do passos
  * Conversa(15min) com a Buriol e o Thiago a respeito do andamento do projeto (quarta)
  * Pequena reunião com o Sérgio (doutorando do Heuser) sobre o projeto (quarta)
  * Aula extra do curso de c (quarta)

***********************************************************************************************************************************

===== Victor =====

==== 30/10 a 06/11 ====

  * Reunião de ingresso no PET (terça).
  * Inserção dos dados pessoais no TRAC, bem como os horários (quarta).
  * Visita ao LRI (quarta).
  * Fui com o Capra e o Ricardo (e o Estevão =P) ao NELE novamente [faltam muitos componentes na placa, trouxemos para o LRI] (Sexta)
