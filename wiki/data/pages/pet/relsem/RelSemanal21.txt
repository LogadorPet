====== Relatorio semanal de 05 de dezembro de 2005 a 08 de janeiro de 2006 ======

OBS.: Deixar em ordem alfabetica.

OBS.: Semana comec,a na segunda e termina na outra segunda.

===== Diego =====

==== 05/12 a 11/12 ====

  * Comecei a organizar o material do Dante (CENAPET) 30min

==== 12/12 a 18/12 ====

  * Leitura da Pet-br (1h)

==== 19/12 a 25/12 ====

  * Organizada no Trac (30 min)
  * Robopet (3h)
  * Exercicio de tarefas administrativas da rede Linux (1h)

==== 02/01 a 09/01 ====

  * Resolvido ticket [[/trac/ticket/76|#76]] (clientes windows) (30 min)
  * Resolvi alguns problemas do Linux pro Gustavo (20 min)
  * Listagem de programas windows instalados (20 min)
  * Ajuda pro Thiago no simulador de arquitetura (3h)
  * Slides GTK (3h)

==== 09/01 a 16/01 ====

  * Organizac,ao do TRAC: 30 min
  * Projeto Bolivar (simulador em GTK+ para os simuladores de Arquitetura): 4h
  * Pesquisa sobre GtkDrawArea: 2h
  * Resoluc,ao de problemas da Angelina: 1h

==== 17/01 a 24/01 ====

  * Artigo Bolivar para o FISL 7.0 (6h)

===== Estevao =====

==== 02/01 a 08/01 ====

  * Leitura e comprensao da linguagem de programac,ao C.
  * Planejamento e discussao acerca do Museu e [[/trac/wiki/GlobalTech|GlobalTech?]].
  * Explicac,oes iniciais acerca do [[/trac/wiki/RoboPet|RoboPet?]] (funcionamento).
  * Leitura e comprensao do microcontrolador PIC16F628A .

==== 09/01 a 13/01 ====

  * Aprendendo a programar em C (ainda).
  * Leitura do datasheet do microcontrolador PIC16F628A .
  * Definic,ao da exposic,ao para a [[/trac/wiki/GlobalTech|GlobalTech?]].
  * Inicio da catalogac,ao dos itens para o Museu.
  * Concepc,ao de uma base teste.

==== 17/01 a 24/01 ====

  * Aprendendo a programar em C (ainda).
  * Estudando o codigo do PIC 16F628A feito pelo ITAMIR .
  * Teste de material interativo para a [[/trac/wiki/GlobalTech|GlobalTech?]] (com sucesso)
  * Catalogac,ao de pec,as do Museu

==== 23/01 a 29/01 ====

  * Analise do codigo feito pelo Itamir (PIC 16F628A).
  * Fotografias e organizac,ao das pec,as do museu .
  * Ida `a pec,a //Homens de Perto//;

===== Fabiano =====

==== 05/12 a 11/12 ====

  * Lista, para fazer orc,amentos, de materias pro LRI;
  * Busca de algumas informac,oes/ideias para o projeto do RoboEdu (internet);

==== 12/12 a 18/12 ====

  * Busca de mais algumas informac,oes/ideias para o projeto do RoboEdu, leitura de varios sites (internet);
    * http://www.robohp.hpg.ig.com.br/roboed_quadro.htm;
  * Orc,amentos dos materias do LRI;
  * Compra dos materias para o LRI;
  * Inicio da leitura do livro "Utopias do Brasil";
  * Inserc,ao da ata da reuniao;

==== 19/12 a 25/12 ====

  * Organizac,ao do novo material do LRI e da sala do LRI; (segunda feira)
  * Termino da leitura do livro "Utopias do Brasil";
  * RoboPet; (terc,a feira)
  * Inicio do aprendizado do Pcad (software para desenhar placas de circuitos impresso); (quarta)

==== 02/01 a 08/01 ====

  * Substituic,ao de alguns componentes de uma placa do robopet que nao estava funcionando por um drive da texas para teste, com sucesso; (terc,a, quarta e quinta)

==== 09/01 a 16/01 ====

  * Inicio do desenvolvimento de um documento que consta a evoluc,ao do Robopet, pesquisas de datas e descric,oes de eventos; (terc,a, quarta manha)
  * Aperfeic,oamento da montagem do robo com chassi novo; (quarta tarde)
  * Confecc,ao de 5 suportes para as placas nos novos chassis do Robopet; (quarta tarde)
  * Troca do sugador de solda e aquisic,ao de uma fonte (para o gravador de pic) e suporte para as pilhas; (quinta de tarde)

==== 17/01 a 23/01 ====

  * Continuac,ao do desenvolvimento do documento evoluc,ao_robopet; (segunda tarde,terc,a a tarde)
  * Organizac,ao do material comprado no LRI; (quarta a tarde)
  * Montagem de 3 suportes de pilhas (estanhar suportes e conectores) para os robos (quarta a tarde)
  * Contato por telefone e e-mail, primeiramente com a Camila e posteriormente com a Marininha (ambas da PROPESQ) a respeito da compra das pilhas e do computador com a verba de R$ 5000,00. (quarta a tarde)
  * Inserc,ao dos provaveis horarios de 2006/1 no trac. (quarta a tarde)
  * Contato com Luis Otavio e com Andre Scheneider sobre o PC "saqueado" na Globaltech.

==== 24/01 a 28/01 ====

  * Conversa com Luis Otavio para agilizar a compra de uma CPU inteira para o PET com a verba do "furto" na globaltech. (segunda tarde)
  * Fotografias para o documento da evoluc,ao do robopet. (segunda tarde)
  * Continuac,ao do desenvolvimento do documento evoluc,ao_robopet; (segunda tarde,terc,a a tarde)

===== Felipe =====

==== 06/02 a 12/02 ====

  * Leitura do Trac, vendo as atividades que foram feitas em Janeiro no período em que eu estava viajando, bem como as atas das reuniões do mesmo mês
  * Visita ao acervo do Museu
  * Leitura do material do Museu
  * Início da catalogação das peças do acervo
  * Identificação de peças já catalogadas na sala da biblioteca
  * Leitura do livro "Fortaleza Digital"
  * Início do aprendizado da linguagem de programação C

==== 13/02 a 19/02 ====

  * Catalogação de peças do acervo do Museu
  * Identificação de peças já catalogadas na sala da biblioteca
  * Continuação do aprendizado da linguagem de programação C

==== 20/02 a 26/02 ====

  * Catalogação de diversas peças (agora também de calculadoras) do acervo do Museu
  * Identificação de peças já catalogadas na sala da biblioteca
  * Continuação do aprendizado da linguagem de programação C

===== Gustavo =====

==== 05/12 a 11/12 ====

  * Desenvolvimento dos exemplos do curso de html
  * Desenvolvimento de uma pagina de orientac,ao do curso

==== 12/12 a 18/12 ====

  * Coleta dos interessados no curso de HTML
  * Email informando o cancelamento do curso de HTML
  * Contato com os candidatos sobre o dia da entrevista
  * Leitura de paginas e projetos sobre a Legislac,ao contra cyber-crimes
  * Entrevista dos Bolsistas

==== 19/12 a 25/12 ====

  * Ajuste da IA do Goleiro
  * Descric,ao do conteudo dos CDs
  * Elaborac,ao dos oficios para os novos bolsistas

==== 02/01 a 08/01 ====

  * Leitura do Projeto [[/trac/wiki/ProLiFa|ProLiFa?]]
  * Auxilio ao Diego na criac,ao de uma lista com os softwares existentes em cada maquina
  * Envio de email para o Prof. Valdeni e para o Alan Carvalho
  * Conversa com o pessoal da administrac,ao da rede sobre a possibilidade de hospedar o site do pet no servidor
  * Fiz uma proposta para ser apresentada na reuniao com a Pro-reitora de Extensao sobre o curso de HTML
  * Reuniao com pro-reitora de extensao
  * Instalacao do software Gnopernicus
  * Cadastro das atividades de extensao (Portas Abertas 2004, Mostra Inova UFRGS, Globaltech).
  * Visita ao acervo do museu
  * Estudo de PHP

==== 09/01 a 16/01 ====

  * Ajuda ao Dante na edic,ao do [[/trac/wiki/PowerPoint|PowerPoint?]]
  * Estudo da linguagem SMIL

==== 17/01 a 24/01 ====

  * Leitura do Livro "Fortaleza Digital"
  * Desenvolvimento do cadastro dos Petianos e Antigos Petianos

===== Marcos =====

==== 02/01 a 06/01 ====

  * Aprimoramento dos conhecimentos de Computac,ao Grafica atraves da leitura do livro 'Interactive Computer Graphics' de Edward Angel
  * Coleta de fontes de informac,oes para o Museu de Informatica
  * Retirada do livro 'Historia da Informatica' de Philippe Breton

==== 09/01 a 13/01 ====

  * Aprimoramento dos conhecimentos de Computac,ao Grafica atraves da leitura do livro 'Interactive Computer Graphics' de Edward Angel e 'OpenGL [[/trac/wiki/SuperBible|SuperBible?]]' de Richard Wright
  * Definic,ao da exposic,ao para a [[/trac/wiki/GlobalTech|GlobalTech?]]
  * Inicio da catalogac,ao dos itens para o Museu

==== 17/01 a 20/01 ====

  * Aprimoramento dos conhecimentos de Computacao Grafica atraves da leitura do livro 'Interactive Computer Graphics' de Edward Angel e 'OpenGL [[/trac/wiki/SuperBible|SuperBible?]]' de Richard Wright
  * Teste de material interativo para a [[/trac/wiki/GlobalTech|GlobalTech?]]
  * Definic,ao do metodo para seriac,ao para o museu
  * Primeiros contatos com [[/trac/wiki/OpenGl|OpenGl?]]
  * Catalogacao dos itens para o Museu
  * Ida `a pec,a 'Humor em Dose Dupla'

==== 23/01 a 27/01 ====

* Estudo de CG e OpenGL * Leitura de Livro "Fortaleza Digital" * Organizada no Museu

===== Nondillo =====

==== 05/12 a 11/12 ====

  * Leitura da pet-br; (1h)
  * Treinamento no software PCB (para desenhar placas); (1,5h)
  * Criac,ao e impressao de um layout teste do regulador de tensao de 6V para os motores do RoboPET utilizando o software PCB; (1h)

==== 12/12 a 18/12 ====

  * Leitura da pet-br; (1h)
  * Ajustes nas RI e colocac,ao das aprovadas no TRAC; (30 min)

==== 19/12 a 25/12 ====

  * Instalac,ao dos coolers e reativac,ao da angelina; (30 min)
  * Instalac,ao do P-CAD na Fernanda para testes; (20 min)
  * Aprendizado do software protel com a criac,ao de um circuito simples inclusive com roteamente automatico do mesmo; (1h)

==== 02/01 a 09/01 ====

  * Instalac,ao do Protel na Angelina; (20 min)
  * Explicac,ao inicial sobre o projeto robopet, pic e desenvolvimento de placas para o Estevao; (3h)
  * Procura, instalac,ao e testes com compiladores C para PIC e simuladores, tanto para windows como para linux; (3h)
  * Inicio do projeto da placa do RoboPET2006; (1h)
  * Organizac,ao inicial do projeto RoboPET 2006 no SVN; (30 min)

==== 09/01 a 15/01 ====

  * Instalac,ao do nero na Ellen e ajuste de permissoes para que qualquer usuario possa gravar CD; (1h20min)
  * Procura, instalac,ao em todos windows e testes com o compilador PICC para PICs. (2h)

==== 16/01 a 22/01 ====

  * Instalac,ao do compilador PICC no linux em carater experimental; (40 min)
  * Analise do codigo para o PIC criado pelo Itamir, com explicac,oes para o Estevao; (3h)
  * Ajustes no repositorio SVN do robopet e explicac,ao de seu funcionamento para Robert, Estevao e Fabiano; (2h)
  * Instalac,ao do AutoCAD na Daniela; (30 min)
  * Estudo da ferramenta PROTEL DXP; (1h e 30min)
  * Estudo de novo microcontrolador que possua 2 PWMs - ainda nao definido; (1 h)

==== 23/01 a 29/01 ====

  * Procura de um PIC melhor para o proximo RoboPET - falta verificar facilidade de compra e prec,o; (1h)
  * Estudo sobre gravadores PIC; (40 min)
  * Engenharia reversa do gravador PIC do colaborador Rodrigo - consequente descoberta do nao sucesso na ultima tentativa de gravac,ao; (3h)
  * Gravac,ao de um PIC com um programa teste do Estevao; (1h)
  * Conserto da Ellen que nao ligava, descobrindo o problema atraves do codigos dos beeps do BIOS; (20 min)
  * Compilac,ao dos documentos de Auto-avaliac,ao do grupo e Avaliac,ao do tutor pelo grupo; (1h e 30 min)
  * Criac,ao de modelo de documento para descric,ao completa do projeto RoboPET; (1h)
  * Pesquisa de prec,o para a escolha de um novo PIC para o RoboPET;
  * Finalmente consegui criar componentes no Protel DXP - Criac,ao do componente PIC; (3h)
  * Criac,ao do [[/trac/wiki/FootPrint|FootPrint?]] do componente acima citado; (1h)
  * Leitura da Pet-br; (1h)

===== Paula =====

==== 05/12 a 11/12 ====

  * Slides do curso de html;
  * Leitura do livro " A utopia do Brasil"

==== 12/12 a 18/12 ====

  * Coleta de alguns interessados no curso de html;
  * Leitura de um artigo de processamento de imagens;

==== 19/12 a 25/12 ====

  * Estudo de Java;
  * Li o primeiro capitulo do livro "Processamento de Imagens Digitais"

==== 02/01 a 08/01 ====

  * Estudo de Java;
  * Reuniao com a Pro-Reitora de Extensao;
  * Leitura do material do museu;

==== 09/01 a 13/01 ====

  * Estudo de java;
  * Leitura do material do museu;
  * Visita ao acervo, na biblioteca;
  * Catalogac,ao da primeira pec,a;

==== 29/01 a 05/02 ====

  * Leitura das atas das reunioes das semanas que faltei;
  * Catalogacao de diversas pec,as do museu;
  * Identificac,ao de algumas pec,as ja catalogadas, na sala da biblioteca;

==== 06/02 a 12/02 ====

  * Catalogacao de peças do museu;
  * Formulario para catalogar as peças;
  * Estudo de php;

===== Paulo =====

==== 05/12 a 11/12 ====

  * Pet-BR (20 min)
  * Configurac,ao do SAMBA para utilizar usuarios no LDAP, criac,ao de usuarios e testes. (2h)

==== 12/12 a 18/12 ====

  * Continuac,ao da instalac,ao do servidor.

==== 1/1 a 7/1 ====

  * Cadastro do Portas Abertas 2004 e da Mostra Inova UFRGS.
  * Estudo sobre o uso do [[/trac/wiki/GnomeCanvas|GnomeCanvas?]] para o desenvolvimento de interfaces interativas.

==== 8/1 a 14/1 ====

  * Backup do servidor velho (6h)
  * Transferencia dos arquivos para o novo servidor (2h)
  * Fazer o novo servidor bootar corretamente (2h 30min)
  * Fiz funcionar logagem remota via LDAP no Linux e Samba no Windows (4h)
  * Trac no ar (30 min)
  * Discussao sobre a portaria do PET (2h 20min)
  * Banho de chuva e confraternizac,ao interpet (extra-oficial) no Cavanhas (2h)
  * Instruc,oes para configurac,ao dos clientes Windows (40min)
  * Configurac,ao dos clientes Linux e Windows (com aprendiz Vicente) (1h)
  * Colocac,ao do Presenc,a TNG no ar (1h 30 min)
  * Colocac,ao do Subversion no ar, e importac,ao dos repositorios (1h)
  * Visao geral do Presenc,a TNG no Trac (20 min)
  * Criac,ao de script de inicializic,ao do Presenca-TNG, no formato do Gentoo, com controle de dependencias (20 min)
  * Criac,ao de script para a automatizac,ao da criac,ao de novos usuarios (pet-adduser), que deve ser usado sempre. Assim, mesmo que mudemos o sistema de contas o processo de criac,ao continua o mesmo. Cria repositorio Subversion. (30 min)
  * Resoluc,ao do Ticket [[/trac/ticket/70|#70]] (Horario do Servidor) (15 min)
  * Resoluc,ao dos Ticket [[/trac/ticket/27|#27]] (Remover Apache 1.x), [[/trac/ticket/32|#32]] (Nova maquina do servidor), [[/trac/ticket/28|#28]] (Limpeza no servidor) e [[/trac/ticket/75|#75]] (Profile no Windows nao funciona), (efeito colateral do novo servidor).

==== 15/1 a 21/1 ====

  * Organizac,ao e Participac,ao no I Campeonato Integrac,ao PET/PFT-Dell/GME de Xadrez Rapido (1h 40min)
  * Resoluc,ao do Ticket [[/trac/ticket/83|#83]] (Configurar Login e Mouse Wheel na Daniela) (30 min)
  * Transferencia de arquivos da pagina e criac,ao de banco de dados para o Chiechelski. (30 min)
  * Ajustes no servidor: atualizac,ao do script pet-adduser para adicionar link do pet-docs no home do usuario, correc,ao dos arquivos de configurac,ao do NFS, instalac,ao do reiserfsprogs. (2h)
  * Resolvi ticket [[/trac/ticket/85|#85]] (disponibilizar trac robopet, pagina do pet e do robopet) (1 h)

===== Robert =====

==== 05/12 a 11/12 ====

  * Verificac,ao dos relatorios semanais;
  * Verificac,ao das horas do ultimo mes;
  * Compilac,ao e envio por e-mail dos relatorios semanais do ultimo mes;
  * Informac,oes com o Dante a respeito da apresentac,ao do PET na PUC;
  * Coleta de informac,oes para criac,ao da apresentac,ao;
  * Finalizac,ao dos 41 slides para a apresentac,ao juntamente com o Thiago.

==== 12/12 a 18/12 ====

  * Retirada dos nomes dos ex-petianos do logador e de certos lugares do TRAC;
  * Verificac,ao dos relatorios semanais;
  * Estudo sobre TeX;
  * Preparac,ao de 29 slides para o seminario interno.

==== 19/12 a 25/12 ====

  * Verificac,ao dos relatorios semanais;
  * Organizac,ao do Planejamento Mensal para a Prograd;
  * Leitura do livro //A Utopia Do Brasil//;
  * Avaliac,ao de 5 horas pendentes;
  * Ajuda no cadastro dos novos bolsistas;
  * Organizac,ao para entrega da Folha de Efetividade;
  * Compilac,ao e envio dos Planejamentos Mensais `a PROGRAD.

==== 02/01 a 08/01 ====

  * Solicitac,ao de reparo do piso da sala junto `a Silvania;
  * Iniciativa para a devoluc,ao de parte das bolsas dos ex-petianos;
  * Avaliac,ao de 3 horas pendentes;
  * Repasse das horas do mes passado para este mes;
  * Verificac,ao dos relatorios semanais, da ultima ata e dos tickets quanto ao andamento das atividades.

==== 09/01 a 15/01 ====

  * Migrac,ao para Linux: aprendendo a utilizar o Gimp;
  * Estudo de formatos de artigo;
  * Validac,ao das horas de todos referentes a 10/01 e a 11/11;
  * Ida ao InterPET //Discussao da Portaria// e `a confraternizac,ao extra-oficial no Cavanhas;
  * Adic,ao de 1 falta em reuniao para o Thiago no Controlador de Horarios.

==== 16/01 a 22/01 ====

  * Elaborac,ao do Relatorio de Atividades do Grupo 2005;
  * Verificac,ao dos relatorios semanais e da ultima ata para acompanhar andamento das atividades;
  * Elaborac,ao de um e-mail a ser enviado pelo Prof. Dante para devoluc,ao parcial de bolsas de ex-petianos;
  * Inicio do estudo do radio do RoboPET.

==== 23/01 a 29/01 ====

  * Avaliac,ao de 5 horas pendentes;
  * Adic,ao de 1 falta em reuniao para o Thiago no Controlador de Horarios;
  * Finalizac,ao do Relatorio de Atividades do Grupo 2005;
  * Ida `a pec,a //Homens de Perto//;
  * Verificac,ao dos relatorios semanais;
  * Participac,ao nas auto-avaliac,oes do grupo e do tutor;
  * Adic,ao de 1 falta em reuniao para o Paulo no Controlador de Horarios;
  * Compilac,ao e envio por email dos relatorios semanais.

===== Thiago =====

==== 05/12 a 11/12 ====

  * Auxilio na confecc,ao das transparencias da PUC
  * Instalac,ao do fluxbox na zulmira

==== 12/12 a 18/12 ====

  * Auxilio ao Nondillo no programa do brac,o Robo
  * Selec,ao dos Bolsistas

==== 19/12 a 25/12 ====

  * Pesquisa de bibliografia para as ferias

==== 01/01 a 07/01 ====

  * Inicio da programac,ao das interfaces dos simuladores
  * Instalac,ao e configurac,ao da Zulmira (agora operacional)
  * Auxilio aos novos Petianos com o Museu

==== 08/01 a 14/01 ====

  * Simuladores de Arquitetura com o Diego

==== 15/01 a 21/01 ====

  * Nao pude comparecer ao pet regularmente (Motivo de saude)

==== 22/01 a 28/01 ====

  * Leitura do livro "A Pratica da Programac,ao"
  * Reuniao LIFAPOR
  * Pesquisa para o projeto LIFAPOR
  * Simuladores de Arquitetura

==== 29/01 a 04/02 ====

  * Tarefas da rede
  * Simuladores de Arquitetura

==== 12/02 a 18/02 ====

  * Escrevendo o artigo para o Fisl 7.0
  * Tratamento dos displays para o simulador

===== Vicente =====

  * RESPONSAVEL POR ENTREGAR O BOLETIM DE EFETIVIDADE ATE O DIA 25 DE CADA MES

==== 02/01 a 06/01 ====

  * Aprendendo o funcionamento interno no PET
  * Ingressei no projeto de criac,ao dos simuladores do Weber
  * Aprendendo Linux
  * Relembrando PHP, MySQL e HTML para auxiliar na construc,ao da [[/trac/wiki/HomePage|HomePage?]] do PET

==== 09/01 a 13/01 ====

  * Aprendendo GTK para auxiliar na construc,ao dos Simuladores
  * Relembrando PHP, MySQL e HTML para auxiliar na construc,ao da [[/trac/wiki/HomePage|HomePage?]] do PET
  * Aprendendo Linux
  * Auxilio ao Paulo na reinstalac,ao do servidor do PET e configurac,ao do mesmo nas estac,oes de trabalho
  * Catalogac,ao do primeiro item para o museu de informatica

==== 16/01 a 20/01 ====

  * Finalizando aprendizado basico de Linux
  * Reuniao sobre o projeto [[/trac/wiki/LiFaPor|LiFaPor?]] na Engenharia Eletrica
  * Museu de informatica

==== 25/01 (unico dia no PET) ====

  * Finalizando aprendizado basico de Linux para auxilio a rede do PET
