====== Relatório das semanas de 31 de maio a 28 de junho de 2005 ======

===== Robert =====

==== 17/5 à 24/5 ====

  * Ida ao interpet;
  * Jogo contra FURG (Globaltech).

==== 24/5 à 31/5 ====

  * Tratamento e colocação das fotos da //Mostra Inova//, //Portas Abertas// e //Globaltech// na página.

==== 31/5 à 7/6 ====

  * Ida ao Interpet na quarta-feira;
  * Contato com o Ezequiel sobre o recurso do SulPET;
  * Colocação de cartazes e distribuição de //flyers// por todos os pontos estratégicos do Campus do Vale.

==== 7/6 à 14/6 ====

  * Ida ao Interpet na terça-feira;
  * Distribuição de //flyers// pelo Campus do Vale na terça-feira;
  * Mini-reunião do hardware do //RoboPET// na terça-feira para definir responsabilidades;
  * Colocação de cartazes na Arquitetura (com distribuição de //flyers//) e no Antônio do centro na quarta-feira;
  * Mini-reunião do hardware do //RoboPET// na quinta-feira para definir prazos;
  * Na ESEF, quinta-feira à tarde, entrega de ingressos e colaboração nos preparativos para festa: bebidas no galpão, cartões de cerveja, venda de ingressos;
  * Participação ativa trabalhando na festa (bar da rua).

==== 14/6 à 21/6 ====

  * Participação no curso desenvolvimento de equipes.

===== Nondillo =====

==== 17/5 à 24/5 ====

  * Compra de parafusos e baterias;
  * Montagem dos chassis dos 5 robôs;
  * Cartaz da festa;
  * Ingressos da festa;
  * Pesquisa de gráfica.

==== 24/5 à 31/5 ====

  * Elaboração, confecção e distribuição de cartazes, penfletos e ingressos para a festa do PET;
  * Confecção de carimbo para os ingressos.

==== 31/5 à 7/6 ====

  * Tentativa frustrada de imprimir os ingressos (arquivo muito pesado);
  * Remodelamento dos ingressos da festa;
  * Impressão dos ingressos (gráfica), "carimbagem", autógrafo (dos ingressos), corte e DISTRIBUIÇÃO para todos os PETs e Antônios;
  * Contatos para a fabricação do acoplamento motor-redução. Ainda sem sucesso.
  * Trabalho no acoplamento do encoder com o motor.

==== 7/6 à 14/6 ====

  * Ida ao Interpet na terça-feira;
  * Mini-reunião do hardware do //RoboPET// na terça-feira para definir responsabilidades;
  * Mini-reunião do hardware do //RoboPET// na quinta-feira para definir prazos;
  * Coleta do dinheiro dos ingressos vendidos nos Antônios;
  * Acoplamento experimental do motor com a redução e desta com o encoder. Necessita melhorias.

==== 14/6 à 21/6 ====

  * Interpet na terça-feira - acerto de contas;
  * Contatos com sucesso para criação do acoplamento motor-redução, que deverá estar pronto em breve;
  * Finalização do acoplamento motor-redução-encoder do protótipo.

===== Rubin =====

==== 17/5 à 24/5 ====

  * Levei o Nomad pra Globaltech;
  * Início do //banner// pra página do Xexéuski.

==== 24/5 à 31/5 ====

  * Devido ao feriado e duas provas so fizemos mais uns 5 cabos de rede para a escola!

==== 31/5 à 7/6 ====

  * Distribuição de flyers na saida do vale na quinta feira
  * Ida a escola para a finalização das configurações das maquinas.(ainda faltam duas)

==== 7/6 à 14/6 ====

  * Ajustes do oficio para o dante.
  * Distribuicao dos flyers na fila do RU e na parada dos onibus.
  * Ida mais cedo para a organizacao da festa!

==== 14/6 à 21/6 ====

  * Eu e o irigon fomos no deposito da ufrgs para classificacao de 20 computadores que serao doados a escola onde estamos realizando o projeto.
  * Ajuda ao nondillo para o orcamento das novas pecas para o pet.

===== Maurício =====

==== 17/5 à 24/5 ====

  * Montagem dos robôs (Robopet)
  * Jogo contra FURG (Globaltech)

==== 24/5 à 31/5 ====

  * Estudos para provas de Análise de Circuitos e Equações Diferenciais.

==== 31/5 à 7/6 ====

  * Assisti Star Wars III

==== 7/6 à 14/6 ====

  * Mini-reunião do hardware do //RoboPET// na terça-feira para definir responsabilidades;
  * Mini-reunião do hardware do //RoboPET// na quinta-feira para definir prazos;
  * Ajuda na análise do planejamento do Projeto PRIBAG de acordo com as diretrizes do programa UNIBRAL.

==== 14/6 à 21/6 ====

  * Pesquisa sobre possíveis tipos de baterias para os robôs do ROBOPET.

===== Mateus =====

==== 17/5 à 24/5 ====

  * Leitura do livro [[/trac/wiki/SisOp|SisOp?]];
  * Entrei em contato com o Luis Otavio sobre a utilizacao dos laboratorios (explicação na reunião).

==== 24/5 à 31/5 ====

  * Leitura do livro de [[/trac/wiki/SisOp|SisOp?]]

==== 31/5 à 7/6 ====

  * Leitura do livro de [[/trac/wiki/SisOp|SisOp?]] (faltam 2 capítulos);
  * Ajudei a cortar, carimbar e ordenar os ingressos para a festa;
  * Distrubuí folhetos e colei cartazes pelo Campus com o Robert e o Irigon.

==== 7/6 à 14/6 ====

  * Acabei a leitura do livro de [[/trac/wiki/SisOp|SisOp?]];
  * Fui com o Irigon na escola, ajudei ele a trocar as placas de rede que funcionavam para as máquinas que nao tinham CD.

==== 14/6 à 21/6 ====

  * Curso de desenvolvimento de equipes

===== Diego =====

==== 17/5 à 24/5 ====

==== 24/5 à 31/5 ====

  * Preparativos finais pra apresentação no FISL 6.0
  * Terminando as pressas o novo modulo de visão do [[/trac/wiki/RoboPet|RoboPet?]]

==== 31/5 à 7/6 ====

  * Apresentação no FISL do artigo sobre o [[/trac/wiki/RobopetVision|RobopetVision?]];
  * Assisti palestras do Forum na quinta, sexta e sábado;
  * Li alguns artigos sobre o GNU/Hurd, pouca coisa.

==== 7/6 à 14/6 ====

  * Instalação do Debian/Hurd no meu micro em casa e na máquina Fernanda do PET
  * Reimplementação de algumas classes do [[/trac/wiki/RobopetVision|RobopetVision?]]

==== 14/6 à 21/6 ====

  * Pesquisa sobre //ratings// de xadrez, para possivel apresentação no Salão de IC

===== João =====

==== 17/5 à 24/5 ====

  * Conclusão do relatório dos questionários.
  * Jogo contra FURG (Globaltech)
  * Leitura do artigo de Rafael Zancan Frantz sobre novidades de java versão 5.0 (generics, for-each, autoboxing, varargs, import static, enumaration).
  * Montei 1 cabo de rede.

==== 24/5 à 31/5 ====

  * Leitura do artigo "Genetic Algorithms for Object Localization in a Complex Scene"
  * Leitura do artigo "A Genetic Algorithm with Coverage for Object Localization"
  * Término do trabalho 1 de CPD

==== 31/5 à 7/6 ====

  * Assisti palestras do Forum na quinta e sexta;
  * Ajudei a carimbar ingressos para a festa;
  * Comecei a tentar fazer uma interface p/ projetinho algoritmos genéticos.

==== 7/6 à 14/6 ====

  * Distribuição de //flyers// pelo Campus do Vale na terça-feira;
  * Continuação da interface (abertura da imagem) do projeto algoritmo genético.

==== 14/6 à 21/6 ====

  * Curso de desenvolvimento de equipes
  * Continuação da interface (seleção de uma sub-imagem) do projeto algoritmo genético.

===== Gustavo =====

==== 17/5 à 24/5 ====

  * Início do estudo de PHP (para criação de um FAQ)
  * Jogo contra FURG (Globaltech)

==== 24/5 à 31/5 ====

  * Contatos para os links do site juntamento com a Lilian
  * Atualização do site Pet (erros)
  * Trabalho de CPD

==== 31/5 à 7/6 ====

  * Assisti palestras do Forum na sexta e no sábado;
  * Ajudei a cortar e ordenar ingressos para a festa;
  * Criei uma página para a festa no site do pet.

==== 7/6 à 14/6 ====

  * Finalização do Layout da página do pedagogia
  * Ajustes no logo do site
  * Criação de todas páginas html do site

==== 14/6 à 21/6 ====

  * Término do site para pedagogia a distância
  * Ajustes no logotipo do site
  * Começo do trabalho final de CPD

===== Irigon =====

==== 17/5 à 24/5 ====

  * Fui a escola pegar as ponteiras RJ-45.
  * Apresentei o relatorio para Eliane.
  * Comecamos a montar os cabos.

==== 24/5 à 31/5 ====

  * Nesta semana, devido ao feriado nao fui a escola.

==== 31/5 à 7/6 ====

  * Fui a escola instalar os cabos de rede e os linux que faltavam;
  * Os linux nao puderam serem instalados pois no CD do slack 10.1 via NFS devido aos kernels do cd nao estarem no mesmo;
  * Discutimos com a vice-diretora sobre como serao feitas as entrevistas para a selecao do curso.

==== 07/6 à 14/6 ====

  * Escola:
    * Instalamos as ultimas maquinas, falta apenas uma que esta sem monitor e que sera instalada assim que o mesmo chegar;
    * Ainda precisamos para o laboratorio ficar completo:
      * Duas placas de rede PCI
      * 5 mouses
    * Os X dos linux ainda nao estao configurados, configurarei esta semana;
    * Sexta-feira, 17/06 devo ir com a Beth analisar e possivelmente arrecadar mais computadores no deposito da ufrgs.
  * Ajudei o Dante a preparar documentos que foram repassados para o Weber para o projeto de intercâmbio Ufrgs-Universidade de Essen;
  * Assinaturas para ingressos para festas, distribuição de flyers e colagem de cartazes;
  * Participação na festa;

==== 14/6 à 21/6 ====

  * Escola:
    * Fomos nos galpões da UFRGS para arrecadar doações de PC com a Beth.
      * Fizemos a requisição de 20 "máquinas" que encontram-se em estado precário, não tivemos oportunidade de testar por elas nao poderem ser desmontadas ou testadas na hora.
    * Falei com a responsável pelo setor pedagógico e discutimos a melhor forma de seleção.
      * Parte dos alunos serã selecionados, outros terão que ser sorteados.
    * Ficou estabelecido a data de início do curso de Linux: 25/06/2005, durante as férias da escola.
      * Ainda serão marcadas os horários das aulas e a carga horária, durante e depois das férias.
    * Fiz a requisição dos mouses para a coordenação que ficou responsável por providenciar os mesmos.
    * Um monitor (doado pelo João) queimou semana passada, este será trocado
    * Um relatório com as atividades referentes a instalação das máquinas via NFS na escola encontra-se na pet-docs/Escola.

===== Paulo =====

==== 17/5 à 24/5 ====

  * Ajustes finais no artigo do FISL 6.0
  * Leitura de mais 2 capítulos do livro de SysOP.
  * Jogo contra a FURG.
  * Pesquisa sobre literatura de SysOPS.
  * Início do desenvolvimento do módulo de captura de MPEG do RoboPET Vision 2.0

==== 24/5 à 31/5 ====

  * Leitura dos capítulos sobre processos, threads e escalonamento de cpu do livro de [[/trac/wiki/SisOp|SisOp?]]
  * Trabalho no sistema de visão do robopet: especificamente, interface de captura ffmpeg (finallizado), e interface com o usuário.

==== 31/5 à 7/6 ====

  * Preparei a apresentação do reconhecimento de imagens para o FISL.
  * Apresentação do mesmo, e participação no Fórum como um todo.

==== 7/6 à 14/6 ====

  * Horas pendentes analisadas
  * Continuação da instalação do GNU/HURD
  * Participação na festa dos PETs

==== 14/6 à 21/6 ====

  * Horas pendentes analisadas
  * Curso de desenvolvimento de equipes
  * Pet SisOP
    * Estudo do emulador de x86 qemu para rodar o HURD
    * GNU/HURD funcionando com REDE no QEMU (em casa)

===== Lilian =====

==== 17/5 à 24/5 ====

  * Ida ao Interpet;
  * Continuação da página para a Rosane Nevado;
  * Continuação dos contatos com a Marta para iniciar um trabalho de pesquisa.

==== 24/5 à 31/5 ====

  * Contatos com a Marta do núcleo de Pensamento Digital para iniciar os trabalhos de ensino e pesquisa.

==== 31/5 à 7/6 ====

  * Contatos com pessoal do Pensamento Digital para iniciar o projeto de pesquisa.

==== 7/6 à 14/6 ====

  * Reunião com a Sabrina e do Décio do núcleo de Pensamento Digital para iniciar o Projeto de Pesquisa na Vila Pinto.

==== 14/6 à 21/06 ====

  * Aguardando retorno da responsável na Vila Pinto para iniciar as atividades.

===== Fabiano =====

==== 17/5 à 24/5 ====

  * Montagem dos robôs (Robopet)
  * Jogo contra FURG (Globaltech)

==== 24/5 à 31/5 ====

  * Leitura sobre cargas de baterias.
  * Estudos para provas de análise de circuitos e equações diferenciais.

==== 31/5 à 7/6 ====

  * Colocação de cartazes.
  * Estudos sobre cargas das baterias de NI/MH (finais).
  * Início da documentação do projeto eletrônico do RoboPET.

==== 7/6 à 14/6 ====

  * Distribuição de //flyers// pelo Campus do Vale na terça-feira;
  * Mini-reunião do hardware do //RoboPET// na terça-feira para definir responsabilidades;
  * Mini-reunião do hardware do //RoboPET// na quinta-feira para definir prazos;
  * Participação trabalhando na festa (bar da rua);
  * Término do esboço do circuito eletrônico do RobôPET;
  * Inserção, no trac do robôpet, das próximas tarefas para o grupo do Hardware (decididas em reunião dias 07 e 09 de junho de 2005);

==== 14/6 à 21/06 ====

  * Projeto e desenho no Auto Cad do acoplador motor/redutor do robô.
  * Início da digitalização do circuito eletrônico do robô.
