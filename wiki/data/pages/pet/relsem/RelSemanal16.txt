====== Mês de 18/07 até 13/08 de 2005 ======

===== Diego =====

==== 20/07 a 26/07 ====

  * Revisão na apostila GTK;
  * (Re)começo do trabalho encima da imagem no [[/trac/wiki/RoboPet|RoboPet?]].

==== 27/07 a 02/08 ====

  * Documentação do código existente da nova imagem do [[/trac/wiki/RoboPet|RoboPet?]];
  * Pesquisa de preços para o orçamento no novo computador.

==== 03/10 a 10/08 ====

  * Finalização dos orçamentos do computador junto ao Decor;
  * Medição dos equipamentos do PET, da sala 113 e da 202 para a posterior mudança;
  * Revisão na apostila GTK.

==== 11/08 a 17/08 ====

  * Transferência da sala do PET para a 202.

===== Fabiano =====

==== 20/07 ao 26/07 ====

  * Início do desenvolvimento do novo chassi para o roboPET (algumas idéias e desenhos em Auto CAD);
  * Pesquisa/procura de um PIC para carregador de baterias (para mim);

==== 27/07 a 02/08 ====

  * Finalização do projeto do novo chassi (mudanças e ajustes finais);
  * Teste nas baterias de chumbo afim de fazer um gráfico de descarga das baterias para poder comparar com as pilhas recarregáveis e também estimar o tempo de autonomia dos robôs;
  * Pesquisa de preço e compra de um carregador de pilhas recarregáveis e 8 pilhas recarregáveis;
  * Teste de descarga das pilhas recarregáveis (vou comparar com as baterias de chumbo);
  * Auxílio ao Robert juntamente com Paulo na mudança da sala do Prof. Dante;
  * Montagem do segundo motor(preto) com o redutor e posteriormente montagem de um robô (aguardando bateria de testes) com ambos motores "rápidos";

==== 03/08 a 10/08 ====

  * Descanso, folga, férias do queiram chamar;

==== 11/08 a 17/08 ====

  * Transferência da sala do PET para a 202

===== Gustavo =====

==== 20/07 ao 26/07 ====

  * Contato com a Professora Rosane assunto: Disponibilizar o site;
  * Troca de Logo no site do Pet;
  * Conversa com o Chico e Maurício sobre o início do desenvolvimento da nova IA.

==== 27/07 a 02/08 ====

  * Desenvolvimento de uma ferramenta para atualizar sites;

==== 03/08 a 10/08 ====

  * Descanso, folga, férias como queiram chamar;

==== 10/08 a 16/08 ====

  * Conversa com Eduardo Sobre a IA;
  * Início do desenvolvimento da IA (estudo da ferramenta do Eduardo);
  * Término do software para Atualizar página WEB

===== Irigon =====

==== 20/7 à 26/7 ====

  * Planejamento da primeira aula para o curso de Linux na escola;
  * Primeira aula do curso de Linux na escola.

===== João =====

==== 20/07 ao 26/07 ====

  * Busca de materiais para começar a apostila de java;
  * Leitura de um capítulo do livro de [[/trac/wiki/SisOp|SisOp?]];
  * Leitura do capítulo "Overview" do tutorial da sun de J2EE.

==== 03/08 ao 10/08 ====

  * Seleção de apostilas para iniciar desenvolvimento da apostila de java.
  * Leitura da metade do livro Metacompetência;

===== Mateus =====

==== 20/7 à 26/7 ====

  * Finalização da apresentação em inglês do Instituto, solicitada pelo Dante;
  * Planejamento da primeira aula para o curso de Linux na escola;
  * Primeira aula do curso de Linux na escola.

==== 27/7 à 2/8 ====

  * Conclusão do curso de Linux na escola (junto com o Irigon e o Rubin - 12 horas).

==== 03/10 a 10/08 ====

  * Curso de Introdução a OO e programação Java - CEI (20 horas);

==== 11/08 a 17/08 ====

  * Início do desenvolvimento do sistema de presenças em Java;
  * Mandei e-mail pra lista sobre a seleção do novo bolsista.

===== Maurício =====

==== 27/7 à 2/8 ====

  * Instalação do Gentoo nos PC's do PET (ainda não concluído)

==== 20/7 à 26/7 ====

  * Apresentação do Instituto no Power Point com Mateus e Robert;
  * Fui com o Gustavo falar com o Chico sobre a IA do ROBOPET;

==== 11/08 a 17/08 ====

  * Instalação do Gentoo na máquina Ellen;
  * Tentativa de arrumar o servidor.

===== Nondillo =====

==== 20/7 à 26/7 ====

  * Finalização do protótipo de controle de velocidade com documentação adequada;
  * Início do projeto do chassi do novo robo.

==== 27/07 a 02/08 ====

  * Ida até a fábrica Componenti para tratar da criação do chassi do novo robô em acrílico, diretamente com o gerente de projeto.
  * Criação de um resumo financeiro do PET.

==== 03/08 a 10/08 ====

  * Folga.
  * Atualizações financeiras, cobrança de devedores.
  * Instalação do Debian num computador para teste.

===== Paulo =====

==== 20/7 à 26/7 ====

  * Participação de todas as atividades do ENAPET

==== 27/7 à 2/8 ====

  * Começei o desenvolimento do seminário
  * Relatório do ENAPET

==== 2/8 - 10/8 ====

  * Atualização do Trac para a versão 0.8.4
  * Resolução do ticket [[/trac/ticket/17|#17]] (hora das máquinas)
  * Preparacao do seminário sobre a rede do pet ([[/trac/ticket/18|#18]])
  * Instalação da Ellen, com geração de pacotes. Trabalho no ticket [[/trac/ticket/15|#15]]

===== Robert =====

==== 20/07 ao 26/07 ====

  * Fotos do II, agora solicitadas pelo Mateus (quinta);
  * Encaixotamento do material da sala do Dante (sexta e segunda);
  * Criação de links no //TRAC//: relatório deste mês e planejamento mensal PROGRAD;
  * Ajuda solicitada pelo Maurício na finalização da apresentação em //Power Point//, juntamente com Dante.

==== 27/07 a 02/08 ====

  * Folga iniciada no dia 27 (quarta-feira), pois as horas da semana e o adiantamento de horas foram realizados.

==== 03/08 a 10/08 ====

  * Retomada das atividades no dia 8 com leitura do //Manual do PET//, solicitado em uma reunião;
  * Apos tentativas frustradas, contatei o Ezequiel para pedir o e-mail dele (solicitado pelo Dante) e ele explicou o que seria necessário para a retirada do nome da Lilian da folha de efetividade;
  * Conversa com Luís Otavio sobre o horário e a forma da troca de sala do da sala do PET.

==== 11/08 a 17/08 ====

  * Transferência da sala do PET para a 202;
  * Início da leitura do livro //Metacompetência//;
  * Entrega da folha de efetividade do mês de agosto à Prograd.

===== Rubin =====

  * Aula na escola !!!
