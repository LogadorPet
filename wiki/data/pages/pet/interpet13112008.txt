====== Ata da Reunião do Interpet de 13/11/2008 ======

  * Presentes: Matheus Proença (Computação), Anderson Peck (Geografia), Lucas da Cruz (Educação Física), Nádia de Castro (Letras), Luana Limas (Letras), Paulo Crochemore (Sociais), Lucas Zawacki (Computação), Fábio Beltrão (Computação), Kauê Silveira (Computação), Tomaz Pereira (Geografia), Ruggiero Stello (Odontologia), Dáfini Knak (Odontologia), Bruna Genari (Odontologia), Laura Boeira (Psicologia), Débora Finkler (Psicologia), Marcos Cavinato (Computação)

  * Relator: Kauê Silveira

===== CLA =====

  * condição atual:
    * 3 tutores, 3 alunos e 3 membros da PROGRAD;
    * não está ativo, deve ser reativado;
    * Paulo enviará o "regimento atual" do CLA para esta lista.

===== Interpetão sobre o CLA e a Mobilização =====

  * dia 6 de dezembro;
  * Lucas verá se pode ser na Educação Física;
  * CLA:
    * novo regimento do CLA;
      * auxílio aos grupos novos;
      * interface com a PROGRAD;
      * controle das IES sobre as verbas;
        * flexibilidade das verbas;
        * atraso das verbas no verão;
    * nova composição do CLA;
      * um membro eleito por cada grupo;
      * um membro da PROGRAD.
    * membros atuais devem se fazer presentes (ser convocados por seus grupos);
  * Mobilização:
    * integração com a Iniciação Científica ou não?
    * integração com PETs de outros estados;
    * elaboração de um documento;
    * ato simbólico com entrega do documento a autoridades competentes;
    * utilizar o DCE como meio de comunicação.

===== ENAPET =====

  * Está disponível no site a ata de 2008;
  * Lucas informar-nos-á sobre a interação com o projeto Rondon;
    * PET-PSICO tem 7 membros interessados;
    * temos que elaborar projetos coletivos (interdisciplinares);
  * Sugestões:
    * Assembléias:
      * Um voto por grupo, talvez através de placas;
      * Alguns temas não necessitam de votação, podem ser apenas discutidos;
      * Discutir mais os pontos que necessitam ser votados;
      * Elaborar um documento com as decisões e disponibilizar para todos os PETs para que haja visibilidade;
    * distribuir melhor os horários;
    * alojamento mais próximo do evento;
    * modificar (ou excluir) os simpósios;
      * não houve público;
    * desenvolver mais e melhor o tema do evento;
    * os GTs funcionam bem, mas desviam do tema;
    * Encontro por Áreas:
      * promover mais a efetiva integração;
      * haver um desafio prático para cada área, formando-se grupos para o mesmo
        * um desafio possível é construir ou propor um projeto;

===== Mobilização =====

  * PET-ESEF falou com o PET-ESEF da UFSC;
  * Dante levou o panfleto para o ministro da educação;
  * cada grupo deve discutir a sua posição;

===== CENAPET =====

  * as decisões devem ser melhor divulgadas;
