O Curso de PHP será realizado na Semana Acadêmica e terá duração de 15 horas. (5 aulas de 3h cada durante a tarde)

==== Datas de Realização ====

  * Dias 22, 23, 24, 25 e 26 de Outubro de 2007

==== Horário ====

  * A definir!!! (3 horas por dia)

==== Cronograma ====

  * Dia 22 - Aula 1 - Revisão básica de HTML, Conceitos básicos de PHP / Tipos de Dados e Constantes (Antonio / Flores)
  * Dia 23 - Aula 2 - Operadores / Estruturas de Controle (Flores / Ismael)
  * Dia 24 - Aula 3 - Funções, Variáveis e Includes (Ismael)
  * Dia 25 - Aula 4 - Formulários / Manipulação de Arquivos Remotos (início de SQL?) (Antonio / Flores)
  * Dia 26 - Aula 5 - SQL, Bancos de Dados com PHP / Cookies (Ismael / Antonio)

==== Ministrantes ====

  * Antônio Augusto da Fontoura
  * Felipe Flores
  * Ismael Stangherlini
