====== [[/trac/wiki/InterMapTxt|InterMapTxt]] ======

===== This is the place for defining [[/trac/wiki/InterWiki|InterWiki]] prefixes =====

This page was modelled after the [[http://www.usemod.com/cgi-bin/mb.pl?InterMapTxt|MeatBall:InterMapTxt]] page. In addition, an optional comment is allowed after the mapping.

This page is interpreted in a special way by Trac, in order to support InterWiki links in a flexible and dynamic way.

The code block after the first line separator in this page will be interpreted as a list of InterWiki specifications:

  prefix &lt;space&gt; URL [&lt;space&gt; # comment]

By using "$1", "$2", etc. within the URL, it is possible to create [[/trac/wiki/InterWiki|InterWiki]] links which support multiple arguments, e.g. Trac:ticket:40. The URL itself can be optionally followed by a comment, which will subsequently be used for decorating the links using that prefix.

New InterWiki links can be created by adding to that list, in real time. Note however that //deletions// are also taken into account immediately, so it may be better to use comments for disabling prefixes.

Also note that InterWiki prefixes are case insensitive.

===== List of Active Prefixes =====

^ //Prefix// ^ //Site// ^
| [[http://www.acronymfinder.com/af-query.asp?String=exact&Acronym=RecentChanges|Acronym]] | [[http://www.acronymfinder.com/af-query.asp?String=exact&Acronym=|http://www.acronymfinder.com/af-query.asp?String=exact&amp;Acronym=]] |
| [[http://c2.com/cgi/wiki?FindPage&value=RecentChanges|C2find]] | [[http://c2.com/cgi/wiki?FindPage&value=|http://c2.com/cgi/wiki?FindPage&amp;value=]] |
| [[http://c2.com/cgi/wiki?RecentChanges|c2Wiki]] | http://c2.com/cgi/wiki? |
| [[http://www.google.com/search?q=cache:RecentChanges|Cache]] | http://www.google.com/search?q=cache: |
| [[http://cheeseshop.python.org/pypi/RecentChanges|CheeseShop]] | [[http://cheeseshop.python.org/pypi/|Python Package $1 from the Cheese Shop]] |
| [[http://search.cpan.org/perldoc?RecentChanges|CPAN]] | http://search.cpan.org/perldoc? |
| [[http://bugs.debian.org/RecentChanges|DebianBug]] | http://bugs.debian.org/ |
| [[http://packages.debian.org/RecentChanges|DebianPackage]] | http://packages.debian.org/ |
| [[http://www.dict.org/bin/Dict?Database=*&Form=Dict1&Strategy=*&Query=RecentChanges|Dictionary]] | [[http://www.dict.org/bin/Dict?Database=*&Form=Dict1&Strategy=*&Query=|http://www.dict.org/bin/Dict?Database=*&amp;Form=Dict1&amp;Strategy=*&amp;Query=]] |
| [[http://www.google.com/search?q=RecentChanges|Google]] | http://www.google.com/search?q= |
| [[http://groups.google.com/group/RecentChanges/msg/|GoogleGroups]] | [[http://groups.google.com/group/$1/msg/$2|Message $2 in $1 Google Group]] |
| [[http://en.wikipedia.org/wiki/ISO_RecentChanges|ISO]] | [[http://en.wikipedia.org/wiki/ISO_|ISO Standard $1 in Wikipedia]] |
| [[http://downlode.org/perl/jargon-redirect.cgi?term=RecentChanges|JargonFile]] | http://downlode.org/perl/jargon-redirect.cgi?term= |
| [[http://www.usemod.com/cgi-bin/mb.pl?RecentChanges|MeatBall]] | http://www.usemod.com/cgi-bin/mb.pl? |
| [[http://www.selenic.com/mercurial/wiki/index.cgi/RecentChanges|Mercurial]] | [[http://www.selenic.com/mercurial/wiki/index.cgi/|the wiki for the Mercurial distributed SCM]] |
| [[http://sunir.org/apps/meta.pl?RecentChanges|MetaWiki]] | http://sunir.org/apps/meta.pl? |
| [[http://meta.wikipedia.org/wiki/RecentChanges|MetaWikiPedia]] | http://meta.wikipedia.org/wiki/ |
| [[http://issues.apache.org/jira/browse/MODPYTHON-RecentChanges|MODPYTHON]] | [[http://issues.apache.org/jira/browse/MODPYTHON-|Issue $1 in mod_python's JIRA instance]] |
| [[http://moinmoin.wikiwikiweb.de/RecentChanges|MoinMoin]] | http://moinmoin.wikiwikiweb.de/ |
| [[http://bugs.mysql.com/bug.php?id=RecentChanges|mysql-bugs]] | [[http://bugs.mysql.com/bug.php?id=|Bug #$1 in MySQL's bug database]] |
| [[http://peak.telecommunity.com/DevCenter/RecentChanges|peak]] | [[http://peak.telecommunity.com/DevCenter/|$1 in Python Enterprise Application Kit's Wiki]] |
| [[http://www.python.org/peps/pep-RecentChanges.html|PEP]] | [[http://www.python.org/peps/pep-$1.html|Python Enhancement Proposal]] |
| [[http://bugs.python.org/issueRecentChanges|PythonBug]] | [[http://bugs.python.org/issue$1|Python Issue #$1]] |
| [[http://tools.ietf.org/html/rfcRecentChanges|RFC]] | [[http://tools.ietf.org/html/rfc$1|IETF's RFC $1]] |
| [[http://www.sqlite.org/cvstrac/wiki?p=RecentChanges|SQLite]] | http://www.sqlite.org/cvstrac/wiki?p= |
| [[http://www.orcaware.com/svn/wiki/RecentChanges|SvnWiki]] | [[http://www.orcaware.com/svn/wiki/|Subversion Wiki]] |
| [[http://thread.gmane.org/gmane.comp.version-control.subversion.trac.devel/RecentChanges|trac-dev]] | [[http://thread.gmane.org/gmane.comp.version-control.subversion.trac.devel/|Message $1 in Trac Development Mailing List]] |
| [[http://thread.gmane.org/gmane.comp.version-control.subversion.trac.general/RecentChanges|Trac-ML]] | [[http://thread.gmane.org/gmane.comp.version-control.subversion.trac.general/|Message $1 in Trac Mailing List]] |
| [[http://www.whois.sc/RecentChanges|WhoIs]] | http://www.whois.sc/ |
| [[http://clublet.com/c/c/why?RecentChanges|Why]] | http://clublet.com/c/c/why? |
| [[http://en.wikipedia.org/wiki/RecentChanges|WikiPedia]] | http://en.wikipedia.org/wiki/ |

----

===== Prefix Definitions =====

  PEP     http://www.python.org/peps/pep-$1.html                                       # Python Enhancement Proposal
  PythonBug http://bugs.python.org/issue$1                                             # Python Issue #$1
  Trac-ML  http://thread.gmane.org/gmane.comp.version-control.subversion.trac.general/ # Message $1 in Trac Mailing List
  trac-dev http://thread.gmane.org/gmane.comp.version-control.subversion.trac.devel/   # Message $1 in Trac Development Mailing List

  Mercurial http://www.selenic.com/mercurial/wiki/index.cgi/ # the wiki for the Mercurial distributed SCM
  RFC       http://tools.ietf.org/html/rfc$1 # IETF's RFC $1
  ISO       http://en.wikipedia.org/wiki/ISO_ # ISO Standard $1 in Wikipedia

  CheeseShop  http://cheeseshop.python.org/pypi/  # Python Package $1 from the Cheese Shop
  SQLite      http://www.sqlite.org/cvstrac/wiki?p=
  mysql-bugs  http://bugs.mysql.com/bug.php?id=  # Bug #$1 in MySQL's bug database
  peak        http://peak.telecommunity.com/DevCenter/ # $1 in Python Enterprise Application Kit's Wiki
  MODPYTHON   http://issues.apache.org/jira/browse/MODPYTHON- # Issue $1 in mod_python's JIRA instance
  SvnWiki     http://www.orcaware.com/svn/wiki/ # Subversion Wiki

  #
  # A arbitrary pick of InterWiki prefixes...
  #
  Acronym          http://www.acronymfinder.com/af-query.asp?String=exact&amp;Acronym=
  C2find           http://c2.com/cgi/wiki?FindPage&amp;value=
  Cache            http://www.google.com/search?q=cache:
  CPAN             http://search.cpan.org/perldoc?
  DebianBug        http://bugs.debian.org/
  DebianPackage    http://packages.debian.org/
  Dictionary       http://www.dict.org/bin/Dict?Database=*&amp;Form=Dict1&amp;Strategy=*&amp;Query=
  Google           http://www.google.com/search?q=
  GoogleGroups     http://groups.google.com/group/$1/msg/$2        # Message $2 in $1 Google Group
  JargonFile       http://downlode.org/perl/jargon-redirect.cgi?term=
  MeatBall         http://www.usemod.com/cgi-bin/mb.pl?
  MetaWiki         http://sunir.org/apps/meta.pl?
  MetaWikiPedia    http://meta.wikipedia.org/wiki/
  MoinMoin         http://moinmoin.wikiwikiweb.de/
  WhoIs            http://www.whois.sc/
  Why              http://clublet.com/c/c/why?
  c2Wiki             http://c2.com/cgi/wiki?
  WikiPedia        http://en.wikipedia.org/wiki/
