  * Ver tabela "Museu.xls" em "pet-docs/museu/Bruno" para lista completa.

  * Apple IIc - CAp0001- [[http://www.old-computers.com/museum/computer.asp?st=1&c=69|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=69]] e http://applemuseum.bott.org/sections/computers/IIc.html

http://cobit.mma.com.br/materias/gravadores.htm

  * Apple Macintosh SE - CAp0002- [[http://www.old-computers.com/museum/computer.asp?c=161&st=1|http://www.old-computers.com/museum/computer.asp?c=161&amp;st=1]]

  * Apple Macintosh 630cd - CAp0006 - http://docs.info.apple.com/article.html?artnum=112269

  * Apple Macintosh LC II -CAp0008 - http://docs.info.apple.com/article.html?artnum=112185 e [[http://www.apple-history.com/?page=gallery&model=lcII&performa=off&sort=date&order=ASC|http://www.apple-history.com/?page=gallery&amp;model=lcII&amp;performa=off&amp;sort=date&amp;order=ASC]]

  * CP 400 - CPr0005 - http://www.mci.org.br/micro/prologica/cp400.html

  * CP 500 - CPr0007 - http://www.mci.org.br/micro/prologica/cp500.html

  * Gradiente Expert Plus - CGr0003- [[http://www.old-computers.com/museum/computer.asp?c=807&st=1|http://www.old-computers.com/museum/computer.asp?c=807&amp;st=1]]

  * Microdigital TK3000 -CMi0004 - [[http://www.old-computers.com/museum/computer.asp?st=1&c=947|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=947]]

  * Toshiba T1200 - LTo0001 - [[http://www.old-computers.com/museum/computer.asp?st=1&c=917|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=917]]

  * Mac II/Vx - CAp0009 - http://1000bit.net/scheda.asp?id=534 ; http://www.computissimo.ch/v-fran/apple.htm ; http://www.everymac.com/systems/apple/mac_ii/stats/mac_iivx.html

  * Mac IIci - CAp0010 - http://www.everymac.com/systems/apple/mac_ii/stats/mac_iici.html

  * Mac Classic - CAp0011 - http://www.everymac.com/systems/apple/mac_classic/stats/mac_classic.html

  * Mac Classic II - CAp0012 - http://www.everymac.com/systems/apple/mac_classic/stats/mac_classic_ii.html

  * Mac SE/30 - CAp0013 - http://www.everymac.com/systems/apple/mac_classic/stats/mac_se30.html

  * HP 85 - CHp0014 - http://www.mci.org.br/micro/outros/hp85.html; [[http://www.old-computers.com/museum/computer.asp?st=1&c=353|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=353]]

  * CP 200S - CPr0015 - http://www.mci.org.br/micro/prologica/cp200.html

  * TK 85 - CMi0016 - [[http://www.old-computers.com/museum/computer.asp?st=1&c=976|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=976]]

  * TK 90 - CMi0017 - [[http://www.old-computers.com/museum/computer.asp?st=1&c=945|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=945]]

  * Apple II Plus - LAp0004 - [[http://www.old-computers.com/museum/computer.asp?st=1&c=571|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=571]]

  * Hotbit HB8000 - CEp0020 - [[http://www.old-computers.com/museum/computer.asp?st=1&c=833|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=833]]

  * Codimex 6809 CCx0019 - http://cobit.mma.com.br/manchetes/c6809.htm; [[http://www.old-computers.com/museum/computer.asp?st=1&c=534|http://www.old-computers.com/museum/computer.asp?st=1&amp;c=534]]

  * Apple Senior - CAp0021 - [[http://www.apple2.com.br/modules.php?name=Sections&op=viewarticle&artid=4|http://www.apple2.com.br/modules.php?name=Sections&amp;op=viewarticle&amp;artid=4]] e http://cobit.mma.com.br/manchetes/applesenior.htm

  * Craft II Plus - CAp0022 - [[http://www.apple2.com.br/modules.php?name=Sections&op=viewarticle&artid=8|http://www.apple2.com.br/modules.php?name=Sections&amp;op=viewarticle&amp;artid=8]] e http://cobit.mma.com.br/micros/craftIIplus.htm

  * PC 7000 - CSh0023 - http://www.old-computers.com/museum/computer.asp?c=496

  * Polymax Maxxi - CAp0024 - http://cobit.mma.com.br/micros/maxxi.htm e [[http://www.classicgaming.com.br/cgi-bin/canal3/foto.asp?arqfoto=/Images/Canal3/Propagandas/Maxxi_1983.jpg&nomefoto=Polymax%20Maxxi|http://www.classicgaming.com.br/cgi-bin/canal3/foto.asp?arqfoto=/Images/Canal3/Propagandas/Maxxi_1983.jpg&amp;nomefoto=Polymax%20Maxxi]]

  * Microengenho I - CAp0025 - http://cobit.mma.com.br/micros/microeng.htm e http://www.inf.ufrgs.br/~cabral/museu.html

  * Sistema 600 - CPr0026 - http://cobit.mma.com.br/micros/sistema600.htm

  * LT 1600 D - LMi0002 - http://www.tk90x.com.br/Microdigital.html e http://www.tk90x.com.br/LT1600D.html

  * TK 82C - CMi0027 - http://cobit.mma.com.br/micros/tk82.htm

  * NE Z8000 - CNe0028 - http://cobit.mma.com.br/micros/nez8000.htm

  * TRS 80 Model III - CTa0029 - http://www.old-computers.com/museum/computer.asp?c=18 e http://www.trs-80.com/

  * Macintosh Quadra 840av - CAp0030 - http://www.everymac.com/systems/apple/mac_quadra/stats/mac_quadra_840av.html e [[http://www.apple-history.com/?page=gallery&model=840|http://www.apple-history.com/?page=gallery&amp;model=840]]

  * Novus 650 - [[/trac/wiki/CalPh|CalPh?]]0001 - http://www.vintage-technology.info/pages/calculators/n/novus650.htm ; http://www.vintagecalculators.com/html/novus_650__mathbox_.html ; http://museu.boselli.com.br/Philco%20Novus%20650.htm

  * EL-405 - [[/trac/wiki/CalSh|CalSh?]]0002 - Manual de Instruções da Calculadora

  * EL-8131 - [[/trac/wiki/CalSh|CalSh?]]0003 - http://www.cs.ubc.ca/~hilpert/eec/calcs/SharpEL8131.html ; http://www.vintage-technology.info/pages/calculators/sharp/sharp8131.htm e Manual de Instruções

  * SC107A - [[/trac/wiki/CalTr|CalTr?]]0004 - http://www.truly.net/html/prod/proddetail.php?id=82 e http://museu.boselli.com.br/Truly%20SC%20107%20A.htm

  * EL-5806s - [[/trac/wiki/CalSh|CalSh?]]0005 - http://www.dentaku-museum.com/2-ref/data/sharp/sharp-list.html (ir em P23) ; http://www.devidts.com/be-calc/catalog_S.asc.htm

  * EL-506P - [[/trac/wiki/CalSh|CalSh?]]0006 - http://www.devidts.com/be-calc/poc_11984.html e [[http://mycalcdb.free.fr/main.php?l=0&id=1615|http://mycalcdb.free.fr/main.php?l=0&amp;id=1615]]

  * HP 41C - [[/trac/wiki/CalHp|CalHp?]]0007 - http://www.hpcc.org/calculators/hp41.html e [[http://mycalcdb.free.fr/main.php?l=0&id=802|http://mycalcdb.free.fr/main.php?l=0&amp;id=802]]

  * HP 25 - [[/trac/wiki/CalHp|CalHp?]]0008 - [[http://mycalcdb.free.fr/main.php?l=0&id=779|http://mycalcdb.free.fr/main.php?l=0&amp;id=779]]

  * HP 33C - [[/trac/wiki/CalHp|CalHp?]]0009 - [[http://mycalcdb.free.fr/main.php?l=0&id=798|http://mycalcdb.free.fr/main.php?l=0&amp;id=798]] ; http://museu.boselli.com.br/HP%2033%20C.htm ; http://www.phys.uwosh.edu/mike/calcs/hp33c.html

  * HP 21 - [[/trac/wiki/CalHp|CalHp?]]0010 - [[http://mycalcdb.free.fr/main.php?l=0&id=778|http://mycalcdb.free.fr/main.php?l=0&amp;id=778]] e http://www.hpmuseum.org/hp21.htm

  * [[/trac/wiki/CoProcessador|CoProcessador?]] Intel 80387 - PIn0001 - http://en.wikipedia.org/wiki/Intel_80387

  * Commodore MM6 - [[/trac/wiki/CalCo|CalCo?]]0011 - [[http://mycalcdb.free.fr/main.php?l=0&id=537|http://mycalcdb.free.fr/main.php?l=0&amp;id=537]] e http://www.vintage-technology.info/pages/calculators/commodore/comin6.htm

  * Impressora Canon BJ-30 - ICa0001 - http://downloadlibrary.usa.canon.com/cpr/pdf/Manuals/BJ30_user_manual.pdf

Obs: as conversões de medidas foram baseadas na tabela de conversões do site: http://www.tediabrazil.com.br/tab_conversao/tabcon_unidades.html
