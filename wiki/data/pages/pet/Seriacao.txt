===== Catalogando uma nova peça =====

  * PRIMEIRO PASSO: inserir os dados da peça no catálogo oficial do museu, no PET-docs (pasta "Catálogos" dentro da pasta "Museu"):
    * Os dados a serem inseridos são: fabricante, modelo, categoria, referência, localização, quantidade e situação;
    * "Referência" é um tipo de código de cada item, composto da seguinte forma:
      * As três primeiras letras correspondem à sigla da categoria da peça;
      * As três próximas correspondem à sigla do fabricante;
      * Se o fabricante ainda não estiver catalogado, defina uma sigla e não esqueça de atualizar em "Siglas Definidas";
      * Os números (sempre de quatro dígitos) é um índice dos itens de mesmo categoria e marca;
      * Vide "Siglas Definidas" para ver as siglas dos fabricantes e dos tipos;

  * "Localização" indica onde a peça está:
    * A = Acervo (sala 102 da Biblioteca do II);
    * M = Museu (sala XXX da Biblioteca do II);
    * E = Exposto (peça exposta pelo projeto Museu Distribuído);
  * No item "Quantidade", caso haja peças em locais diferentes, indique a quantidade em cada local:
    * Exemplo: 1A/1M (uma peça no Acervo e uma no Museu);
  * "Situação" indica se a peça funciona ou não:
    * F = Funciona;
    * NF = Não Funciona;
    * NT = Não foi testado;

  * SEGUNDO PASSO: atualizar o catálogo no TRAC

  * TERCEIRO PASSO: escrever os dois textos da peça e colocar no PET-docs (pasta "Museu Distribuído")
    * Texto completo: de três a quatro parágrafos, contendo as principais informações sobre a peça, como, por exemplo:
      * Fabricante e modelo
      * Data de desenvolvimento/lançamento
      * Público alvo
      * Quais as expectativas da empresa
    * Texto resumido: contedo as principais informações em forma de itens e uma breve história em um ou dois parágrafos

===== Siglas Definidas =====

  * Categorias
    * Cmp = Computador Pessoal
    * Mon = Monitor
    * Tec = Teclado
    * Sca = Scanner
    * Not = Notebook
    * Imp = Impressora
    * Vdg = Video Game
    * Pec = Peça
    * Out = Outros

  * Fabricantes
    * Apple = App
    * Barret = Brr
    * BCD = Bcd
    * Bondwell = Bdw
    * Canon = Can
    * Cassio =
    * CCE = Cce
    * Chipcom = Chc
    * Citizen = Ctz
    * Cobra = Cbr
    * Codimex = Cmx
    * Commodore = Cmm
    * Cyrix = Cyr
    * DeTeWe Hamann = Dtw
    * Digitel = Dgt
    * Dismac = Dsm
    * Edisa = Edi
    * Elgin = Elg
    * Epcom = Epc
    * Epson = Epson
    * Ericsson = Erc
    * Facit = Fct
    * General = Gnr
    * Genius = Gns
    * Globus Digital = Gld
    * Gradiente = Grd
    * Highwave = Hiw
    * HotBit = Htb
    * Hewlett-Packard = Hpq
    * IBM = Ibm
    * Intel = Int
    * InterGraph = Igr
    * ISA Indústria de Impressoras = Isa
    * Itautec = Itt
    * Magitronis Technology = Mgt
    * Metroplan = Mtp
    * Microdigital = Mdg
    * Microtec = Mct
    * Motorola = Mot
    * Mtek = Mtk
    * Nokia = Nok
    * Nova Eletrônica = Nel
    * Novus = Nvs
    * Olivetti = Olv
    * Philco = Phc
    * Philips = Php
    * Polymax = Plx
    * Prológica = Prl
    * QUBIE' = Qub
    * Rima = Rma
    * Samsung = Ssg
    * Scopus = Scp
    * SEB Loisirs = Seb
    * Sharp = Shp
    * SiD = Sid
    * Silvex = Svx
    * SMS = Sms
    * Sony = Sny
    * Spectrum Equipamentos Eletrõnicos = See
    * Tandy Radio Shack = Trs
    * TeknoLógica = Tlg
    * Tektronix = Ttx
    * Toshiba = Tsh
    * Truly = Trl
    * UFRGS - Instituto de Informática = Inf
    * US Robotics = Usr
    * Varian Data Machine = Vdm
    * Videocompo = Vcp
    * Visiogrf = Vsg
    * Zenith Data Systems = Zds
