<?php
/* Insere Horas (insere_horas.php)
1. Insere as horas informadas na semana respectiva do usu�rio no banco de dados
2. Os dados referentes a quantidade de horas a inserir s�o provenientes da p�gina 'inserir_horas.php' 
*/
	  header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
    header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1
	
	require_once("Globals.php");
	//verifica se o administrador est� logado
	checkCookie(REDIRECT_ONLY);

	// realiza conex�o com o banco de dados
	conecta();
	
	$data_f = $_POST['data_f'];
	
	$dateBegin = array();
	$dateBegin['day'] = $_POST['dia'];
	$dateBegin['month'] = $_POST['mes'];
	$dateBegin['year'] = $_POST['ano'];
	$dateBegin['hour'] = $_POST['hora_i'];
	$dateBegin['minute'] = $_POST['minuto_i'];
	
	$dateFinish = array();
	list($dateFinish['day'], $monthName, $dateFinish['year']) = explode(' ', $data_f);
	$monthArrayIndex = array_search($monthName, $meses);
	$monthIndex = $monthArrayIndex + 1;
	
	$dateFinish['month'] = $monthIndex;
	if($dateFinish['month'] < 10)
		$dateFinish['month'] = '0'.$dateFinish['month'];
	if($dateFinish['day'] < 10)
		$dateFinish['day'] = '0'.$dateFinish['day'];
	$dateFinish['hour'] = $_POST['hora_f'];
	$dateFinish['minute'] = $_POST['minuto_f'];
	
	// determina os tempos em segundos das datas de in�cio e fim da inser��o
	$strStart = $dateBegin['year'] . "-" . $dateBegin['month'] . "-" . $dateBegin['day'] . " " . $dateBegin['hour'] . ":" . $dateBegin['minute'] . ":00";
	$strEnd = $dateFinish['year'] . "-" . $dateFinish['month'] . "-" . $dateFinish['day'] . " " . $dateFinish['hour'] . ":" . $dateFinish['minute'] . ":00"; 
  
	$rs = mysql_query("SELECT UNIX_TIMESTAMP('". $strStart ."') AS tempo_i, UNIX_TIMESTAMP('". $strEnd ."') AS tempo_f, UNIX_TIMESTAMP() AS now");
	$tempo_i = mysql_result($rs, 0, 'tempo_i');
	$tempo_f = mysql_result($rs, 0, 'tempo_f');
	$now = mysql_result($rs, 0, 'now');

	// caso o tempo inicial da inser��o seja maior que o tempo atual, a inser��o � para o futuro (por isso, inv�lida)
	if ( ($tempo_i > $now) OR ($tempo_f > $now) )
	{
		echo("O per�odo solicitado ainda n�o aconteceu!<br>");
	    echo "<a href='javascript: history.back(-1)'>Voltar</a></body></html>";
		die();
	} 
	// se o tempo inicial � maior ou igual ao tempo final, a inser��o � inv�lida
	else
	if ($tempo_i >= $tempo_f)
	{
		if($tempo_i == $tempo_f)
			echo("A data inicial deve ser diferente da data final.<br>");
		else
			echo("A data inicial deve ser anterior � data final.<br>");
	    echo "<a href='javascript: history.back(-1)'>Voltar</a></body></html>";
		die();
	} 
	else // caso contr�rio, inser��o V�LIDA! Insere no banco de dados
	{
		$sql = "SELECT 1 FROM config
				WHERE 
					(
						( (DATE(FROM_UNIXTIME('". $tempo_i ."')) >= mes_inicio) AND (DATE(FROM_UNIXTIME('". $tempo_f ."')) >= mes_inicio) ) 
						OR
						( (DATE(FROM_UNIXTIME('". $tempo_i ."')) < mes_inicio) AND (DATE(FROM_UNIXTIME('". $tempo_f ."')) < mes_inicio) ) 
					);";
		$rs = mysql_query($sql);
		if(mysql_num_rows($rs) > 0) //inicio e fim estao no mesmo mes
		{
			$sql = "INSERT INTO historico (id_user, inicio, fim) VALUES ('". $_POST['user_id'] ."', FROM_UNIXTIME('". $tempo_i ."'), FROM_UNIXTIME('". $tempo_f ."'))";
			mysql_query($sql) or die(mysql_error());
		}
				else
				{
					$sql = "INSERT INTO historico (id_user, inicio, fim) VALUES ('". $_POST['user_id'] ."', FROM_UNIXTIME('". $tempo_i ."'), (SELECT (mes_inicio - INTERVAL 1 SECOND) FROM config))";
					mysql_query($sql);
					$sql = "INSERT INTO historico (id_user, inicio, fim) VALUES ('". $_POST['user_id'] ."', (SELECT mes_inicio FROM config), FROM_UNIXTIME('". $tempo_f ."'))";
					mysql_query($sql);
				}
				
				echo("Inser��o realizada com sucesso!<br>");
			echo "<a href='admin.php'><< Administrador</a><br><a href='index.php'><< Logador </a><br><br></body></html>";
	}
?>