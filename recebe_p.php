<?php
/* Recebe Pendentes (recebe_p.php)
1. Recebe os dados enviados das pendentes de 'submeter_p.php'
2. Verifica a validade da pendente fornecida
3. Insere pendente no banco de dados
*/

// inclui as fun��es necess�rias para o trabalho com o logador
include("calctemp.php");

// realiza a conex�o com o banco de dados
require_once("Globals.php");
conecta();

$userId = $_POST['user_id'];
$local = $_POST['local'];
$reason = $_POST['justificativa'];

// caso as informa��es relativas a justificativa e ao local existam
if (!(empty($reason) OR empty($local) OR empty($userId)))
{
	$data_f = $_POST['data_f'];
	
	$dateBegin = array();
	$dateBegin['day'] = $_POST['dia'];
	$dateBegin['month'] = $_POST['mes'];
	$dateBegin['year'] = $_POST['ano'];
	$dateBegin['hour'] = $_POST['hora_i'];
	$dateBegin['minute'] = $_POST['minuto_i'];
	
	$dateFinish = array();
	list($dateFinish['day'], $monthName, $dateFinish['year']) = explode(' ', $data_f);
	$monthArrayIndex = array_search($monthName, $meses);
	$monthIndex = $monthArrayIndex + 1;
	
	$dateFinish['month'] = $monthIndex;
	if($dateFinish['month'] < 10)
		$dateFinish['month'] = '0'.$dateFinish['month'];
	if($dateFinish['day'] < 10)
		$dateFinish['day'] = '0'.$dateFinish['day'];
	$dateFinish['hour'] = $_POST['hora_f'];
	$dateFinish['minute'] = $_POST['minuto_f'];
	
	// determina os tempos em segundos das datas de in�cio e fim da pendente
	$strStart = $dateBegin['year'] . "-" . $dateBegin['month'] . "-" . $dateBegin['day'] . " " . $dateBegin['hour'] . ":" . $dateBegin['minute'] . ":00";
	$strEnd = $dateFinish['year'] . "-" . $dateFinish['month'] . "-" . $dateFinish['day'] . " " . $dateFinish['hour'] . ":" . $dateFinish['minute'] . ":00"; 
  
	$tempo_i = strtotime($strStart);//mktime($dateBegin['hour'],$dateBegin['minute'],00,$dateBegin['month'],$dateBegin['day'],$dateBegin['year']);
	$tempo_f = strtotime($strEnd);//mktime($dateFinish['hour'],$dateFinish['minute'],00,$dateFinish['month'],$dateFinish['day'],$dateFinish['year']);
	
	$rs = mysql_query("SELECT UNIX_TIMESTAMP() AS now");
	$now = mysql_result($rs, 'now');

	// caso o tempo inicial da pendente seja maior que o tempo atual, a pendente � para o futuro (por isso, inv�lida)
	if ( ($tempo_i > $now) OR ($tempo_f > $now) )
	{
		echo("A pendente solicitada ainda n�o aconteceu!<br>");
	    echo "<a href='javascript: history.back(-1)'>Voltar</a></body></html>";
		die();
	} 
	// se o tempo inicial � maior ou igual ao tempo final, a pendente � inv�lida
	else
	if ($tempo_i >= $tempo_f)
	{
		if($tempo_i == $tempo_f)
			echo("A data inicial deve ser diferente da data final.<br>");
		else
			echo("A data inicial deve ser anterior � data final.<br>");
	    echo "<a href='javascript: history.back(-1)'>Voltar</a></body></html>";
		die();
	} 
	else // caso contr�rio, pendente V�LIDA! Insere pendente no banco de dados
	{
		$sql = "INSERT INTO pendentes (id_user,inicio,fim,local,justificativa) VALUES ('". $userId ."','". $strStart ."','". $strEnd ."','". $local ."','". $reason ."')";
		mysql_query($sql) or die("Nao enviou... :(");
		echo("Pendente solicitada com sucesso!<br>");
	    echo "<a href='index.php'><< Logador</a><br></body></html>";
  }
}
else // caso as informa��es de justificativa e/ou local n�o existam
{
	echo "<h1><center>Dados incompletos!</h1></center>";
	echo "<center><a href='javascript: history.back(-1)'>Voltar</a></center></body></html>";
}
?>

