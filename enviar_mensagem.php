<?php
/* Envio de Mensagens ao Usu�rio (submeter_p.php)
1. Recebe o texto que formar� a mensagem a ser encaminhada ao Administrador
2. Possui link para redirecionar os dados para a p�gina 'envia_mensagem.php' 
*/
  header ("Pragma: no-cache");						            // sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
  header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1
 
 require_once("Globals.php");
 // realiza conex�o com o banco de dados
 conecta();

 if( isset($_GET['id_to']) && ($_GET['id_to']) )
 	$id_to = $_GET['id_to'];
?>


<html>
<head>
<title>Enviar Mensagem - Logador PET</title>
<link rel="stylesheet" type="text/css" href="estilo.css">
<script language="JavaScript" type="text/javascript">
/*
Fun��o para testar se todos os campos foram preenchidos
*/

function checkrequired(which)
{
  var pass=true
  if (document.images)
	{
    for (i=0;i<which.length;i++)
		{
      var tempobj=which.elements[i];
       if (((tempobj.type=="texto"||tempobj.type=="textarea"||tempobj.type=="select-one")&&tempobj.value==''))
			{
        pass=false
        break
      }
			
    }
  }

  if (!pass)
  {
    alert("Voc� n�o preencheu todos os campos!")
    return false
  }

}

function limita_tamanho(which,limite)
{
  var tamanho;
	var count;
	tamanho = which.value.length;
	if (tamanho > limite)
	{
	  which.value = which.value.substring(0,limite);
	}
	document.getElementByID("contador").innerHTML = "Member";
}

function updateCount(textareaId, spanId, limite) 
{
  var tamanho;
  textarea = document.getElementById(textareaId);
	tamanho = textarea.value.length;
	if (tamanho > limite)
	{
	  textarea.value = textarea.value.substring(0,limite);
	}
  if (textarea == null) 
	{
    return;
  }
  if (textarea.value.length > limite) 
	{
    textarea.value = textarea.value.substring(0, limite);
  }
  document.getElementById(spanId).innerHTML = limite - textarea.value.length;
}
</script>
</head>
<body>

<?php
if(!isset($id_to))
	echo "<center><h2>Enviar Mensagem</h2></center>";
?>

<form action="envia_mensagem.php" method="POST" name="form1" onsubmit="return checkrequired(this)">
<table border="2" class='bordasimples' align="center">
		 <thead>
			 <td colspan="6">
			  <center>
					 Enviar Mensagem
			 </center>
			 </td>					 
		 </thead>
<?php
	// obt�m os dados de todos os usu�rios
	$usuario_nome = mysql_query("SELECT usuario.id_user AS id, usuario.login AS nome FROM usuario ORDER BY nome");
	$usuarios = array();
	
	// pesquisa usu�rio um a um e define a op��o para selecionar seu nome para indicar quem est� solicitando pendentes
	while ($usuario = mysql_fetch_array($usuario_nome))
	{	  
		
		$id = $usuario['id'];
		$nome = $usuario['nome'];
		$usuarios[] = array("id"=>$id, "nome"=>$nome);
	}
?>
			
<tr class='tbrow noHover'>
	<td>Remetente:</td>
	<td>
		<SELECT id="user_id_from" name="user_id_from" style="width:340px">
		<OPTION checked value=""></OPTION>
		<?php
		foreach($usuarios as $usuario)
			echo "<option value=\"". $usuario['id'] ."\">". $usuario['nome'];
		?>
		</SELECT>  
	</td>
</tr>

<?php
if(!isset($id_to))
{
?>
<tr class='tbrow noHover'>
	<td>Destinat�rio:</td>
	<td>
		<SELECT id="user_id_to" name="user_id_to" style="width:340px">
		<OPTION checked value="0">Administrador</OPTION>
		<optgroup label="Usu�rios">
		<?php
		foreach($usuarios as $usuario)
			echo "<option value=\"". $usuario['id'] ."\">". $usuario['nome'];
		?>
			</optgroup>
		</SELECT>  
	</td>
</tr>
<?
}
else
{
	echo "<input type='hidden' name='user_id_to' value='". $id_to ."'>";
}
?>


<tr class='tbrow noHover'>
	<td>Mensagem:</td>
	<td>
		<table style="border:0">
			<tr>
			<td><textarea id="texto" cols="40" rows="10" name="texto"  onKeyUp="updateCount('texto', 'contador', 1024)"></textarea></td>
			</tr>
			<tr>
			<td>Restam <i><span id="contador">1024</span></i> caracteres.</td>
			</tr>
		</table>
	</td>
</tr>

<tr class='tbrow noHover' align="center">
<td colspan="6">
	<?php
	if(!isset($id_to))
		echo "<input type='hidden' name='quick_send' value='0'>";
	else
		echo "<input type='hidden' name='quick_send' value='1'>";
	?>
	<input type="submit" value="Enviar">&nbsp;&nbsp;<input type="reset" value="Limpar Dados">
</td>
</tr>
</table>
</form>

<center><a href="index.php"><< Voltar p/ Logador</a></center>

</body>
</html>



