<?php
/* �rea do Administrador (admin.php)
1. Se o administrador estiver logado, mostra op��es para as diversas atividades pertinentes ao mesmo
2. Caso contr�rio, solicita o nome e a senha do mesmo para a realiza��o de login
*/
	header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
    header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1

	require_once("Globals.php");		
	checkCookie(NOTHING);
	conecta();

?>
<html>
<head>
<title>�rea do Administrador</title>
<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
<center><h2> �rea do Administrador </h2></center>

<?php
  // se cookie existe, administrador est� logado, logo tem permiss�o de acesso a �rea do administrador
	// e criam-se bot�es para todas as atividades pertinentes ao mesmo
  if (isset($_COOKIE['c_adm']))
	{
		
	
?>

  <center>
	<table border=2 class='adm_table'>
	    <tr bgcolor="#990033" style="color: #fff;" align="center">
	<th width=240>Pendentes e Mensagens</th>
	<th width=200>Usu�rios</th>
	<th width=200>Administrar Horas</th>
	<th width=200>Hist�rico</th>
	</tr>
	<tr bgcolor="#F3F3F3">
<?php
  $pesq_pend = mysql_query("SELECT 
					COUNT(*) as pendentes
				FROM 
					config,
					pendentes
				WHERE 
					fim > config.mes_anterior");
	$num_linhas = mysql_result($pesq_pend, 'num');
	if (!$num_linhas)
	{
		echo "<td><center>";
	}
	else
	{
		echo "<td class='alt'><center>";
	}
?>
	<a href='adm_p.php'>Administrar Pendentes (<?php echo $num_linhas; ?>)</a>
	</td>

	<td>
    <center>
	<a href='alterar_usu.php'>Administrar Usu�rios</a>
	</td>

	<td>
	  <center>
		<a href='inserir_horas.php'>Inserir Horas</a>
	</td>
	
	<td>
	  <center>
    <a href='horas_semanais.php'>Horas Semanais / Atual</a>
	</td>
	</tr>
	
	<tr bgcolor="#F3F3F3">
<?php
  $pesq_pend = mysql_query("SELECT 
							COUNT(*) AS num
						FROM 
							config,
							pendentes
						WHERE fim <= config.mes_anterior");
	$num_linhas = mysql_result($pesq_pend, 'num');
	
	if (!$num_linhas)
	{
?>
	  <td><center>
<?php 
  }
	else
	{
?>
    <td class='alt'><center>
<?php
  }
  echo "<a href='adm_p.php?passado'>Ver Pendentes Esquecidas ($num_linhas)</a>";
?>
  </td>	
  
	<td>
	  <center>
		<a href='alterar_admin.php'>Alterar Administra��o</a>
	</td>
	
	<td>
	  <center>
		<a href='excluir_horas.php'>Excluir Horas</a>
	</td>
	
	<td>
	  <center>
    <a href='horas_semanais.php?passado'>Horas Semanais / Passadas</a>
	</td>
	</tr>
	
	
	
	<tr bgcolor="#F3F3F3">
<?php
  $pesq_mens = mysql_query("SELECT COUNT(*) AS num FROM mensagem WHERE id_to IS NULL");
	$num_linhas = mysql_result($pesq_mens, 'num');
	
	if (!$num_linhas)
	{
?>
	  <td><center>
<?php 
  }
	else
	{
?>
    <td class='alt'><center>
<?php
  }
  echo "<a href='analisar_mensagens.php'>Mensagens para Administrador ($num_linhas)</a>";
?>
	</td>
	
	<td>
	  <center>
		<a href='alterar_faltas.php'>Alterar Faltas</a>
	</td>
	
	<td>&nbsp; </td>
	<td>
	  <center>
		<a href='historico.php'>Hist�rico / M�s Atual</a>
	</td>
	</tr>
	
	
	<tr bgcolor="#F3F3F3">
	<?php
	$pesq_mens = mysql_query("SELECT COUNT(*) AS num FROM mensagem WHERE id_to IS NOT NULL");
	$num_linhas = mysql_result($pesq_mens, 'num');
	?>

	<td>
		<center>
			<a href="analisar_mensagens.php?all">Mensagens entre Usu�rios (<?php echo $num_linhas; ?>)</a>
		</center>
	</td>

	<td>&nbsp;</td>	
	<td>&nbsp;</td>
	<td>
	  <center>
		<a href='historico.php?passado'>Hist�rico / M�s Passado</a>
	</td>
	</tr>
	
		
	
	</table>

  <br><br>
	<a href="logout_adm.php">Logout</a>
  </center>
	
	
<?php
  } // caso o cookie n�o exista, administrador n�o est� logado, logo solicita-se o nome e a senha
	else
	{	  
?>
	 <form action="logadm.php" method="POST">
	 <table border="2" class='bordasimples' align="center">
		 <thead>
			 <td colspan="2">
			  <center>
					 Login
			 </center>
			 </td>					 
		 </thead>
	<tr class='tbrow'>
		<td>Nome:</td>
		<td><select name='id' style="width:150px">
<?php
$sql = "SELECT login, id_user FROM usuario WHERE senha is NOT NULL";
$rs = mysql_query($sql);

while($row = mysql_fetch_array($rs))
{
	echo "<option value='". $row['id_user'] ."'>". $row['login'];	
}
?>
		</select>
		</td>
	</tr>
	<tr class='tbrow'>
		<td>Senha:</td>
		<td><input type="password" name="pass" style="width:150px"></td>
	</tr>
	<tr class='tbrow' align="center">
		<td colspan="2">
			<input type="submit" value="Entrar">
		 </td>
	</tr>
	
	 <?php if (isset($_GET['erro'])) 
			echo "<tr class='tbrow' align='center'>
					<td colspan=2 style='font-size:14px'>". base64_decode($_GET['erro']) ."</td>
				  </tr>";
	 ?>
	
	
	</table>
	</form>				 
<?php				 
	}
	
?>	 
<center>
			<a href="index.php"><< Logador</a>
	</center>
</body>
</html> 