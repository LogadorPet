<?php
/*
Include CalcTemp
Aqui encontram-se diversas fun��es utilizadas pelas diversas p�ginas do logador.
S�o fun��es que tratam da manipula��o de datas, conex�o com o banco de dados, al�m
de controle de verifica��o de datas de acesso ao logador, bem como o ajuste do banco de dados
para atualiza��es de tabelas (quando troca-se a semana ou m�s)
*/

// defini��o do tempo em segundos de quatro semanas
define("QUATRO_SEMANAS_SEC",2419200);
// defini��o do tempo em segundos de uma semana (QUATRO_SEMANAS_SEC / 4)
define("UMA_SEMANA_SEC",604800);
// defini��o do caminho para o logador, caso se utilize o SQLite como SGBD
define("CAMINHO","../sqlitemanager/logador");

require_once("Globals.php");
conecta();

$__time = array();
recalculateTime();

function recalculateTime()
{
	global $__time;
	$sql = "SELECT UNIX_TIMESTAMP(mes_inicio) as 'mes_inicio', UNIX_TIMESTAMP(semana_inicio) as 'semana_inicio', UNIX_TIMESTAMP(prox_mes) as 'prox_mes', UNIX_TIMESTAMP(mes_anterior) as 'mes_anterior' FROM config;";
	$rs = mysql_query($sql);
	$__time = mysql_fetch_array($rs);
}

function getInicioMes()
{
	global $__time;
	return $__time['mes_inicio'];
}

function getInicioSemana()
{
	global $__time;
	return $__time['semana_inicio'];
}	 	 

function getProxMes()
{
	global $__time;
	return $__time['prox_mes'];
}

function getInicioMesAnterior()
{
	global $__time;
	return $__time['mes_anterior'];
}

// Func�o para realizar a conex�o com o banco de dados no SQLite
function conecta_bd()
{
  $db = sqlite_open(CAMINHO) or die("n�o abriu");
	return $db;
}


/*
// Fun��o para verificar se as datas informadas em $inicio e $fim est�o presentes em uma mesma semana
// (para verifica��o das pendentes)
function verifica_mesma_semana_pendente($inicio,$fim)
{
	
   // obt�m a o tempo inicial do m�s no arquivo $arquivo_mes
	 $arquivo_mes = "mes_inicio.txt";
   $parq = fopen($arquivo_mes,"r");
   $limite_inf_sem = fgets($parq,4096);
	 fclose($parq);
	 
	 // obt�m os tempos respectivos das datas de in�cio e fim da pendente
	 $tempo_ini_pend = strtotime($inicio);
	 $tempo_fim_pend = strtotime($fim);
	 
	 // descobre o n�mero da semana em que se encontra a pendente e atualiza os limites inferiores e superiores dessa semana
	 $num_semana = 1;
	 while ($limite_inf_sem + UMA_SEMANA_SEC <= $tempo_ini_pend)
	 {
	   $limite_inf_sem += UMA_SEMANA_SEC;
		 $num_semana++;
	 }
	 $limite_sup_sem = $limite_inf_sem + UMA_SEMANA_SEC;
	 
	 // se os tempos inicial e final da pendente encontram-se na mesma semana, retorna o n�mero da semana respectiva
	 if (($tempo_fim_pend <= $limite_sup_sem) && ($tempo_ini_pend >= $limite_inf_sem))
	   return $num_semana;
	 else // caso contr�rio, retorna 0
	   return 0;

	define('TEMPO_PRIMEIRA_SEMANA', 1479600);
	$semanaInicio = ceil(($inicio-TEMPO_PRIMEIRA_SEMANA) / UMA_SEMANA_SEC);
	$semanaFim = ceil(($fim-TEMPO_PRIMEIRA_SEMANA) / UMA_SEMANA_SEC);
	return $semanaInicio == $semanaFim;
}
*/

// Fun��o para retornar o n�mero da semana corrente do logador
function numero_semana()
{
   // obt�m o tempo inicial do m�s no arquivo $arquivo_mes
	 $tempo_mes = getInicioMes();

	 // obt�m o tempo atual
	 $tempo_atual = 1231156800;
	 
	 // calcula a diferen�a entre o tempo atual e o tempo inicial do m�s
	 $dif_tempo = $tempo_atual - $tempo_mes;
	 
	 // calcula o n�mero da semana atual e retorna pela fun��o
	 $num_semana = ($dif_tempo / UMA_SEMANA_SEC)%4;
	 
   return $num_semana;
}

// Fun��o que verifica se o tempo atual ainda pertence a semana correspondente no arquivo "ultima_semana.txt"
// Caso n�o estiver, faz modifica��es necess�rias no banco de dados e no arquivo da semana

function atualiza_semana()
{
	//$sql = "UPDATE config SET value = DATE(NOW() - INTERVAL DAYOFWEEK(NOW())-1 DAY) WHERE name = 'semana_inicio';";
	$sql = "UPDATE config SET semana_inicio = DATE(NOW() - INTERVAL DAYOFWEEK(NOW())-1 DAY);";
	$rs = mysql_query($sql);
	
	if(mysql_affected_rows() > 0)
		recalculateTime();
}



function verifica_mesmo_mes()
{	
	static $cont=0;
	$cont++;
	$sql = "SELECT prox_mes FROM config WHERE DATE(NOW()) >= prox_mes;";
	$rs = mysql_query($sql);

	if(mysql_num_rows($rs) > 0) //mudou de mes
	{
		//echo "mudou de mes";
		$sql = "UPDATE config SET mes_anterior = mes_inicio, mes_inicio = prox_mes, prox_mes = DATE(prox_mes + INTERVAL 4 WEEK);";
		$rs = mysql_query($sql);
			
		recalculateTime();
		verifica_mesmo_mes();
		$cont--;
		if($cont==1)
		{
			$sql = "DELETE FROM historico WHERE fim < FROM_UNIXTIME(". getInicioMesAnterior() .");";
			$rs = mysql_query($sql);
		}
	}		
}

// Fun��o para calcular a diferen�a de tempo (em formato de data) entre duas datas
function tempo($data1,$data2)
{
  // separa as datas em ano-mes-dia e horas-minutos-segundos
	$pedaco_data1 = split(" ",$data1);
	$pedaco_data2 = split(" ",$data2);

  // separa o horas-minutos-segundo em horas, minutos e segundos nos vetores $hms1 e $hms2
	$hms1 = split(":",$pedaco_data1[1]);
	$hms2 = split(":",$pedaco_data2[1]);
	
	// separa o ano-mes-dia em ano, mes e dia nos vetores $amd1 e $amd2
	$amd1 = split("-",$pedaco_data1[0]);
	$amd2 = split("-",$pedaco_data2[0]);  

	// obt�m o tempo em segundos das duas datas
	$tempo1 = mktime($hms1[0],$hms1[1],$hms1[2],$amd1[1],$amd1[2],$amd1[0]);
	$tempo2 = mktime($hms2[0],$hms2[1],$hms2[2],$amd2[1],$amd2[2],$amd2[0]);
  
	// calcula o tempo total de segundos de diferen�a entre as duas datas
	$tempo["segundos_total"] = $tempo2 - $tempo1;
	
	// calcula o tempo total de minutos de diferen�a entre as duas datas		
	$tempo["minutos_total"] = $tempo["segundos_total"] / 60;	
	
	// calcula o total final de segundos a existir na data
	$tempo["segundos"] = $tempo["segundos_total"] % 60;
	
	// calcula o total final de minutos a existir na data
	$tempo["minutos"] = $tempo["minutos_total"] % 60;

	// faz os c�lculos necess�rios para os demais de forma semelhante...
	$tempo["horas"] = floor($tempo["minutos_total"]/60);	
	$calculo = ceil((($tempo2 - $tempo1)/60)/60);
	$tempo["hora_total"] = $calculo;		
	$tempo["anos"] = ($calculo-($calculo%(365*24)))/(365*24);
	$calculo = ($calculo%(365*24));
	$tempo["meses"] = ($calculo-($calculo%(30*24)))/(30*24);
	$calculo = ($calculo%(30*24));
	$tempo["semanas"] = ($calculo-($calculo%(7*24)))/(7*24);
	$calculo = ($calculo%(7*24));
	$tempo["dias"] = ($calculo-($calculo%24))/24;
	$calculo = ($calculo%24);	
	$tempo["horas2"] = $tempo["horas"]%24;
	
	// formata o tempo em dias adequadamente
  $dias="0".$tempo['dias'];
	if ($tempo['dias']>9)
	  $dias=$tempo['dias'];

	// formata o tempo em horas adequadamente
	$horas="0".$tempo['horas2'];
	if ($tempo['horas2']>9)
	  $horas=$tempo['horas2'];
	
	// formata o tempo em minutos adequadamente
	$minutos="0".$tempo['minutos'];
	if ($tempo['minutos']>9)
	  $minutos=$tempo['minutos'];
	
	// formata o tempo em segundos adequadamente
	$segundos="0".$tempo['segundos'];
	if ($tempo['segundos']>9)
	  $segundos=$tempo['segundos'];	
	
	// formata o tempo em meses adequadamente
	$meses="0".$tempo['meses'];
	if ($tempo['meses']>9)
	  $meses=$tempo['meses'];
	
	// formata o tempo em anos adequadamente
	$anos="000".$tempo['anos'];
	if ($tempo['anos']<10)
	  $anos="000".$tempo['anos'];
	if ($tempo['anos']<100)
	  $anos="00".$tempo['anos'];
	if ($tempo['anos']<1000)
	  $anos="0".$tempo['anos'];
	else $anos=$tempo['anos'];			
	
	// formata a data final e retorna pela fun��o
 	$tempo['data'] = "$anos-$meses-$dias $horas:$minutos:$segundos";	
	return $tempo;
}

// Fun��o para transformar uma data em tempo total de segundos (tempo como data base 00-00-00 00:00:00 para tempo 0)
function datetosec($data) 
{
  // separa a data em ano-mes-dia e horas-minutos-segundos
	$pedaco_data = split(" ",$data);
	
	// separa o horas-minutos-segundo em horas, minutos e segundos no vetor $hms
  $hms = split(":",$pedaco_data[1]);
	
	// separa o ano-mes-dia em ano, mes e dia no vetor $amd
	$amd = split("-",$pedaco_data[0]);	
	
	// obt�m o tempo em segudos de uma certa data em rela��o a data inicial 00-00-00 00:00:00 e retorna pela fun��o
  $tempo = mktime($hms[0],$hms[1],$hms[2],$amd[1],$amd[2],$amd[0])-943927200;
	return $tempo;		
}

// Fun��o que transforma um n�mero de segundos em uma data (representando tempo 0s como data 00-00-00 00:00:00) 
function sectodate2 ($seconds) 
{
   // se o n�mero de segundos for menor que 60, o caso � trivial e retorna data constru�da
   if ($seconds<60) 
	 {
		 if ($seconds < 10) 
		   $str_sec = "0" . $seconds;
		 else
		   $str_sec = $seconds;		
		 return ("0000-00-00 00:00:$str_sec");
	 }
	 
	 // caso contr�rio, calcula a data...
	 // obt�m o n�mero total de minutos, considerando o n�mero total de segundos
   $tempo["minutos_total"]= $seconds / 60;	
	 
	 // obt�m o valor de segundos final a ser colocado na data
	 $tempo["segundos"] = $seconds % 60;
	 
	 // obt�m o valor total de horas, levando em considera��o o n�mero total de minuto
	 $tempo["horas_total"] = floor($tempo["minutos_total"] / 60);
	 
	 // obt�m o valor de horas final a ser colocado na data
	 $tempo["horas"] = $tempo["horas_total"] % 24;
	
	 // obt�m o valor de minutos total a ser colocado na data
	 $tempo["minutos"] = $tempo["minutos_total"] % 60;
	 
	 // obt�m o valor de dias final a ser colocado na data
   $tempo["dias"] = floor($tempo["horas_total"] / 24);	
	 
	 // obt�m o string dos dias final
	 $dias = "0".$tempo["dias"];
	 if ($tempo["dias"] > 9)
	   $dias = $tempo["dias"];
		 
	 // obt�m o string das horas final
	 $horas = "0".$tempo["horas"];	 
	 if ($tempo["horas"] > 9)
	   $horas = $tempo["horas"];
		 
	 // obt�m o string dos minutos final	
	 $minutos = "0" . $tempo["minutos"];
	 if ($tempo["minutos"] > 9)
	   $minutos = $tempo["minutos"];	
		 
	 // obt�m o string dos segundos final
	 $segundos ="0".$tempo["segundos"];
	 if ($tempo["segundos"] > 9)
	   $segundos=$tempo["segundos"];	
	 
	 // realiza o formato da data a ser retornado
   $formato = "$dias-00-0000 $horas:$minutos:$segundos";
	 return $formato; 
}


// Fun��o que transforma um n�mero de segundos em uma data (representando tempo 0s como data 00-00-00 00:00:00) 
function sectodate ($seconds) 
{
   // se o n�mero de segundos for menor que 60, o caso � trivial e retorna data constru�da
   if ($seconds<60) 
	 {
		 if ($seconds < 10) 
		   $str_sec = "0" . $seconds;
		 else
		   $str_sec = $seconds;		
		 return ("0000-00-00 00:00:$str_sec");
	 }
	 
	 // caso contr�rio, calcula a data...
	 // obt�m o n�mero total de minutos, considerando o n�mero total de segundos
   $tempo["minutos_total"]= $seconds / 60;	
	 
	 // obt�m o valor de segundos final a ser colocado na data
	 $tempo["segundos"] = $seconds % 60;
	 
	 // obt�m o valor total de horas, levando em considera��o o n�mero total de minuto
	 $tempo["horas_total"] = floor($tempo["minutos_total"] / 60);
	 
	 // obt�m o valor de horas final a ser colocado na data
	 $tempo["horas"] = $tempo["horas_total"] % 24;
	
	 // obt�m o valor de minutos total a ser colocado na data
	 $tempo["minutos"] = $tempo["minutos_total"] % 60;
	 
	 // obt�m o valor de dias final a ser colocado na data
   $tempo["dias"] = floor($tempo["horas_total"] / 24);	
	 
	 // obt�m o string dos dias final
	 $dias = "0".$tempo["dias"];
	 if ($tempo["dias"] > 9)
	   $dias = $tempo["dias"];
		 
	 // obt�m o string das horas final
	 $horas = "0".$tempo["horas"];	 
	 if ($tempo["horas"] > 9)
	   $horas = $tempo["horas"];
		 
	 // obt�m o string dos minutos final	
	 $minutos = "0" . $tempo["minutos"];
	 if ($tempo["minutos"] > 9)
	   $minutos = $tempo["minutos"];	
		 
	 // obt�m o string dos segundos final
	 $segundos ="0".$tempo["segundos"];
	 if ($tempo["segundos"] > 9)
	   $segundos=$tempo["segundos"];	
	 
	 // realiza o formato da data a ser retornado
   $formato = "0000-00-$dias $horas:$minutos:$segundos";
	 return $formato; 
}

// Fun��o que, dado uma data no formato DATETIME, retransforma o valor em formato 'xxh yym'
function datetohour($data) 
{	
  // separa a data em ano-mes-dia e horas-minutos-segundos
	$pedaco_data = split(" ",$data);
	
	// separa o horas-minutos-segundo em horas, minutos e segundos no vetor $hms
  $hms = split(":",$pedaco_data[1]);
	
	// separa o ano-mes-dia em ano, mes e dia no vetor $amd
	$amd = split("-",$pedaco_data[0]);	
	
	// obt�m o n�mero de minutos e horas
	$minutos = $hms[1];
	$horas = $hms[0] + $amd[2]*24;
	
	// formata o string das horas		
	if ($horas < 10) 
	  $str_horas = "0".$horas;
	else
	  $str_horas = $horas;
		
	// transforma o n�mero de horas e minutos para o novo formato em string
	$formato = $str_horas ."h ". $minutos."min"; 
	return $formato;
}

// Fun��o que dado um determinado valor em segundos, transforma o valor em horas para efeitos de visualiza��o na tela
function sectohour ($sec) 
{
  if ($sec!=0)return (datetohour(sectodate($sec)));
		 			else return("00h 00min");
}

// Fun��o que dados dois valores em DATETIME, soma as datas (em segundos) e retransforma em uma nova data
function somadata ($data1,$data2) 
{
	$x =  sectodate(datetosec($data1) + datetosec($data2));
	return $x;
}


// Fun��o que dados dois valores em DATETIME, subtrai as datas (em segundos) e retransforma em uma nova data
function subtraidata ($data1,$data2)
{
  $x = sectodate(datetosec($data1) - datetosec($data2));
	return $x;
}

function subtraidata2 ($data1,$data2)
{
  $x = sectodate2(datetosec($data1) - datetosec($data2));
	return $x;
}

?>