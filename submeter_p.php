<?php
/* Submiss�o de Pendentes (submeter_p.php)
1. Recebe os dados selecionados pelo usu�rio para pendente
2. Possui link para redirecionar os dados para a p�gina 'recebe_p.php' 
*/
  header ("Pragma: no-cache");							// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
  header("Cache-Control: no-cache, must-revalidate");	// idem http 1,1
 
  //realiza a conex�o com o banco de dados
  require_once("Globals.php");
  conecta();
?>

<html>
<head>
<title>Submeter Pendente - Logador PET</title>
<script language="JavaScript" type="text/javascript">
/*
Fun��o para testar se todos os campos foram preenchidos
*/

function checkrequired(which)
{
  var pass=true
	var dia=true
  if (document.images)
	{
    for (i=0;i<which.length;i++)
		{
      var tempobj=which.elements[i]
       if (((tempobj.type=="text"||tempobj.type=="textarea")&&tempobj.value==''))
			{
        pass=false
        break
      }
			
    }
  }
	if (document.form1.nome.value == "")
	  pass = false
	
  if (!pass)
  {
    alert("Voc� n�o preencheu todos os campos!")
    return false
  }

}

function limita_tamanho(which,limite)
{
  var tamanho;
	var count;
	tamanho = which.value.length;
	if (tamanho > limite)
	{
	  which.value = which.value.substring(0,limite);
	}
	document.getElementByID("contador").innerHTML = "Member";
}

function updateCount(textareaId, spanId, limite) 
{
  var tamanho;
  textarea = document.getElementById(textareaId);
	tamanho = textarea.value.length;
	if (tamanho > limite)
	{
	  textarea.value = textarea.value.substring(0,limite);
	}
  if (textarea == null) 
	{
    return;
  }
  if (textarea.value.length > limite) 
	{
    textarea.value = textarea.value.substring(0, limite);
  }
  document.getElementById(spanId).innerHTML = limite - textarea.value.length;
}

function $() {
	var elements = new Array();
	for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
			element = document.getElementById(element);
		if (arguments.length == 1)
			return element;
		elements.push(element);
	}
	return elements;
}


function mudaData()
{
	var meses = new Array("janeiro","fevereiro","mar�o","abril","maio","junho","julho","agosto","setembro","outubro","novembro","dezembro");
	
	dia_i = $('dia').value;
	mes_i = $('mes').value;
	ano_i = $('ano').value;
	var myday = new Date(""+ ano_i + "/" + mes_i + "/" +dia_i);
	
	limit = 2;
	
	selectBox = $('data_fim');
	selectBox.options.length = 0;
	myday.setDate(myday.getDate());
	dia_f = myday.getDate();
	mes_f = myday.getMonth()+1;
	ano_f = myday.getFullYear();
	selectBox.options[0] = new Option(dia_f + " " + meses[mes_f-1] + " " + ano_f);
		
	for (i=1; i<limit; i++)
	{
		myday.setDate(myday.getDate()+1);
		dia_f = myday.getDate();
		mes_f = myday.getMonth()+1;
		ano_f = myday.getFullYear();
		selectBox.options[i] = new Option(dia_f + " " + meses[mes_f-1] + " " + ano_f);
	
	}
}
</script>
<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body onLoad="updateCount('justificativa', 'contador', 1024);mudaData()">

<form action="recebe_p.php" method="POST" name="form1" onSubmit="return checkrequired(this)">
<table border="2" class='bordasimples' align="center">
		 <thead>
			 <td colspan="6">
			  <center>
					 Solicitar Pendentes
			 </center>
			 </td>					 
		 </thead>
<tr class='tbrow'>
	<td>Nome:</td>
	<td colspan="5">
		<SELECT id="user_id" name="user_id" style="width:340px">
		<OPTION checked></OPTION>
		<?php
		  // obt�m os dados de todos os usu�rios
		  $usuario_nome = mysql_query("SELECT usuario.id_user AS id, usuario.login AS nome FROM usuario ORDER BY nome");
			
			// pesquisa usu�rio um a um e define a op��o para selecionar seu nome para indicar quem est� solicitando pendentes
		  while ($usuario = mysql_fetch_array($usuario_nome))
			{	  
				$id = $usuario['id'];
				$nome = $usuario['nome'];
				echo "<option value=\"$id\">$nome";
			}
			?>
		</SELECT>  
	</td>
</tr>
<tr class='tbrow'>
	 <td width="50"></td>
	 <td>Dia</td>
	 <td>M�s</td>
	 <td>Ano</td>
	 <td>Hora</td>
	 <td>Minutos</td>
</tr>
<tr class='tbrow'>
	<td>In�cio</td>
		<td>			 	
		<select id="dia" name="dia" onChange="mudaData()">
		<?php
					 
			  // para todos os dias do m�s, cria op��es para selecionar o dia da pendente
		for ($x=1;$x<=31;$x++)
		{
			if ($x<10)
				$dia = "0$x";
			else 
				$dia = $x;
				
			echo "<option value=\"$dia\" ";
			if ($dia == date('j'))
				echo "selected";
			echo ">$dia";
		}				
		?> 
		</select>
		</td>
		<td>
		<!-- define as op��es para selecionar o m�s da pendente -->
		<?php

		echo "<select id='mes' name='mes' onChange='mudaData()'>";

		for ($i = 1; $i <= sizeof($meses); $i++)
		{
			if ($i < 10)
				$str = "0$i";
			else
				$str = $i;
			echo "<option value=\"$str\" ";
			if ($str == date('m'))
				echo "selected";
			echo ">" . $meses[$i-1];
		}

		echo "</select>";
		?>
		</td>
		<td>
		<select id="ano" name="ano" onChange="mudaData()">
		<?php 

			  // para todos os anos entre atual e atual+1, oferece a op��o de selecionar o ano espec�fico da pendente  
		for ($x=(date('Y')-1);$x<=date('Y')+1;$x++)
		{
			echo "<option value=\"$x\" ";
			if( $x == date('Y') )
				echo "selected";
			echo ">$x";
		}				
		?> 
		</select>
		</td>
		<td>
		<select id="hora_i" name="hora_i">
		<?php 
			  // oferece op��es para selecionar as horas do in�cio da pendente
					for ($x=0;$x<=23;$x++)
					{
		?>
		<option value=<?php if ($x<10) echo "0$x"; else echo $x; ?> > <?php if ($x<10) echo "0$x"; else echo $x; ?>
		<?php
					}				
		?> 
		</select>
		</td>
		<td>
		<select id="minuto_i" name="minuto_i">
		<?php 
			  // oferece op��es para selecionar os minutos do in�cio da pendente
				  for ($x=0;$x<=59;$x++)
					{
		?>
		<option value=<?php if ($x<10) echo "0$x"; else echo $x; ?> > <?php if ($x<10) echo "0$x"; else echo $x; ?>
		<?php
					}				
		?> 
		</select>
		</td>
</tr>

<tr class='tbrow'>
	<td>Fim</td>
	<td colspan="3">			 	
	<select id="data_fim" name="data_f" style="width:100%">
	<option>
	</select>
	</td>
	<td>

	<select name="hora_f">
	<?php 
				// oferece op��es para selecionar a hora final da pendente
				for ($x=0;$x<=23;$x++)
				{
	?>
	<option value=<?php if ($x<10) echo "0$x"; else echo $x; ?> > <?php if ($x<10) echo "0$x"; else echo $x; ?>
	<?php
				}				
	?> 
	</select>
	</td>
	<td>

	<select name="minuto_f">
	<?php 
				// oferece op��es para selecionar o minuto final da pendente
				for ($x=0;$x<=59;$x++)
				{
	?>
	<option value=<?php if ($x<10) echo "0$x"; else echo $x; ?> > <?php if ($x<10) echo "0$x"; else echo $x; ?>
	<?php
				}
				
				// fecha a conex�o com o banco de dados
								
	?> 
	</select>
	</td>
</tr>

<tr class='tbrow'>
	<td>Local:</td>
	<td colspan="5"><input type="text" name="local" style="width:340px"></td>
</tr>

<tr class='tbrow'>
	<td>Justificativa:</td>
	<td colspan="5">
		<table style="border:0">
			<tr>
			<td><textarea id="justificativa" cols="40" rows="10" name="justificativa" onKeyUp="updateCount('justificativa', 'contador', 1024)"></textarea></td>
			</tr>
			<tr>
			<td>Restam <i><span id="contador">1024</span></i> caracteres.</td>
			</tr>
		</table>
	</td>
</tr>

<tr class='tbrow' align="center">
<td colspan="6">
	<input type="submit" value="Enviar">&nbsp;&nbsp;<input type="reset" value="Limpar Dados">
</td>
</tr>

</table>
</form>

<center><a href="index.php"><< Voltar p/ Logador</a></center>


</body>
</html>
