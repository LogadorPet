<?php
/* Alterar Administrador (alterar_admin.php)
1. Solicita a sele��o do novo administrador, a informa��o da nova senha e confirma��o
2. Repassa as informa��es recebidas para a p�gina 'altera_admin.php'
*/
  header ("Pragma: no-cache");						// sempre carregar p�gina (n�o vai ser armazenada no cache http 1,0
  header("Cache-Control: no-cache, must-revalidate"); // idem	http 1,1
	
require_once("Globals.php");
//verifica se o administrador est� logado
checkCookie();
// realiza conex�o com o banco de dados
conecta();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Alterar Administrador</title>
<link rel="stylesheet" type="text/css" href="estilo.css">
<script language="JavaScript" type="text/javascript">
/*
Fun��o para testar se todos os campos foram preenchidos
*/
function checkrequired(form)
{
  var pass=true
	
	if (form.senha.value!=form.senha_conf.value||(form.senha.value==''))
	{
		pass=false		
	}
	
  if (!pass)
  {
	alert("\"Nova Senha\" diferente de \"Confirma Nova Senha\"!")
    return false
  }
  else
    return true
}

function confirma_exclusao(nome)
{
	if(confirm('Voc� confirma que "'+ nome +'" n�o � mais Administrador?'))
		return true;
	else
		return false;
}
</script>
</head>
<body>
<center><h3>Alterar Administradores</h2></center>

<center>
	<table border=2 class='bordasimples'>
	<thead>
	<th width=100>Nome</th>
	<th width=50>Editar</th>
	<th width=50>Excluir</th>
	</thead>

<?php

$sql = "SELECT login, id_user FROM usuario WHERE senha is NOT NULL ORDER BY login ASC";
$rs = mysql_query($sql);

while($row = mysql_fetch_array($rs))
{
	echo "<tr class='tbrow'><td>";
	echo "<option value='". $row['id_user'] ."'>". $row['login'];	
	echo "</td>";
	echo "<td align='center'><a href='alterar_senha.php?id=". $row['id_user'] ."'><img src='images/page_edit.png'></a></td>";
	echo "<td align='center'><a href='altera_senha.php?id=". $row['id_user'] ."&deleta' onclick='return confirma_exclusao(\"". $row['login'] ."\")'><img src='images/delete.png'></a></td>";
	echo "</tr>";
}
?>
</table>
</center>

<br>

<center><h3>Cadastrar Novo</h3></center>
<form action="insere_admin.php" method="POST" onsubmit="return checkrequired(this)">

<center>
<table border=2 class='bordasimples'>
<tr bgcolor="#990033" style="color: #fff;" align="center">
<th width=200 colspan="2">Novo administrador</th>
</tr>
 
<tr class="tbrow"><td colspan="2">
<select name="usuario" style="width:100%">
<?php
  // obt�m os dados de todos os usu�rios
  $usuario_nome = mysql_query("SELECT login, id_user FROM usuario WHERE senha is NULL ORDER BY login ASC");
	
	// para cada usu�rio, cria um campo para sele��o do mesmo
  while ($usuario = mysql_fetch_array($usuario_nome))
	{	
		echo "<OPTION value='". $usuario['id_user'] ."'>". $usuario['login'];
	}
	// solicita nova senha e confirma��o de nova senha
	?>
</select>
</td></tr>

<tr class='tbrow'>
<td>Senha do Novo Administrador</td>
<td><input type="password" name="senha"></td>
</tr>

<tr class='tbrow'>
<td>Conf. Senha</td>
<td><input type="password" name="senha_conf"></td>
</tr>

<tr class='tbrow'>
<td align=center colspan=2><input type="submit" value="Enviar">&nbsp;&nbsp;<input type="reset" value="Limpar Dados"></td>
</tr>

</table>
</center>

<center>
<a href="admin.php"><< Administrador</a><br>
<a href="index.php"><< Logador </a><br>
</center>
</form>

</body>
</html>
